<?php

use Illuminate\Support\Facades\Hash;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/locale/{locale}', function ($locale) { 
    session(['einutech_locale' => $locale]);

    return redirect()->back();
})->name('locale');



Route::get('/admin/login', 'Auth\LoginController@showAdminLoginForm')->name('login');
Route::post('/admin/login', 'Auth\LoginController@loginAdmin');

Route::get('/{country_code}/{company_slug}/login', 'Auth\LoginController@showLoginForm')->name('login.company');
Route::post('/{country_code}/{company_slug}/login', 'Auth\LoginController@login');

Route::get('/images/logo/{id}', 'ImagesController@companyLogo')->name('images.logo');
Route::get('/embed/{id}', 'EmbedController@index')->name('embed.index');
Route::get('/apply/{id}', 'ApplicationsController@show')->name('applications.show');
Route::post('/apply/{id}', 'ApplicationsController@store')->name('applications.store');

Route::group(['middleware' => 'auth'], function () {
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

    Route::get('/files/download/{id}', 'FilesController@download')->name('files.download');
    Route::get('/files/icon/{id}', 'FilesController@icon')->name('files.icon');
    Route::delete('/files/delete', 'FilesController@destroy')->name('files.delete');

    Route::get('/images/{id}', 'ImagesController@show')->name('images.show');
    Route::get('/settings', 'SettingsController@index')->name('settings.index');
    Route::get('/settings/{page}', 'SettingsController@page')->name('settings.page');

    Route::group(['middleware' => 'role:administrator', 'prefix' => 'admin', 'namespace' => 'Administration\\'], function () {
        /**
         * All administrators
         */
        Route::get('/administrators', 'AdministratorsController@index')->name('administrators.index');
        Route::post('/administrators', 'AdministratorsController@store');
        Route::post('/administrators/delete', 'AdministratorsController@bulkDelete')->name('administrators.bulkdelete');
        Route::delete('/administrators/delete', 'AdministratorsController@bulkDestroy');
        Route::get('/administrators/create', 'AdministratorsController@create')->name('administrators.create');
        Route::get('/administrators/{id}', 'AdministratorsController@show')->name('administrators.show');
        Route::patch('/administrators/{id}', 'AdministratorsController@update');
        Route::delete('/administrators/{id}', 'AdministratorsController@destroy');
        Route::get('/administrators/{id}/edit', 'AdministratorsController@edit')->name('administrators.edit');
        Route::get('/administrators/{id}/delete', 'AdministratorsController@delete')->name('administrators.delete');
        Route::get('/administrators/download/spreadsheet', 'AdministratorsController@downloadSpreadsheet')->name('administrators.download.spreadsheet');

        Route::group(['prefix' => 'companies'], function () {
            /**
             * All companies
             */
            Route::get('/', 'CompaniesController@index')->name('companies.index');
            Route::post('/', 'CompaniesController@store');
            Route::post('/delete', 'CompaniesController@bulkDelete')->name('companies.bulkdelete');
            Route::delete('/delete', 'CompaniesController@bulkDestroy');
            Route::get('/create', 'CompaniesController@create')->name('companies.create');
            Route::get('/{id}', 'CompaniesController@show')->name('companies.show');
            Route::patch('/{id}', 'CompaniesController@update');
            Route::delete('/{id}', 'CompaniesController@destroy');
            Route::get('/{id}/edit', 'CompaniesController@edit')->name('companies.edit');
            Route::get('/{company_id}/delete', 'CompaniesController@delete')->name('companies.delete');
            Route::get('/download/spreadsheet', 'CompaniesController@downloadSpreadsheet')->name('companies.download.spreadsheet');

            /**
             * All managers that belong to a company
             */
            Route::get('/{company_id}/managers', 'ManagersController@index')->name('companies.managers.index');
            Route::post('/{company_id}/managers', 'ManagersController@store');
            Route::post('/{company_id}/managers/delete', 'ManagersController@bulkDelete')->name('companies.managers.bulkdelete');
            Route::delete('/{company_id}/managers/delete', 'ManagersController@bulkDestroy');
            Route::get('/{company_id}/managers/create', 'ManagersController@create')->name('companies.managers.create');
            Route::get('/{company_id}/managers/{manager_id}', 'ManagersController@show')->name('companies.managers.show');
            Route::patch('/{company_id}/managers/{manager_id}', 'ManagersController@update');
            Route::delete('/{company_id}/managers/{manager_id}', 'ManagersController@destroy');
            Route::get('/{company_id}/managers/{manager_id}/edit', 'ManagersController@edit')->name('companies.managers.edit');
            Route::get('/{company_id}/managers/{manager_id}/delete', 'ManagersController@delete')->name('companies.managers.delete');
            Route::get('/{company_id}/managers/download/spreadsheet', 'ManagersController@downloadSpreadsheet')->name('companies.managers.download.spreadsheet');

            /**
             * All agents that belong to a company
             */
            Route::get('/{company_id}/agents', 'AgentsController@index')->name('companies.agents.index');
            Route::post('/{company_id}/agents', 'AgentsController@store');
            Route::post('/{company_id}/agents/delete', 'AgentsController@bulkDelete')->name('companies.agents.bulkdelete');
            Route::delete('/{company_id}/agents/delete', 'AgentsController@bulkDestroy');
            Route::get('/{company_id}/agents/create', 'AgentsController@create')->name('companies.agents.create');
            Route::get('/{company_id}/agents/{agent_id}', 'AgentsController@show')->name('companies.agents.show');
            Route::patch('/{company_id}/agents/{agent_id}', 'AgentsController@update');
            Route::delete('/{company_id}/agents/{agent_id}', 'AgentsController@destroy');
            Route::get('/{company_id}/agents/{agent_id}/edit', 'AgentsController@edit')->name('companies.agents.edit');
            Route::get('/{company_id}/agents/{agent_id}/delete', 'AgentsController@delete')->name('companies.agents.delete');
            Route::get('/{company_id}/agents/download/spreadsheet', 'AgentsController@downloadSpreadsheet')->name('companies.agents.download.spreadsheet');

            /**
             * All employees that belong to a company
             */
            Route::get('/{company_id}/employees', 'EmployeesController@index')->name('companies.employees.index');
            Route::post('/{company_id}/employees', 'EmployeesController@store');
            Route::post('/{company_id}/employees/delete', 'EmployeesController@bulkDelete')->name('companies.employees.bulkdelete');
            Route::delete('/{company_id}/employees/delete', 'EmployeesController@bulkDestroy');
            Route::get('/{company_id}/employees/create', 'EmployeesController@create')->name('companies.employees.create');
            Route::get('/{company_id}/employees/{employee_id}', 'EmployeesController@show')->name('companies.employees.show');
            Route::patch('/{company_id}/employees/{employee_id}', 'EmployeesController@update');
            Route::delete('/{company_id}/employees/{employee_id}', 'EmployeesController@destroy');
            Route::get('/{company_id}/employees/{employee_id}/edit', 'EmployeesController@edit')->name('companies.employees.edit');
            Route::get('/{company_id}/employees/{employee_id}/delete', 'EmployeesController@delete')->name('companies.employees.delete');
            Route::get('/{company_id}/employees/download/spreadsheet', 'EmployeesController@downloadSpreadsheet')->name('companies.employees.download.spreadsheet');

            /**
             * All candidates that belong to a company
             */
            Route::get('/{company_id}/candidates', 'CandidatesController@index')->name('companies.candidates.index');
            Route::post('/{company_id}/candidates', 'CandidatesController@store');
            Route::post('/{company_id}/candidates/delete', 'CandidatesController@bulkDelete')->name('companies.candidates.bulkdelete');
            Route::delete('/{company_id}/candidates/delete', 'CandidatesController@bulkDestroy');
            Route::get('/{company_id}/candidates/create', 'CandidatesController@create')->name('companies.candidates.create');
            Route::get('/{company_id}/candidates/{candidate_id}', 'CandidatesController@show')->name('companies.candidates.show');
            Route::patch('/{company_id}/candidates/{candidate_id}', 'CandidatesController@update');
            Route::delete('/{company_id}/candidates/{candidate_id}', 'CandidatesController@destroy');
            Route::get('/{company_id}/candidates/{candidate_id}/edit', 'CandidatesController@edit')->name('companies.candidates.edit');
            Route::get('/{company_id}/candidates/{candidate_id}/delete', 'CandidatesController@delete')->name('companies.candidates.delete');
            Route::get('/{company_id}/candidates/download/spreadsheet', 'CandidatesController@downloadSpreadsheet')->name('companies.candidates.download.spreadsheet');

            /**
             * All positions that belong to a company
             */
            Route::get('/{company_id}/positions', 'PositionsController@index')->name('companies.positions.index');
            Route::post('/{company_id}/positions', 'PositionsController@store');
            Route::post('/{company_id}/positions/delete', 'PositionsController@bulkDelete')->name('companies.positions.bulkdelete');
            Route::delete('/{company_id}/positions/delete', 'PositionsController@bulkDestroy');
            Route::get('/{company_id}/positions/create', 'PositionsController@create')->name('companies.positions.create');
            Route::get('/{company_id}/positions/{position_id}', 'PositionsController@show')->name('companies.positions.show');
            Route::patch('/{company_id}/positions/{position_id}', 'PositionsController@update');
            Route::delete('/{company_id}/positions/{position_id}', 'PositionsController@destroy');
            Route::get('/{company_id}/positions/{position_id}/edit', 'PositionsController@edit')->name('companies.positions.edit');
            Route::get('/{company_id}/positions/{position_id}/delete', 'PositionsController@delete')->name('companies.positions.delete');
            Route::get('/{company_id}/positions/download/spreadsheet', 'PositionsController@downloadSpreadsheet')->name('companies.positions.download.spreadsheet');
        });
    });

    Route::group(['middleware' => 'role:manager'], function () {
        /**
         * Current company information
         */
        Route::get('/company', 'CompanyController@index')->name('company.index');
        Route::patch('/company', 'CompanyController@update');
        Route::get('/company/edit', 'CompanyController@edit')->name('company.edit');
        Route::post('/company/upload/files', 'CompanyController@uploadFiles')->name('company.upload.files');
        Route::post('/company/upload/image', 'CompanyController@uploadImage')->name('company.upload.image');

        /**
         * All agents that belong to current managers's company
         */
        Route::get('/agents', 'AgentsController@index')->name('agents.index');
        Route::post('/agents', 'AgentsController@store');
        Route::post('/agents/delete', 'AgentsController@bulkDelete')->name('agents.bulkdelete');
        Route::delete('/agents/delete', 'AgentsController@bulkDestroy');
        Route::get('/agents/create', 'AgentsController@create')->name('agents.create');
        Route::get('/agents/download/spreadsheet', 'AgentsController@downloadSpreadsheet')->name('agents.download.spreadsheet');
        Route::get('/agents/{id}', 'AgentsController@show')->name('agents.show');
        Route::patch('/agents/{id}', 'AgentsController@update');
        Route::delete('/agents/{id}', 'AgentsController@destroy');
        Route::get('/agents/{id}/edit', 'AgentsController@edit')->name('agents.edit');
        Route::get('/agents/{id}/delete', 'AgentsController@delete')->name('agents.delete');
    });

    Route::group(['middleware' => 'role:agent'], function () {
        /**
         * All employees that belong to current agents's company
         */
        Route::get('/employees', 'EmployeesController@index')->name('employees.index');
        Route::post('/employees', 'EmployeesController@store');
        Route::post('/employees/delete', 'EmployeesController@bulkDelete')->name('employees.bulkdelete');
        Route::delete('/employees/delete', 'EmployeesController@bulkDestroy');
        Route::get('/employees/create', 'EmployeesController@create')->name('employees.create');
        Route::get('/employees/download/spreadsheet', 'EmployeesController@downloadSpreadsheet')->name('employees.download.spreadsheet');
        Route::get('/employees/{id}', 'EmployeesController@show')->name('employees.show');
        Route::patch('/employees/{id}', 'EmployeesController@update');
        Route::delete('/employees/{id}', 'EmployeesController@destroy');
        Route::get('/employees/{id}/edit', 'EmployeesController@edit')->name('employees.edit');
        Route::get('/employees/{id}/delete', 'EmployeesController@delete')->name('employees.delete');
        Route::get('/employees/{id}/print', 'EmployeesController@print')->name('employees.print');
        Route::post('/employees/{id}/upload/files', 'EmployeesController@uploadFiles')->name('employees.upload.files');
        Route::post('/employees/{id}/upload/image', 'EmployeesController@uploadImage')->name('employees.upload.image');

        /**
         * All candidates that belong to current agents's company
         */
        Route::get('/candidates', 'CandidatesController@index')->name('candidates.index');
        Route::post('/candidates', 'CandidatesController@store');
        Route::post('/candidates/delete', 'CandidatesController@bulkDelete')->name('candidates.bulkdelete');
        Route::delete('/candidates/delete', 'CandidatesController@bulkDestroy');
        Route::get('/candidates/create', 'CandidatesController@create')->name('candidates.create');
        Route::get('/candidates/download/spreadsheet', 'CandidatesController@downloadSpreadsheet')->name('candidates.download.spreadsheet');
        Route::get('/candidates/{id}', 'CandidatesController@show')->name('candidates.show');
        Route::patch('/candidates/{id}', 'CandidatesController@update');
        Route::delete('/candidates/{id}', 'CandidatesController@destroy');
        Route::get('/candidates/{id}/edit', 'CandidatesController@edit')->name('candidates.edit');
        Route::get('/candidates/{id}/delete', 'CandidatesController@delete')->name('candidates.delete');
        Route::get('/candidates/{id}/print', 'CandidatesController@print')->name('candidates.print');
        Route::post('/candidates/{id}/upload/files', 'CandidatesController@uploadFiles')->name('candidates.upload.files');
        Route::post('/candidates/{id}/upload/image', 'CandidatesController@uploadImage')->name('candidates.upload.image');

        /**
         * All departments that belong to current agent's company
         */
        Route::get('/departments', 'DepartmentsController@index')->name('departments.index');
        Route::post('/departments', 'DepartmentsController@store');
        Route::post('/departments/delete', 'DepartmentsController@bulkDelete')->name('departments.bulkdelete');
        Route::delete('/departments/delete', 'DepartmentsController@bulkDestroy');
        Route::get('/departments/create', 'DepartmentsController@create')->name('departments.create');
        Route::get('/departments/download/spreadsheet', 'DepartmentsController@downloadSpreadsheet')->name('departments.download.spreadsheet');
        Route::get('/departments/{id}', 'DepartmentsController@show')->name('departments.show');
        Route::patch('/departments/{id}', 'DepartmentsController@update');
        Route::delete('/departments/{id}', 'DepartmentsController@destroy');
        Route::get('/departments/{id}/edit', 'DepartmentsController@edit')->name('departments.edit');
        Route::get('/departments/{id}/delete', 'DepartmentsController@delete')->name('departments.delete');

        /**
         * All positions that belong to current agents's company
         */
        Route::get('/positions', 'PositionsController@index')->name('positions.index');
        Route::post('/positions', 'PositionsController@store');
        Route::post('/positions/delete', 'PositionsController@bulkDelete')->name('positions.bulkdelete');
        Route::delete('/positions/delete', 'PositionsController@bulkDestroy');
        Route::get('/positions/create', 'PositionsController@create')->name('positions.create');
        Route::get('/positions/download/spreadsheet', 'PositionsController@downloadSpreadsheet')->name('positions.download.spreadsheet');
        Route::get('/positions/{id}', 'PositionsController@show')->name('positions.show');
        Route::patch('/positions/{id}', 'PositionsController@update');
        Route::delete('/positions/{id}', 'PositionsController@destroy');
        Route::get('/positions/{id}/edit', 'PositionsController@edit')->name('positions.edit');
        Route::get('/positions/{id}/delete', 'PositionsController@delete')->name('positions.delete');
        Route::get('/positions/getSkills/{id}', 'PositionsController@getSkills')->name('positions.getSkills');
    });
});

Route::prefix('auth')->group(function()  {
    Route::group(['middleware' => 'auth'], function () {
        // Logout Route...
    });

    // Two-Factor Authentication Routes...
    Route::get('/checkpoint', 'Auth\LoginController@getValidateToken')->name('checkpoint');
    Route::post('/checkpoint', ['middleware' => 'throttle:5', 'uses' => 'Auth\LoginController@postValidateToken']);

    // Registration Routes...
    Route::get('/register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    Route::get('/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
});