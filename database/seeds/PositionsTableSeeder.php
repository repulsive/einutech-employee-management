<?php

use Illuminate\Database\Seeder;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $position1 = \App\Models\Position::create(['name' => 'Senior Backend Developer', 'state' => false, 'skills' => ['PHP', 'MySQL', 'Laravel', 'CodeIgniter']]);
        $position2 = \App\Models\Position::create(['name' => 'Medior Backend Developer', 'state' => false, 'skills' => ['PHP', 'MySQL', 'Laravel']]);
        $position3 = \App\Models\Position::create(['name' => 'Junior Backend Developer', 'state' => false, 'skills' => ['PHP', 'MySQL']]);
        $position4 = \App\Models\Position::create(['name' => 'Senior Frontend Developer', 'state' => false, 'skills' => null]);
        $position5 = \App\Models\Position::create(['name' => 'Medior Frontend Developer', 'state' => false, 'skills' => null]);
        $position6 = \App\Models\Position::create(['name' => 'Junior Frontend Developer', 'state' => false, 'skills' => null]);

        $company = \App\Models\Company::where('name', 'Einutech')->first();

        $position1->company()->associate($company);
        $position2->company()->associate($company);
        $position3->company()->associate($company);
        $position4->company()->associate($company);
        $position5->company()->associate($company);
        $position6->company()->associate($company);

        $position1->save();
        $position2->save();
        $position3->save();
        $position4->save();
        $position5->save();
        $position6->save();
    }
}
