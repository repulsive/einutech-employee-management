<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Role::create(['name' => 'Agent', 'level' => '1']);
        \App\Models\Role::create(['name' => 'Manager', 'level' => '2']);
        \App\Models\Role::create(['name' => 'Administrator', 'level' => '3']);
        \App\Models\Role::create(['name' => 'Owner', 'level' => '4']);
    }
}
