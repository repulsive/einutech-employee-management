<?php

use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = \App\Models\Company::create(
            ['name' => 'Einutech', 'slug' => 'einutech',
             'email' => 'office@einutech.com', 'phone' => '0601467460',
             'address' => '22. Decembra 8', 'postal' => '31330',
             'city' => 'Priboj', 'state' => 'Zlatiborski Okrug',
             'tax_id_number' => 0, 'company_number' => 0,
             'activity_code' => 0]
        );

        $country = \App\Models\Country::where('code', 'RS')->first();
        $company->country()->associate($country);
        $company->save();
    }
}

/*

{
    "_id" : ObjectId("5ad2e88d1bd3a31b680040f0"),
    "code" : "US",
    "name" : "United States"
}
{
    "_id" : ObjectId("5ad30ac81bd3a32bf40064af"),
    "code" : "US",
    "name" : "United States"
}

 */