<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\Models\User::create(
            ['name' => 'Einutech Test', 'email' => 'test@einutech.com', 'password' => bcrypt('test')]
        );

        $managerRole = \App\Models\Role::where('name', 'Manager')->first();
        $administratorRole = \App\Models\Role::where('name', 'Administrator')->first();

        $user->assignRole($managerRole);
        $user->assignRole($administratorRole);

        $company = \App\Models\Company::where('name', 'Einutech')->first();

        $user->company()->associate($company);
        $user->save();
    }
}
