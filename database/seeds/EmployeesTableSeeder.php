<?php

use Illuminate\Database\Seeder;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $employee = \App\Models\Employee::create(
            ['company_id' => 1, 'country_id' => 189, 'position_id' => 1,
             'first_name' => 'Jovan', 'last_name' => 'Ivanovic',
             'email' => 'jovanivanovic@einutech.com', 'phone' => '0601467460',
             'address' => '22. Decembra 8', 'postal' => '31330',
             'city' => 'Priboj', 'State' => 'Zlatiborski Okrug',
             'sex' => 'male', 'umcn' => '0405997793412',
             'birth_date' => '2017-12-31', 'start_date' => '2017-12-31',
             'skills' => [
                'PHP' => 24, 'MySQL' => 89, 'Laravel' => 44, 'CodeIgniter' => 7
             ]]
        );

        $company = \App\Models\Company::where('name', 'Einutech')->first();
        $country = \App\Models\Country::where('code', 'RS')->first();
        $position = \App\Models\Position::where('name', 'Senior Backend Developer')->first();

        $employee->company()->associate($company);
        $employee->country()->associate($country);
        $employee->position()->associate($position);

        $employee->save();
    }
}
