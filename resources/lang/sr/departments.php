<?php

return [
    'title.basic' => 'Osnovne informacije',
    'title.location' => 'Lokacija',
    'title.main' => 'Odseci',
    'title.info' => 'Informacije o odseku',
    'title.delete' => 'Potvrdi brisanje',

    'action.new' => 'Novi odsek',
    'action.show' => 'Prikaži odsek',
    'action.edit' => 'Izmeni odsek',
    'action.delete' => 'Izbriši odsek',
    'action.download' => 'Preuzmi Excel tabelu',
    'action.deletebulk' => 'Izbriši odabrane stavke',
    'action.submit' => 'Unesi',
    'action.yes' => 'Da',
    'action.no' => 'Ne',

    'field.id' => 'ID',
    'field.name' => 'Naziv',
    'field.email' => 'E-pošta',
    'field.phone' => 'Telefon',
    'field.address' => 'Adresa',
    'field.city' => 'Grad',
    'field.postal' => 'Poštanski broj',
    'field.state' => 'Okrug',
    'field.country' => 'Država',
    'field.options' => 'Opcije',
    'field.continue.label' => 'Nastavi sa unosom',

    'message.empty' => 'Lista odseka je prazna',
    'message.delete' => 'Da li ste sigurni da želite da izbrišete odsek ":name" iz baze podataka?',
    'message.deletebulk' => 'Da li ste sigurno da želite da izbrišete odabrane odseke iz baze podataka?',
    'message.required' => 'Obavezna polja.'
];