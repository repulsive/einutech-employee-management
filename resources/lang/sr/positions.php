<?php

return [
    'title.main' => 'Pozicije',
    'title.info' => 'Informacije o poziciji',
    'title.delete' => 'Potvrdi brisanje',

    'action.new' => 'Nova pozicija',
    'action.show' => 'Prikaži poziciju',
    'action.edit' => 'Izmeni poziciju',
    'action.delete' => 'Izbriši poziciju',
    'action.download' => 'Preuzmi Excel tabelu',
    'action.deletebulk' => 'Izbriši odabrane stavke',
    'action.submit' => 'Unesi',
    'action.yes' => 'Da',
    'action.no' => 'Ne',
    'action.apply' => 'Apliciraj',

    'field.id' => 'ID',
    'field.name' => 'Naziv',
    'field.department' => 'Odsek',
    'field.description' => 'Opis',
    'field.state' => 'Status',
    'field.state.label' => 'Otvorena pozicija',
    'field.state.open' => 'Otvorena',
    'field.state.closed' => 'Zatvorena',
    'field.deadline' => 'Rok',
    'field.options' => 'Opcije',
    'field.continue.label' => 'Nastavi sa unosom',

    'error.department.unavailable' => 'Ne pripada odseku',

    'message.empty' => 'Lista pozicija je prazna',
    'message.no_open' => 'Trenutno nema otvorenih pozicija.',
    'message.delete' => 'Da li ste sigurni da želite da izbrišete poziciju ":name" iz baze podataka?',
    'message.deletebulk' => 'Da li ste sigurno da želite da izbrišete odabrane pozicije iz baze podataka?',
];