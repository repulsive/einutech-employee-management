<?php

return [
    'title.main' => 'Kompanije',
    'title.personal' => 'Informacije',
    'title.location' => 'Lokacija',

    'action.new' => 'Nova kompanija',
    'action.show' => 'Prikaži kompaniju',
    'action.edit' => 'Izmeni kompaniju',
    'action.delete' => 'Izbriši kompaniju',
    'action.download' => 'Preuzmi Excel tabelu',
    'action.deletebulk' => 'Izbriši odabrane stavke',
    'action.field' => 'Novo polje',
    'action.submit' => 'Unesi',
    'action.yes' => 'Da',
    'action.no' => 'Ne',

    'field.id' => 'ID',
    'field.name' => 'Naziv',
    'field.slug' => 'URL prikaz',
    'field.email' => 'E-pošta',
    'field.phone' => 'Telefon',
    'field.company_number' => 'Matični broj',
    'field.tax_id_number' => 'PIB',
    'field.activity_code' => 'Šifra delatnosti',
    'field.address' => 'Adresa',
    'field.city' => 'Grad',
    'field.postal' => 'Poštanski broj',
    'field.state' => 'Okrug',
    'field.country' => 'Država',
    'field.options' => 'Opcije',

    'error.company_number.unavailable' => 'Nema Matični broj',
    'error.tax_id_number.unavailable' => 'Nema PIB',
];