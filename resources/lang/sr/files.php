<?php

return [
    'action.upload.files' => 'Otpremi datoteke',

    'field.name' => 'Naziv',
    'field.options' => 'Opcije',

    'message.empty' => 'Lista datoteka je prazna',
];