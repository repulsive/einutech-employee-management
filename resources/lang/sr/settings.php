<?php

return [
    'title.main' => 'Podešavanja',
    'title.change_password' => 'Promeni lozinku',
    'title.tfa' => 'Autentikacija u dva koraka',

    'page.account' => 'Nalog',
    'page.appearance' => 'Izgled',
    'page.security' => 'Bezbednost',

    'field.current_password' => 'Trenutna lozinka',
    'field.new_password' => 'Nova lozinka',
    'field.repeat_password' => 'Ponovi lozinku',
];