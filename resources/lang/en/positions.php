<?php

return [
    'title.main' => 'Positions',
    'title.info' => 'Position information',
    'title.delete' => 'Confirm delete',

    'action.new' => 'New position',
    'action.show' => 'Show position',
    'action.edit' => 'Edit position',
    'action.delete' => 'Delete position',
    'action.download' => 'Download spreadsheet',
    'action.deletebulk' => 'Delete selected items',
    'action.submit' => 'Submit',
    'action.yes' => 'Yes',
    'action.no' => 'No',

    'field.id' => 'ID',
    'field.name' => 'Name',
    'field.description' => 'Description',
    'field.state' => 'State',
    'field.state.label' => 'Open position',
    'field.state.open' => 'Open',
    'field.state.closed' => 'Closed',
    'field.options' => 'Options',
    'field.continue.label' => 'Continue input',

    'message.empty' => 'Position list is empty',
    'message.delete' => 'Are you sure you want to delete position ":name" from the database?',
    'message.deletebulk' => 'Are you sure you want to delete selected positions from the database?',
];