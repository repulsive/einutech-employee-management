<?php

return [
    'title.main' => 'Employees',
    'title.personal' => 'Personal Information',
    'title.location' => 'Location Information',
    'title.job' => 'Job Information',
    'title.additional' => 'Additional Fields',
    'title.delete' => 'Confirm delete',
    'title.files' => 'Files',

    'action.new' => 'New employee',
    'action.show' => 'Show employee',
    'action.edit' => 'Edit employee',
    'action.delete' => 'Delete employee',
    'action.download' => 'Download spreadsheet',
    'action.deletebulk' => 'Delete selected items',
    'action.field' => 'New field',
    'action.submit' => 'Submit',
    'action.yes' => 'Yes',
    'action.no' => 'No',

    'field.id' => 'ID',
    'field.first_name' => 'First Name',
    'field.last_name' => 'Last Name',
    'field.email' => 'Email',
    'field.phone' => 'Phone',
    'field.birth_date' => 'Birth Date',
    'field.umcn' => 'UMCN',
    'field.sex' => 'Sex',
    'field.sex.male' => 'Male',
    'field.sex.female' => 'Female',
    'field.address' => 'Address',
    'field.city' => 'City',
    'field.postal' => 'Postal',
    'field.state' => 'State',
    'field.country' => 'Country',
    'field.start_date' => 'Start Date',
    'field.position' => 'Position',
    'field.custom.key' => 'Field Name',
    'field.custom.value' => 'Field Value',
    'field.options' => 'Options',
    'field.continue.label' => 'Continue input',

    'error.position.unavailable' => 'No position',

    'message.empty' => 'Employee list is empty',
    'message.delete' => 'Are you sure you want to delete employee ":name" from the database?',
    'message.deletebulk' => 'Are you sure you want to delete selected employees from the database?',
];