@extends('layouts.print')
@section('pageTitle', __('employees.title.main').': '.$employee->first_name.' '.$employee->last_name)
@section('screen', 'print')
@section('sidebar_category', 'employees')

@section('content')
    <script>(function(){window.print();})();</script>
    <einu-heading size="1">
        {{ __('employees.title.personal') }}
    </einu-heading>
    <einu-control-group>
        <einu-col size="M4 T4 D4">
            <img src="" />
        </einu-col>
        <einu-col size="M8 T8 D8">
            <einu-col size="M12 T12 D12">
                <label for="first_name">{{ __('employees.field.first_name') }}</label>
                <input disabled type="text" id="first_name" value="{{ $employee->first_name }}" />
            </einu-col>
            <einu-col size="M12 T12 D12">
                <label for="last_name">{{ __('employees.field.last_name') }}</label>
                <input disabled type="text" id="last_name" value="{{ $employee->last_name }}" />
            </einu-col>
        </einu-col>
    </einu-control-group>

    <einu-control-group>
        <einu-col size="M6 T6 D6">
            <label for="email">{{ __('employees.field.email') }}</label>
            <input disabled type="email" id="email" value="{{ $employee->email }}" />
        </einu-col>
        <einu-col size="M6 T6 D6">
            <label for="phone">{{ __('employees.field.phone') }}</label>
            <input disabled type="text" id="phone" value="{{ $employee->phone }}" />
        </einu-col>
    </einu-control-group>

    <einu-control-group>
        <einu-col size="M4 T4 D4">
            <label for="birth_date">{{ __('employees.field.birth_date') }}</label>
            <input disabled type="text" id="birth_date" value="{{ $employee->birth_date }}" />
        </einu-col>
        <einu-col size="M4 T4 D4">
            <label for="umcn">{{ __('employees.field.umcn') }}</label>
            <input disabled type="text" id="umcn" value="{{ $employee->umcn }}" />
        </einu-col>
        <einu-col size="M4 T4 D4">
            <label for="sex">{{ __('employees.field.sex') }}</label>
            <input disabled type="text" id="sex" value="{{ $employee->sex == 1 ? __('employees.field.sex.male') : __('employees.field.sex.female') }}" />
        </einu-col>
    </einu-control-group>

    <einu-heading size="1">{{ __('employees.title.location') }}</einu-heading>
    <einu-control-group>
        <einu-col size="M4 T4 D4">
            <label for="address">{{ __('employees.field.address') }}</label>
            <input disabled type="text" id="address" value="{{ $employee->address }}" />
        </einu-col>
        <einu-col size="M4 T4 D4">
            <label for="city">{{ __('employees.field.city') }}</label>
            <input disabled type="text" id="city" value="{{ $employee->city }}" />
        </einu-col>
        <einu-col size="M4 T4 D4">
            <label for="postal">{{ __('employees.field.postal') }}</label>
            <input disabled type="text" id="postal" value="{{ $employee->postal }}" />
        </einu-col>
    </einu-control-group>


    <einu-control-group>
        <einu-col size="M6 T6 D6">
            <label for="state">{{ __('employees.field.state') }}</label>
            <input disabled type="text" id="state" value="{{ $employee->state }}" />
        </einu-col>
        <einu-col size="M6 T6 D6">
            <label for="country">{{ __('employees.field.country') }}</label>
            <input disabled type="text" id="country" value="{{ $employee->country->name }}" />
        </einu-col>
    </einu-control-group>

    <einu-heading size="1">{{ __('employees.title.job') }}</einu-heading>
    <einu-control-group>
        <einu-col size="M6 T6 D6">
            <label for="start_date">{{ __('employees.field.start_date') }}</label>
            <input disabled type="text" id="start_date" value="{{ $employee->start_date }}" />
        </einu-col>
        <einu-col size="M6 T6 D6">
            <label for="position">{{ __('employees.field.position') }}</label>
            <input disabled type="text" id="position" value="{{ $employee->position != null ? $employee->position->name : __('employees.error.position.unavailable') }}" />
        </einu-col>
    </einu-control-group>

    @if (!empty($custom_fields))
        <einu-heading size="1">{{ __('employees.title.additional') }}</einu-heading>
        <einu-block style="padding: 0;">
            <?php $count = 0; ?>

            @foreach ($custom_fields as $key => $value)
                <?php $count++; ?>

                @if ($count % 2 != 0)
                    <einu-control-group class="field">
                        @endif
                        <einu-col size="M6 T6 D6">
                            <label for="{{ $key }}">{{ $key }}</label>
                            <input disabled type="text" id="{{ $key }}" value="{{ $value }}" />
                        </einu-col>
                        @if ($count % 2 == 0)
                    </einu-control-group>
                @endif
            @endforeach
        </einu-block>
    @endif
@endsection