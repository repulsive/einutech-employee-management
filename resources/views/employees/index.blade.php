@extends('layouts.app')
@section('pageTitle', __('employees.title.main'))
@section('screen', 'app')
@section('sidebar_category', 'employees')

@section('content')
    <form action="{{ route('employees.bulkdelete') }}" method="POST">
        {{ csrf_field() }}

        <einu-block class="section">
            <einu-heading size="1">
                {{ __('employees.title.main') }}

                <a href="{{ route('employees.download.spreadsheet') }}" class="alternative left">
                    <einu-icon code="download"></einu-icon> <span>{{ __('employees.action.download') }} (.xlsx)</span>
                </a>
                <a href="{{ route('employees.create') }}" class="alternative right green">
                    <einu-icon code="plus"></einu-icon> <span>{{ __('employees.action.new') }}</span>
                </a>
            </einu-heading>
            {!! $dataGrid->show() !!}

            {{--<einu-table style="width: 100%;">--}}
                {{--<einu-table-header>--}}
                    {{--<einu-table-cell>--}}
                        {{--<script>function toggleAllCheckboxes(element){$(element).prop('checked')==1?$('.selectcheck').prop('checked',!0):$('.selectcheck').prop('checked',!1);}</script>--}}
                        {{--<input type="checkbox" onchange="toggleAllCheckboxes(this)"/>--}}
                    {{--</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('employees.field.id') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('employees.field.first_name') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('employees.field.last_name') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('employees.field.email') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('employees.field.department') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('employees.field.position') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('employees.field.score') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('employees.field.options') }}</einu-table-cell>--}}
                {{--</einu-table-header>--}}
                {{--@forelse ($employees as $employee)--}}
                    {{--<einu-table-row>--}}
                        {{--<einu-table-cell>--}}
                            {{--<input type="checkbox" class="selectcheck" name="selected[]" value="{{ $employee->id }}" />--}}
                        {{--</einu-table-cell>--}}
                        {{--<einu-table-cell style="font-family: 'Source Code Pro';">{{ $employee->id }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{{ $employee->first_name }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{{ $employee->last_name }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{{ $employee->email }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{!! ($employee->position !== null) ? ($employee->position->department !== null) ? $employee->position->department->name : '<span class="error"><einu-icon code="exclamation-triangle"></einu-icon> '.__('employees.error.department.unavailable').'</span>' : '<span class="error"><einu-icon code="exclamation-triangle"></einu-icon> '.__('employees.error.department.unavailable').'</span>' !!}</einu-table-cell>--}}
                        {{--<einu-table-cell>{!! ($employee->position !== null) ? $employee->position->name : '<span class="error"><einu-icon code="exclamation-triangle"></einu-icon> '.__('employees.error.position.unavailable').'</span>' !!}</einu-table-cell>--}}
                        {{--<einu-table-cell><span class="score">{{ $employee->score }}</span></einu-table-cell>--}}
                        {{--<einu-table-cell style="width: 125px;">--}}
                            {{--<a class="button squared rounded" href="{{ route('employees.show', $employee->id) }}"><einu-icon code="eye"></einu-icon></a>--}}
                            {{--<a class="button squared rounded" href="{{ route('employees.edit', $employee->id) }}"><einu-icon code="pencil"></einu-icon></a>--}}
                            {{--<a class="button squared rounded red" href="{{ route('employees.delete', $employee->id) }}"><einu-icon code="trash-o"></einu-icon></a>--}}
                        {{--</einu-table-cell>--}}
                    {{--</einu-table-row>--}}
                {{--@empty--}}
                    {{--@section('empty.message')--}}
                        {{--<einu-textblock class="message">{{ __('employees.message.empty') }}</einu-textblock>--}}
                    {{--@endsection--}}
                {{--@endforelse--}}
            {{--</einu-table>--}}
            {{--@yield('empty.message')--}}
        </einu-block>
        <einu-block class="action-bar">
            <einu-control-group>
                <button class="red rounded" type="submit">
                    {{ __('employees.action.deletebulk') }}
                </button>
            </einu-control-group>
        </einu-block>
    </form>
@endsection

@push('styles')
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.css" />
@endpush

@push('scripts')
    <script>
        {{--var $data = {!! $employees !!};--}}
//        var $employees = $data.data;

        $(".datagrid").jsGrid({
            width: "100%",
            height: "auto",
            autowidth: true,
            sorting: true,
            paging: true,

            data: $employees,

            fields: [
                { name: "first_name", title: "{{ __('employees.field.first_name') }}" },
                { name: "last_name", title: "{{ __('employees.field.last_name') }}" },
                { name: "email", title: "{{ __('employees.field.email') }}" },
                { name: "department_name", title: "{{ __('employees.field.department') }}" },
                { name: "position_name", title: "{{ __('employees.field.position') }}" },
                { name: "score", title: "{{ __('employees.field.score') }}" },
                { name: "options", title: "{{ __('employees.field.options') }}" }
            ]
        });
    </script>
@endpush

{{--<einu-table-cell>{{ __('employees.field.id') }}</einu-table-cell>--}}
{{--<einu-table-cell>{{ __('employees.field.first_name') }}</einu-table-cell>--}}
{{--<einu-table-cell>{{ __('employees.field.last_name') }}</einu-table-cell>--}}
{{--<einu-table-cell>{{ __('employees.field.email') }}</einu-table-cell>--}}
{{--<einu-table-cell>{{ __('employees.field.department') }}</einu-table-cell>--}}
{{--<einu-table-cell>{{ __('employees.field.position') }}</einu-table-cell>--}}
{{--<einu-table-cell>{{ __('employees.field.score') }}</einu-table-cell>--}}
{{--<einu-table-cell>{{ __('employees.field.options') }}</einu-table-cell>--}}