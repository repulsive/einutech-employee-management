@extends('layouts.app')
@section('pageTitle', __('employees.title.main').': '.$employee->first_name.' '.$employee->last_name)
@section('screen', 'app')
@section('sidebar_category', 'employees')

@section('content')
    <form class="einu-form">
        {{ csrf_field() }}

        <einu-heading size="1">
            {{ __('employees.title.personal') }}

            <a class="alternative left" href="{{ route('employees.edit', $employee->id) }}">
                <einu-icon code="pencil"></einu-icon> <span>{{ __('employees.action.edit') }}</span>
            </a>
            <a class="alternative right red" href="{{ route('employees.delete', $employee->id) }}">
                <einu-icon code="trash-o"></einu-icon> <span>{{ __('employees.action.delete') }}</span>
            </a>
        </einu-heading>
        <einu-control-group>
            <einu-col size="M12 T3 D3" class="image-container">
                <einu-block class="image employee" style="background-image: url('{{ route('images.show', $image != null ? $image->id : 'avatar') }}');"></einu-block>
            </einu-col>
            <einu-col size="M12 T8 D8" offset="M0 T1 D1">
                <einu-control-group>
                    <einu-col size="M12 T12 D12">
                        <label for="first_name">{{ __('employees.field.first_name') }}</label>
                        <input disabled type="text" id="first_name" value="{{ $employee->first_name }}" />
                    </einu-col>
                </einu-control-group>
                <einu-control-group>
                    <einu-col size="M12 T12 D12">
                        <label for="last_name">{{ __('employees.field.last_name') }}</label>
                        <input disabled type="text" id="last_name" value="{{ $employee->last_name }}" />
                    </einu-col>
                </einu-control-group>
                <einu-control-group>
                    <einu-col size="M12 T12 D12">
                        <label for="umcn">{{ __('employees.field.umcn') }}</label>
                        <input disabled type="text" id="umcn" value="{{ $employee->umcn }}" />
                    </einu-col>
                </einu-control-group>
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M12 T6 D6">
                <label for="email">{{ __('employees.field.email') }}</label>
                <input disabled type="email" id="email" value="{{ $employee->email }}" />
            </einu-col>
            <einu-col size="M12 T6 D6">
                <label for="phone">{{ __('employees.field.phone') }}</label>
                <input disabled type="text" id="phone" value="{{ $employee->phone }}" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M12 T6 D6">
                <label for="birth_date">{{ __('employees.field.birth_date') }}</label>
                <input disabled type="text" id="birth_date" value="{{ $employee->birth_date }}" />
            </einu-col>
            <einu-col size="M12 T6 D6">
                <label for="sex">{{ __('employees.field.sex') }}</label>
                <input disabled type="text" id="sex" value="{{ $employee->sex == 1 ? __('employees.field.sex.male') : __('employees.field.sex.female') }}" />
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">{{ __('employees.title.location') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M12 T4 D4">
                <label for="address">{{ __('employees.field.address') }}</label>
                <input disabled type="text" id="address" value="{{ $employee->address }}" />
            </einu-col>
            <einu-col size="M12 T4 D4">
                <label for="city">{{ __('employees.field.city') }}</label>
                <input disabled type="text" id="city" value="{{ $employee->city }}" />
            </einu-col>
            <einu-col size="M12 T4 D4">
                <label for="postal">{{ __('employees.field.postal') }}</label>
                <input disabled type="text" id="postal" value="{{ $employee->postal }}" />
            </einu-col>
        </einu-control-group>


        <einu-control-group>
            <einu-col size="M12 T6 D6">
                <label for="state">{{ __('employees.field.state') }}</label>
                <input disabled type="text" id="state" value="{{ $employee->state }}" />
            </einu-col>
            <einu-col size="M12 T6 D6">
                <label for="country">{{ __('employees.field.country') }}</label>
                <input disabled type="text" id="country" value="{{ $employee->country->name }}" />
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">{{ __('employees.title.job') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M12 T4 D4">
                <label for="start_date">{{ __('employees.field.start_date') }}</label>
                <input disabled type="text" id="start_date" value="{{ $employee->start_date }}" />
            </einu-col>
            <einu-col size="M12 T4 D4">
                <label for="department">{{ __('employees.field.department') }}</label>
                <input disabled type="text" id="department" value="{{ $employee->position != null ? $employee->position->department != null ? $employee->position->department->name : __('employees.error.department.unavailable') : __('employees.error.department.unavailable') }}" />
            </einu-col>
            <einu-col size="M12 T4 D4">
                <label for="position">{{ __('employees.field.position') }}</label>
                <input disabled type="text" id="position" value="{{ $employee->position != null ? $employee->position->name : __('employees.error.position.unavailable') }}" />
            </einu-col>
        </einu-control-group>

        @if ($employee->skills != null && $employee->position->skills != null)
            <einu-heading size="1">
                {{ __('employees.title.skills') }}
            </einu-heading>
            <einu-control-group>
                <einu-col size="M12 T6 D6">
                    @foreach ($employee->allSkills() as $skill)
                        <einu-control-group>
                            <label for="skill">{{ $skill }}</label>
                            <einu-input>
                                <?php
                                $class = '';

                                if (in_array($skill, $employee->position->skills)) {
                                    if (array_key_exists($skill, $employee->skills)) {
                                        $class = '';
                                    } else {
                                        $class = 'red';
                                    }
                                } else {
                                    if (array_key_exists($skill, $employee->skills)) {
                                        $class = 'yellow';
                                    }
                                }

                                if (array_key_exists($skill, $employee->skills)) {
                                    $value = number_format($employee->skills[$skill], 2);
                                } else {
                                    $value = 'n/a';
                                }
                                ?>


                                <einu-slider disabled class="{{ $class }}" index="{{ $skill }}" default="" name="{{ $skill }}" value="{{ $value }}"></einu-slider>
                                <label>{{ $value }}</label>
                            </einu-input>
                        </einu-control-group>
                    @endforeach
                </einu-col>
                <einu-col size="M12 T6 D6">
                    <canvas id="skillsChart" style="width: 100%;" height="200"></canvas>
                    <einu-block class="score">
                        <einu-heading size="2">{{ $employee->score }}</einu-heading>
                        <label>{{ __('employees.field.score') }}</label>
                    </einu-block>
                </einu-col>
            </einu-control-group>
        @endif

        @if (!empty($custom_fields))
            <einu-heading size="1">{{ __('employees.title.additional') }}</einu-heading>
            <einu-block style="padding: 0;">
                <?php $count = 0; ?>

                @foreach ($custom_fields as $key => $value)
                    <?php $count++; ?>

                    @if ($count % 2 != 0)
                        <einu-control-group class="field">
                    @endif
                        <einu-col size="M12 T6 D6">
                            <label for="{{ $key }}">{{ $key }}</label>
                            <input disabled type="text" id="{{ $key }}" value="{{ $value }}" />
                        </einu-col>
                    @if ($count % 2 == 0)
                        </einu-control-group>
                    @endif
                @endforeach
            </einu-block>
        @endif

        @if ($employee->files->count() > 0)
            <einu-heading size="1">
                {{ __('employees.title.files') }}
            </einu-heading>
            <einu-block class="section">
                <einu-table style="width: 100%;" class="datagrid    ">
                    <einu-table-header>
                        <einu-table-cell></einu-table-cell>
                        <einu-table-cell>{{ __('files.field.name') }}</einu-table-cell>
                        <einu-table-cell>{{ __('files.field.options') }}</einu-table-cell>
                    </einu-table-header>
                    @forelse ($employee->files as $file)
                        <einu-table-row>
                            <einu-table-cell><img src="{{ $file->icon }}" /></einu-table-cell>
                            <einu-table-cell style="font-family: 'Source Code Pro', monospace; font-size: 13px;">{{ $file->name }}</einu-table-cell>
                            <einu-table-cell style="width: 55px;">
                                <a class="button squared rounded" href="{{ route('files.download', $file->id) }}"><einu-icon code="download"></einu-icon></a>
                            </einu-table-cell>
                        </einu-table-row>
                    @empty
                    @section('empty.message')
                        <einu-textblock class="message">{{ __('files.message.empty') }}</einu-textblock>
                    @endsection
                    @endforelse
                </einu-table>
                @yield('empty.message')
            </einu-block>
        @endif
    </form>
@endsection

@push('scripts')
<script src="{{ asset('js/chart.bundle.min.js') }}"></script>
<script>
    var ctx = $("#skillsChart");

    var $labels = {!! json_encode($employee->position->skills) !!};
    var $dataObject = {!! json_encode($employee->skills) !!};
    var $data = [];

    $labels.forEach(function (element) { $data.push($dataObject[element]); });

    var skillsChart = new Chart(ctx, {
        type: 'radar',
        data: {
            labels: $labels,
            datasets: [
                {
                    data: $data,
                    backgroundColor: 'rgba(39, 181, 86, 0.4)',
                    borderColor: 'rgb(39, 181, 86)'
                }
            ]
        },
        options: {
            legend: {
                display: false
            },
            scale: {
                ticks: {
                    beginAtZero: true,
                    max: 100,
                    stepSize:50
                }
            }
        }
    });
</script>
@endpush