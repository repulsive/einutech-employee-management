@extends('layouts.app')
@section('pageTitle', __('employees.title.main').': Edit')
@section('screen', 'app')
@section('sidebar_category', 'employees')

@section('content')
    <form class="einu-form" action="{{ route('employees.show', $employee->id) }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('patch') }}

        <einu-heading size="1">
            {{ __('employees.title.personal') }}

            <a class="alternative left red" href="{{ route('employees.delete', $employee->id) }}">
                <einu-icon code="trash-o"></einu-icon> <span>{{ __('employees.action.delete') }}</span>
            </a>
            <a class="alternative right" href="{{ route('employees.show', $employee->id) }}">
                <einu-icon code="eye"></einu-icon> <span>{{ __('employees.action.show') }}</span>
            </a>
        </einu-heading>
        <einu-control-group>
            <script>
                function uploadImage(token, self) {
                    var input = self;
                    var $image = $(input).parent();
                    var imageUrl = '{{ URL::to('/images') }}/';

                    var formData = new FormData();
                    formData.append('_token', token);
                    formData.append('image', input.files[0]);

                    $.ajax({
                        url: '{{ route('employees.upload.image', $employee->id) }}',
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            $image.css('background-image', 'url(' + imageUrl + data.id + ')');
                        }
                    });
                }
            </script>
            <einu-col size="M12 T3 D3" class="image-container">
                <einu-block class="image employee editable file-input" style="background-image: url('{{ route('images.show', $image != null ? $image->id : 'avatar') }}');">
                    <einu-block class="hover">
                        <einu-icon code="photo"></einu-icon>
                    </einu-block>
                    <input type="file" name="image" onchange="uploadImage('{{ csrf_token() }}', this)" />
                </einu-block>
            </einu-col>
            <einu-col size="M12 T8 D8" offset="M0 T1 D1">
                <einu-control-group>
                    <einu-col size="M12 T12 D12">
                        <label for="first_name">{{ __('employees.field.first_name') }}<span class="required"> •</span></label>
                        <input type="text" name="first_name" id="first_name" value="{{ $employee->first_name }}" />
                    </einu-col>
                </einu-control-group>
                <einu-control-group>
                    <einu-col size="M12 T12 D12">
                        <label for="last_name">{{ __('employees.field.last_name') }}<span class="required"> •</span></label>
                        <input type="text" name="last_name" id="last_name" value="{{ $employee->last_name }}" />
                    </einu-col>
                </einu-control-group>
                <einu-control-group>
                    <einu-col size="M12 T12 D12">
                        <label for="umcn">{{ __('employees.field.umcn') }}<span class="required"> •</span></label>
                        <input type="text" name="umcn" id="umcn" value="{{ $employee->umcn }}" />
                    </einu-col>
                </einu-control-group>
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M12 T6 D6">
                <label for="email">{{ __('employees.field.email') }}<span class="required"> •</span></label>
                <input type="email" name="email" id="email" value="{{ $employee->email }}" />
            </einu-col>
            <einu-col size="M12 T6 D6">
                <label for="phone">{{ __('employees.field.phone') }}<span class="required"> •</span></label>
                <input type="text" name="phone" id="phone" value="{{ $employee->phone }}" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M12 T6 D6">
                <label for="birth_date">{{ __('employees.field.birth_date') }}<span class="required"> •</span></label>
                <input type="date" name="birth_date" id="birth_date" value="{{ $employee->birth_date }}" />
            </einu-col>
            <einu-col size="M12 T6 D6">
                <label for="sex">{{ __('employees.field.sex') }}<span class="required"> •</span></label>
                <select name="sex" id="sex">
                    <option value="1" {{ $employee->sex == 1 ? 'selected' : '' }}>{{ __('employees.field.sex.male') }}</option>
                    <option value="2" {{ $employee->sex == 2 ? 'selected' : '' }}>{{ __('employees.field.sex.female') }}</option>
                </select>
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">{{ __('employees.title.location') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M12 T4 D4">
                <label for="address">{{ __('employees.field.address') }}<span class="required"> •</span></label>
                <input type="text" name="address" id="address" value="{{ $employee->address }}" />
            </einu-col>
            <einu-col size="M12 T4 D4">
                <label for="city">{{ __('employees.field.city') }}<span class="required"> •</span></label>
                <input type="text" name="city" id="city" value="{{ $employee->city }}" />
            </einu-col>
            <einu-col size="M12 T4 D4">
                <label for="postal">{{ __('employees.field.postal') }}<span class="required"> •</span></label>
                <input type="text" name="postal" id="postal" value="{{ $employee->postal }}" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M12 T6 D6">
                <label for="state">{{ __('employees.field.state') }}<span class="required"> •</span></label>
                <input type="text" name="state" id="state" value="{{ $employee->state }}" />
            </einu-col>
            <einu-col size="M12 T6 D6">
                <label for="country">{{ __('employees.field.country') }}<span class="required"> •</span></label>
                <select name="country_id" id="country">
                    @foreach ($countries as $country)
                        @if ($employee->country_id == $country->id)
                            <option selected value="{{ $country->id }}">{{ $country->name }}</option>
                        @else
                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                        @endif
                    @endforeach
                </select>
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">{{ __('employees.title.job') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M12 T6 D6">
                <label for="start_date">{{ __('employees.field.start_date') }}<span class="required"> •</span></label>
                <input type="date" name="start_date" id="start_date" value="{{ $employee->start_date }}" />
            </einu-col>
            <einu-col size="M12 T6 D6">
                <label for="position">{{ __('employees.field.position') }}</label>
                <script>
                    function changePosition(self) {
                        var $this = $(self);
                        var $selected = $this.children('option:selected');

                        var $id = $selected.val();
                        var $url = '{{ URL::to('positions/getSkills/') }}/' + $id;

                        $.ajax({
                            url: $url,
                            type: 'GET',
                            success: function(data) {
                                window.$labels = data;
                                window.$data = [];

                                window.skillsChart.config.data.labels = data;
                                window.$labels.forEach(function (element) { window.$data.push(window.$dataObject[element]); });
                                window.skillsChart.config.data.datasets[0].data = window.$data;

                                window.calculateScore();
                                window.setActiveSliders();

                                window.skillsChart.update();
                            }
                        });
                    }
                </script>
                <select name="position_id" id="position" onchange="changePosition(this)">
                    <option></option>
                    @foreach ($positions as $position)
                        @if ($employee->position_id == $position->id)
                            <option selected value="{{ $position->id }}">{{ $position->name }}</option>
                        @else
                            <option value="{{ $position->id }}">{{ $position->name }}</option>
                        @endif
                    @endforeach
                    @foreach ($departments as $department)
                        @if ($department->positions->count() > 0)
                            <optgroup label="{{ $department->name }}">
                                @foreach ($department->positions as $position)
                                    @if ($employee->position_id == $position->id)
                                        <option selected value="{{ $position->id }}">{{ $position->name }}</option>
                                    @else
                                        <option value="{{ $position->id }}">{{ $position->name }}</option>
                                    @endif
                                @endforeach
                            </optgroup>
                        @endif
                    @endforeach
                </select>
            </einu-col>
        </einu-control-group>

        @if ($employee->skills != null && $employee->position->skills != null)
            <einu-heading size="1">
                {{ __('employees.title.skills') }}
            </einu-heading>
            <einu-control-group>
                <einu-col size="M12 T6 D6">
                    @foreach ($employee->allSkills() as $skill)
                        <einu-control-group>
                            <label for="skill">{{ $skill }}</label>
                            <einu-input>
                                <?php
                                    $class = '';

                                    if (in_array($skill, $employee->position->skills)) {
                                        if (array_key_exists($skill, $employee->skills)) {
                                            $class = '';
                                        } else {
                                            $class = 'red';
                                        }
                                    } else {
                                        if (array_key_exists($skill, $employee->skills)) {
                                            $class = 'yellow';
                                        }
                                    }

                                    if (array_key_exists($skill, $employee->skills)) {
                                        $value = number_format($employee->skills[$skill], 2);
                                    } else {
                                        $value = 0.00;
                                    }
                                ?>


                                <einu-slider class="{{ $class }}" index="{{ $skill }}" default="{{ $value }}" name="{{ $skill }}" value="{{ $value }}"></einu-slider>
                                <input type="hidden" name="skills[{{ $skill }}]" value="{{ $value }}" />
                                <label>{{ $value }}</label>
                            </einu-input>
                        </einu-control-group>
                    @endforeach
                </einu-col>
                <einu-col size="M12 T6 D6">
                    <canvas id="skillsChart" style="width: 100%;" height="200"></canvas>
                    <einu-block class="score">
                        <einu-heading size="2">{{ $employee->score }}</einu-heading>
                        <label>{{ __('employees.field.score') }}</label>
                    </einu-block>
                </einu-col>
            </einu-control-group>
        @endif

        <einu-heading size="1">
            {{ __('employees.title.additional') }}

            <a class="alternative right" href="javascript:void(0)" onclick="add_field('custom_fields')">
                <einu-icon code="plus"></einu-icon> <span>{{ __('employees.action.field') }}</span>
            </a>
        </einu-heading>
        <einu-block style="padding: 0;" id="custom_fields">
            <script>
                function add_field(id){var custom_fields=$('#'+id);var fields_count=custom_fields.find('.field').length;var field='<einu-control-group class="field" einu-data-id="'+(fields_count+1)+'"> <einu-col size="M12 T6 D6"> <label for="custom_data_key">{{ __('employees.field.custom.key') }}</label> <input type="text" name="custom_data_key[]" id="custom_data_key"/> </einu-col> <einu-col size="M12 T6 D6"> <label for="custom_data_value">{{ __('employees.field.custom.value') }}</label> <input type="text" name="custom_data_value[]" id="custom_data_value"/> </einu-col> </einu-control-group>';custom_fields.append(field);}function remove_field(id,field){var field_to_remove=$('#'+id).find('.field[einu-data-id="'+field+'"]');field_to_remove.remove();}
            </script>
            @foreach ($custom_fields as $key => $value)
                <einu-control-group class="field">
                    <einu-col size="M12 T6 D6">
                        <label for="custom_data_key">{{ __('employees.field.custom.key') }}</label>
                        <input type="text" name="custom_data_key[]" id="custom_data_key" value="{{ $key }}" />
                    </einu-col>
                    <einu-col size="M12 T6 D6">
                        <label for="custom_data_value">{{ __('employees.field.custom.value') }}</label>
                        <input type="text" name="custom_data_value[]" id="custom_data_value" value="{{ $value }}" />
                    </einu-col>
                </einu-control-group>
            @endforeach
        </einu-block>

        <script>
            function generateFileRow(id, name, icon) {
                var downloadUrl = '{{ URL::to('files/download/') }}/' + id;
                var token = '{{ csrf_token() }}';

                var row = '<einu-table-row>'
                    + '<einu-table-cell><img src="' + icon + '" /></einu-table-cell>'
                    + '<einu-table-cell style="font-family: \'Source Code Pro\', monospace; font-size: 13px;">' + name + '</einu-table-cell>'
                    + '<einu-table-cell style="width: 90px;">'
                    + '<a class="button squared rounded red" href="javascript:void(0)" onclick="deleteFile(\'' + token + '\', this, \'' + id + '\')"><einu-icon code="trash-o"></einu-icon></a>'
                    + '<a class="button squared rounded blue" href="' + downloadUrl + '"><einu-icon code="download"></einu-icon></a>'
                    + '</einu-table-cell>'
                    + '</einu-table-row>';

                return row;
            }

            function uploadFiles(token, self) {
                var input = self;
                var $table = $('._files_table');
                var $section = $table.parent();
                var $emptyMessage = $section.find('._empty_message');

                var formData = new FormData();
                formData.append('_token', token);
                for(var key in input.files) { formData.append('files[]', input.files[key]); }

                $.ajax({
                    url: '{{ route('employees.upload.files', $employee->id) }}',
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        $emptyMessage.remove();

                        for (var file in data.files) {
                            $table.append(generateFileRow(data.files[file].id, data.files[file].name, data.files[file].icon));
                        }
                    }
                });
            }

            function deleteFile(token, self, id) {
                var button = self;
                var url = '{{ route('files.delete') }}';
                var data = { id: id, _token: token, _method: "DELETE" };

                var emptyMessage = '{{ __('files.message.empty') }}';
                var emptyHtml = '<einu-textblock class="message _empty_message">' + emptyMessage + '</einu-textblock>';

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function() {
                        var $row = $(button).parent().parent();
                        var $table = $row.parent();
                        var $section = $table.parent();

                        $row.remove();

                        if ($table.find('einu-table-row').length == 0) {
                            $section.append(emptyHtml);
                        }
                    }
                });
            }
        </script>
        <einu-heading size="1">
            {{ __('employees.title.files') }}

            <a class="alternative right blue file-input" href="javascript:void(0)">
                <einu-icon code="upload"></einu-icon> <span>{{ __('files.action.upload.files') }}</span>
                <input type="file" name="files[]" multiple onchange="uploadFiles('{{ csrf_token() }}', this)" />
            </a>
        </einu-heading>

        <einu-block class="section">
            <einu-table style="width: 100%;" class="_files_table datagrid">
                <einu-table-header>
                    <einu-table-cell></einu-table-cell>
                    <einu-table-cell>{{ __('files.field.name') }}</einu-table-cell>
                    <einu-table-cell>{{ __('files.field.options') }}</einu-table-cell>
                </einu-table-header>
                @forelse ($employee->files as $file)
                    <einu-table-row>
                        <einu-table-cell><img src="{{ $file->icon }}" /></einu-table-cell>
                        <einu-table-cell style="font-family: 'Source Code Pro', monospace; font-size: 13px;">{{ $file->name }}</einu-table-cell>
                        <einu-table-cell style="width: 90px;">
                            <a class="button squared rounded red" href="javascript:void(0)" onclick="deleteFile('{{ csrf_token() }}', this, '{{ $file->id }}')"><einu-icon code="trash-o"></einu-icon></a>
                            <a class="button squared rounded blue" href="{{ route('files.download', $file->id) }}"><einu-icon code="download"></einu-icon></a>
                        </einu-table-cell>
                    </einu-table-row>
                @empty
                    @section('empty.message')
                        <einu-textblock class="message _empty_message">{{ __('files.message.empty') }}</einu-textblock>
                    @endsection
                @endforelse
            </einu-table>
            @yield('empty.message')
        </einu-block>

        <einu-control-group style="margin-top: 20px;">
            <button type="submit" class="blue">{{ __('employees.action.submit') }}</button>
        </einu-control-group>

        <einu-control-group class="footer">
            <einu-textblock class="comment"><span style="color: #C02C44;">•</span> {{ __('employees.message.required') }}</einu-textblock>
        </einu-control-group>
    </form>
@endsection

@push('scripts')
<script src="{{ asset('js/chart.bundle.min.js') }}"></script>
<script>
    window.ctx = $("#skillsChart");

    window.$labels = {!! json_encode($employee->position->skills) !!};
    window.$dataObject = {!! json_encode($employee->skills) !!};
    window.$data = [];

    window.$labels.forEach(function (element) { window.$data.push(window.$dataObject[element]); });

    window.skillsChart = new Chart(window.ctx, {
        type: 'radar',
        data: {
            labels: window.$labels,
            datasets: [
                {
                    data: window.$data,
                    backgroundColor: 'rgba(39, 181, 86, 0.4)',
                    borderColor: 'rgb(39, 181, 86)'
                }
            ]
        },
        options: {
            legend: {
                display: false
            },
            scale: {
                ticks: {
                    beginAtZero: true,
                    max: 100,
                    stepSize:50
                }
            }
        }
    });

    $('einu-slider').on('slide', function() {
        var $this = $(this);

        var $index = $this.attr('index');
        var $name = $this.attr('name');
        var $value = $this.attr('value');

        if ($this.hasClass('red')) { $this.removeClass('red'); }

        var $input = $this.next();
        $input.val($value);

        var $label = $input.next();

        window.$data[window.$labels.indexOf($index)] = parseFloat($value);
        window.$dataObject[$index] = parseFloat($value).toFixed(2);

        window.calculateScore();

        $label.text(parseFloat($value).toFixed(2))


        window.skillsChart.update();
    });
</script>
@endpush

@push('scripts-head')
<script>
    window.calculateScore = function() {
        var $dataSum = window.$data.reduce((x, y) => parseFloat(x) + parseFloat(y));
        var $score = parseFloat(($dataSum / window.$labels.length) / 10).toFixed(1);

        var $scoreElement = $(".score > einu-heading");
        $scoreElement.text($score);

        return $score;
    }

    window.setActiveSliders = function() {
        var $sliders = $('einu-slider');

        $sliders.each(function () {
            var $element = $(this);
            var $name = $element.attr('name');

            if ($labels.indexOf($name) == -1) {
                $element.removeClass('red');
                $element.addClass('yellow');
            } else {
                $element.removeClass('red');
                $element.removeClass('yellow');
            }
        });
    }
</script>
@endpush