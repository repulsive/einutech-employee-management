@extends('layouts.app')
@section('pageTitle', __('employees.title.main').': Create')
@section('screen', 'app')
@section('sidebar_category', 'employees')

@section('content')
    <form class="einu-form" action="{{ route('employees.index') }}" method="POST">
        {{ csrf_field() }}

        <einu-heading size="1">{{ __('employees.title.personal') }}</einu-heading>
        <einu-control-group>
            <script>
                function uploadImage(token, self) {
                    var input = self;
                    var $image = $(input).parent();
                    var $hidden = $('.image > ._image_hidden');
                    var imageUrl = '{{ URL::to('/images') }}/';

                    var formData = new FormData();
                    formData.append('_token', token);
                    formData.append('image', input.files[0]);

                    $.ajax({
                        url: '{{ route('employees.upload.image', 'none') }}',
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            $image.css('background-image', 'url(' + imageUrl + data.id + ')');
                            $hidden.attr('value', data.id);
                            $hidden.val(data.id);
                            $hidden.trigger('change');
                        }
                    });
                }
            </script>
            <einu-col size="M12 T3 D3" class="image-container">
                <einu-block class="image employee editable file-input" style="background-image: url('{{ route('images.show', 'avatar') }}');">
                    <einu-block class="hover">
                        <einu-icon code="photo"></einu-icon>
                    </einu-block>
                    <input type="file" name="image" onchange="uploadImage('{{ csrf_token() }}', this)" />
                    <input type="hidden" name="image_id" class="_image_hidden" />
                </einu-block>
            </einu-col>
            <einu-col size="M12 T8 D8" offset="M0 T1 D1">
                <einu-control-group>
                    <einu-col size="M12 T12 D12">
                        <label for="first_name">{{ __('employees.field.first_name') }}<span class="required"> •</span></label>
                        <input type="text" name="first_name" id="first_name" />
                    </einu-col>
                </einu-control-group>
                <einu-control-group>
                    <einu-col size="M12 T12 D12">
                        <label for="last_name">{{ __('employees.field.last_name') }}<span class="required"> •</span></label>
                        <input type="text" name="last_name" id="last_name" />
                    </einu-col>
                </einu-control-group>
                <einu-control-group>
                    <einu-col size="M12 T12 D12">
                        <label for="umcn">{{ __('employees.field.umcn') }}<span class="required"> •</span></label>
                        <input type="text" name="umcn" id="umcn" />
                    </einu-col>
                </einu-control-group>
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M12 T6 D6">
                <label for="email">{{ __('employees.field.email') }}<span class="required"> •</span></label>
                <input type="email" name="email" id="email" />
            </einu-col>
            <einu-col size="M12 T6 D6">
                <label for="phone">{{ __('employees.field.phone') }}<span class="required"> •</span></label>
                <input type="text" name="phone" id="phone" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M12 T6 D6">
                <label for="birth_date">{{ __('employees.field.birth_date') }}<span class="required"> •</span></label>
                <input type="date" name="birth_date" id="birth_date" />
            </einu-col>
            <einu-col size="M12 T6 D6">
                <label for="sex">{{ __('employees.field.sex') }}<span class="required"> •</span></label>
                <select name="sex" id="sex">
                    <option value="1">{{ __('employees.field.sex.male') }}</option>
                    <option value="2">{{ __('employees.field.sex.female') }}</option>
                </select>
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">{{ __('employees.title.location') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M12 T4 D4">
                <label for="address">{{ __('employees.field.address') }}<span class="required"> •</span></label>
                <input type="text" name="address" id="address" />
            </einu-col>
            <einu-col size="M12 T4 D4">
                <label for="city">{{ __('employees.field.city') }}<span class="required"> •</span></label>
                <input type="text" name="city" id="city" />
            </einu-col>
            <einu-col size="M12 T4 D4">
                <label for="postal">{{ __('employees.field.postal') }}<span class="required"> •</span></label>
                <input type="text" name="postal" id="postal" />
            </einu-col>
        </einu-control-group>


        <einu-control-group>
            <einu-col size="M12 T6 D6">
                <label for="state">{{ __('employees.field.state') }}<span class="required"> •</span></label>
                <input type="text" name="state" id="state" />
            </einu-col>
            <einu-col size="M12 T6 D6">
                <label for="country">{{ __('employees.field.country') }}<span class="required"> •</span></label>
                <select name="country_id" id="country">
                    @foreach ($countries as $country)
                        @if (auth()->user()->country_id == $country->id)
                            <option selected value="{{ $country->id }}">{{ $country->name }}</option>
                        @else
                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                        @endif
                    @endforeach
                </select>
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">{{ __('employees.title.job') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M12 T6 D6">
                <label for="start_date">{{ __('employees.field.start_date') }}<span class="required"> •</span></label>
                <input type="date" name="start_date" id="start_date" />
            </einu-col>
            <einu-col size="M12 T6 D6">
                <label for="position">{{ __('employees.field.position') }}</label>
                <select name="position_id" id="position">
                    <option selected></option>
                    @foreach ($positions as $position)
                        <option value="{{ $position->id }}">{{ $position->name }}</option>
                    @endforeach
                    @foreach ($departments as $department)
                        @if ($department->positions->count() > 0)
                            <optgroup label="{{ $department->name }}">
                                @foreach ($department->positions as $position)
                                    <option value="{{ $position->id }}">{{ $position->name }}</option>
                                @endforeach
                            </optgroup>
                        @endif
                    @endforeach
                </select>
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">
            {{ __('employees.title.additional') }}

            <a class="alternative right" href="javascript:void(0)" onclick="add_field('custom_fields')">
                <einu-icon code="plus"></einu-icon> {{ __('employees.action.field') }}
            </a>
        </einu-heading>
        <einu-block style="padding: 0;" id="custom_fields">
            <script>
                function add_field(id){var custom_fields=$('#'+id);var fields_count=custom_fields.find('.field').length;var field='<einu-control-group class="field" einu-data-id="'+(fields_count+1)+'"> <einu-col size="M12 T6 D6"> <label for="custom_data_key">{{ __('employees.field.custom.key') }}</label> <input type="text" name="custom_data_key[]" id="custom_data_key"/> </einu-col> <einu-col size="M12 T6 D6"> <label for="custom_data_value">{{ __('employees.field.custom.value') }}</label> <input type="text" name="custom_data_value[]" id="custom_data_value"/> </einu-col> </einu-control-group>';custom_fields.append(field);}function remove_field(id,field){var field_to_remove=$('#'+id).find('.field[einu-data-id="'+field+'"]');field_to_remove.remove();}
            </script>
            <einu-control-group class="field">
                <einu-col size="M12 T6 D6">
                    <label for="custom_data_key">{{ __('employees.field.custom.key') }}</label>
                    <input type="text" name="custom_data_key[]" id="custom_data_key" />
                </einu-col>
                <einu-col size="M12 T6 D6">
                    <label for="custom_data_value">{{ __('employees.field.custom.value') }}</label>
                    <input type="text" name="custom_data_value[]" id="custom_data_value" />
                </einu-col>
            </einu-control-group>
        </einu-block>
        <script>
            function generateFileRow(id, name) {
                var downloadUrl = '{{ URL::to('files/download/') }}/' + id;
                var iconUrl = '{{ URL::to('files/icon/') }}/' + id;
                var token = '{{ csrf_token() }}';

                var row = '<einu-table-row>'
                        + '<einu-table-cell><img src="' + iconUrl + '" /></einu-table-cell>'
                        + '<einu-table-cell style="font-family: \'Source Code Pro\'; font-size: 13px;">' + name + '</einu-table-cell>'
                        + '<einu-table-cell style="width: 90px;">'
                        + '<a class="button squared rounded" href="' + downloadUrl + '"><einu-icon code="download"></einu-icon></a>'
                        + '<a class="button squared rounded red" href="javascript:void(0)" onclick="deleteFile(\'' + token + '\', this, \'' + id + '\')"><einu-icon code="trash-o"></einu-icon></a>'
                        + '<input type="hidden" name="file_ids[]" value="' + id + '" />'
                        + '</einu-table-cell>'
                        + '</einu-table-row>';

                return row;
            }

            function uploadFiles(token, self) {
                var input = self;
                var $table = $('._files_table');
                var $section = $table.parent();
                var $emptyMessage = $section.find('._empty_message');

                var formData = new FormData();
                formData.append('_token', token);
                for(var key in input.files) { formData.append('files[]', input.files[key]); }

                $.ajax({
                    url: '{{ route('employees.upload.files', 'none') }}',
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        $emptyMessage.remove();

                        for (var file in data.files) {
                            $table.append(generateFileRow(data.files[file].id, data.files[file].name));
                        }
                    }
                });
            }

            function deleteFile(token, self, id) {
                var button = self;
                var url = '{{ route('files.delete') }}';
                var data = { id: id, _token: token, _method: "DELETE" };

                var emptyMessage = '{{ __('files.message.empty') }}';
                var emptyHtml = '<einu-textblock class="message _empty_message">' + emptyMessage + '</einu-textblock>';

                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    success: function() {
                        var $row = $(button).parent().parent();
                        var $table = $row.parent();
                        var $section = $table.parent();

                        $row.remove();

                        if ($table.find('einu-table-row').length == 0) {
                            $section.append(emptyHtml);
                        }
                    }
                });
            }
        </script>
        <einu-heading size="1">
            {{ __('employees.title.files') }}

            <a class="alternative right blue file-input" href="javascript:void(0)">
                <einu-icon code="upload"></einu-icon> {{ __('files.action.upload.files') }}
                <input type="file" name="files[]" multiple onchange="uploadFiles('{{ csrf_token() }}', this)" />
            </a>
        </einu-heading>

        <einu-block class="section">
            <einu-table style="width: 100%;" class="_files_table">
                <einu-table-header>
                    <einu-table-cell></einu-table-cell>
                    <einu-table-cell>{{ __('files.field.name') }}</einu-table-cell>
                    <einu-table-cell>{{ __('files.field.options') }}</einu-table-cell>
                </einu-table-header>
            </einu-table>
            <einu-textblock class="message _empty_message">{{ __('files.message.empty') }}</einu-textblock>
        </einu-block>
        <einu-control-group style="margin-top: 20px;">
            <button type="submit" class="blue">{{ __('employees.action.submit') }}</button>
        </einu-control-group>
        <einu-control-group>
            <input type="checkbox" name="continue" id="continue" {{ isset($continue) ? 'checked' : '' }} />
            <label for="continue">{{ __('employees.field.continue.label') }}</label>
        </einu-control-group>

        <einu-control-group class="footer">
            <einu-textblock class="comment"><span style="color: #C02C44;">•</span> {{ __('employees.message.required') }}</einu-textblock>
        </einu-control-group>
    </form>
@endsection