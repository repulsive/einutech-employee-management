@extends('layouts.app')
@section('pageTitle', __('candidates.title.main'))
@section('screen', 'app')
@section('sidebar_category', 'candidates')

@section('content')
    <form action="{{ route('candidates.bulkdelete') }}" method="POST">
        {{ csrf_field() }}

        <einu-block class="section">
            <einu-heading size="1">
                {{ __('candidates.title.main') }}

                <a href="{{ route('candidates.download.spreadsheet') }}" class="alternative left">
                    <einu-icon code="download"></einu-icon> <span>{{ __('candidates.action.download') }} (.xlsx)</span>
                </a>
                <a href="{{ route('candidates.create') }}" class="alternative right green">
                    <einu-icon code="plus"></einu-icon> <span>{{ __('candidates.action.new') }}</span>
                </a>
            </einu-heading>
            {!! $dataGrid->show() !!}

            {{--<einu-table style="width: 100%;">--}}
                {{--<einu-table-header>--}}
                    {{--<einu-table-cell>--}}
                        {{--<script>function toggleAllCheckboxes(element){$(element).prop('checked')==1?$('.selectcheck').prop('checked',!0):$('.selectcheck').prop('checked',!1);}</script>--}}
                        {{--<input type="checkbox" onchange="toggleAllCheckboxes(this)"/>--}}
                    {{--</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('candidates.field.id') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('candidates.field.first_name') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('candidates.field.last_name') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('candidates.field.email') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('candidates.field.position') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('candidates.field.options') }}</einu-table-cell>--}}
                {{--</einu-table-header>--}}
                {{--@forelse ($candidates as $candidate)--}}
                    {{--<einu-table-row>--}}
                        {{--<einu-table-cell>--}}
                            {{--<input type="checkbox" class="selectcheck" name="selected[]" value="{{ $candidate->id }}" />--}}
                        {{--</einu-table-cell>--}}
                        {{--<einu-table-cell style="font-family: 'Source Code Pro';">{{ $candidate->id }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{{ $candidate->first_name }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{{ $candidate->last_name }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{{ $candidate->email }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{!! ($candidate->position !== null) ? $candidate->position->name : '<span class="error"><einu-icon code="exclamation-triangle"></einu-icon> '.__('candidates.error.position.unavailable').'</span>' !!}</einu-table-cell>--}}
                        {{--<einu-table-cell style="width: 125px;">--}}
                            {{--<a class="button squared rounded" href="{{ route('candidates.show', $candidate->id) }}"><einu-icon code="eye"></einu-icon></a>--}}
                            {{--<a class="button squared rounded" href="{{ route('candidates.edit', $candidate->id) }}"><einu-icon code="pencil"></einu-icon></a>--}}
                            {{--<a class="button squared rounded red" href="{{ route('candidates.delete', $candidate->id) }}"><einu-icon code="trash-o"></einu-icon></a>--}}
                        {{--</einu-table-cell>--}}
                    {{--</einu-table-row>--}}
                {{--@empty--}}
                    {{--@section('empty.message')--}}
                        {{--<einu-textblock class="message">{{ __('candidates.message.empty') }}</einu-textblock>--}}
                    {{--@endsection--}}
                {{--@endforelse--}}
            {{--</einu-table>--}}
            {{--@yield('empty.message')--}}
        </einu-block>
        <einu-block class="action-bar">
            <einu-control-group>
                <button class="red rounded" type="submit">
                    {{ __('candidates.action.deletebulk') }}
                </button>
            </einu-control-group>
        </einu-block>
    </form>
@endsection