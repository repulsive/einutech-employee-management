@extends('layouts.app')
@section('pageTitle', __('candidates.title.main').': Create')
@section('screen', 'app')
@section('sidebar_category', 'candidates')

@section('content')
    <form class="einu-form" action="{{ route('candidates.index') }}" method="POST">
        {{ csrf_field() }}

        <einu-heading size="1">{{ __('candidates.title.personal') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="first_name">{{ __('candidates.field.first_name') }}</label>
                <input type="text" name="first_name" id="first_name" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="last_name">{{ __('candidates.field.last_name') }}</label>
                <input type="text" name="last_name" id="last_name" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="email">{{ __('candidates.field.email') }}</label>
                <input type="email" name="email" id="email" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="phone">{{ __('candidates.field.phone') }}</label>
                <input type="text" name="phone" id="phone" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M4 T4 D4">
                <label for="birth_date">{{ __('candidates.field.birth_date') }}</label>
                <input type="date" name="birth_date" id="birth_date" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="umcn">{{ __('candidates.field.umcn') }}</label>
                <input type="text" name="umcn" id="umcn" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="sex">{{ __('candidates.field.sex') }}</label>
                <select name="sex" id="sex">
                    <option value="1">{{ __('candidates.field.sex.male') }}</option>
                    <option value="2">{{ __('candidates.field.sex.female') }}</option>
                </select>
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">{{ __('candidates.title.location') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M4 T4 D4">
                <label for="address">{{ __('candidates.field.address') }}</label>
                <input type="text" name="address" id="address" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="city">{{ __('candidates.field.city') }}</label>
                <input type="text" name="city" id="city" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="postal">{{ __('candidates.field.postal') }}</label>
                <input type="text" name="postal" id="postal" />
            </einu-col>
        </einu-control-group>


        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="state">{{ __('candidates.field.state') }}</label>
                <input type="text" name="state" id="state" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="country">{{ __('candidates.field.country') }}</label>
                <select name="country_id" id="country">
                    @foreach ($countries as $country)
                        @if (auth()->user()->country_id == $country->id)
                            <option selected value="{{ $country->id }}">{{ $country->name }}</option>
                        @else
                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                        @endif
                    @endforeach
                </select>
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">{{ __('candidates.title.job') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="start_date">{{ __('candidates.field.start_date') }}</label>
                <input type="date" name="start_date" id="start_date" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="position">{{ __('candidates.field.position') }}</label>
                <select name="position_id" id="position">
                    @foreach ($positions as $position)
                        <option value="{{ $position->id }}">{{ $position->name }}</option>
                    @endforeach
                </select>
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">
            {{ __('candidates.title.additional') }}

            <a class="alternative right" href="javascript:void(0)" onclick="add_field('custom_fields')">
                <einu-icon code="plus"></einu-icon> {{ __('candidates.action.field') }}
            </a>
        </einu-heading>
        <einu-block style="padding: 0;" id="custom_fields">
            <script>
                function add_field(id){var custom_fields=$('#'+id);var fields_count=custom_fields.find('.field').length;var field='<einu-control-group class="field" einu-data-id="'+(fields_count+1)+'"> <einu-col size="M6 T6 D6"> <label for="custom_data_key">{{ __('candidates.field.custom.key') }}</label> <input type="text" name="custom_data_key[]" id="custom_data_key"/> </einu-col> <einu-col size="M6 T6 D6"> <label for="custom_data_value">{{ __('candidates.field.custom.value') }}</label> <input type="text" name="custom_data_value[]" id="custom_data_value"/> </einu-col> </einu-control-group>';custom_fields.append(field);}function remove_field(id,field){var field_to_remove=$('#'+id).find('.field[einu-data-id="'+field+'"]');field_to_remove.remove();}
            </script>
            <einu-control-group class="field">
                <einu-col size="M6 T6 D6">
                    <label for="custom_data_key">{{ __('candidates.field.custom.key') }}</label>
                    <input type="text" name="custom_data_key[]" id="custom_data_key" />
                </einu-col>
                <einu-col size="M6 T6 D6">
                    <label for="custom_data_value">{{ __('candidates.field.custom.value') }}</label>
                    <input type="text" name="custom_data_value[]" id="custom_data_value" />
                </einu-col>
            </einu-control-group>
        </einu-block>
        <einu-control-group style="margin-top: 20px;">
            <button type="submit" class="blue">{{ __('candidates.action.submit') }}</button>
        </einu-control-group>
        <einu-control-group>
            <input type="checkbox" name="continue" id="continue" {{ isset($continue) ? 'checked' : '' }} />
            <label for="continue">{{ __('positions.field.continue.label') }}</label>
        </einu-control-group>
    </form>
@endsection