@extends('layouts.app')
@section('pageTitle', __('candidates.title.main').': Delete')
@section('screen', 'dialog')
@section('sidebar_category', 'candidates')

@section('content')
    <einu-flex-container>
        <einu-block class="dialog">
            <img class="logo" src="{{ asset('img/eem-logo.png') }}" />
            <einu-heading size="1">{{ __('candidates.title.delete') }}</einu-heading>
            <einu-block class="form-container">
                <form action="{{ route('candidates.show', $candidate->id) }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('delete') }}

                    <einu-control-group>
                        <einu-textblock>
                            {{ __('candidates.message.delete', ['name' => $candidate->first_name." ".$candidate->last_name]) }}
                        </einu-textblock>
                    </einu-control-group>

                    <einu-control-group>
                        <einu-col size="M6 T6 D6">
                            <button name="submit" value="yes" class="green" type="submit">{{ __('candidates.action.yes') }}</button>
                        </einu-col>
                        <einu-col size="M6 T6 D6">
                            <button name="submit" value="no" class="red" type="submit">{{ __('candidates.action.no') }}</button>
                        </einu-col>
                    </einu-control-group>
                </form>
            </einu-block>
        </einu-block>
    </einu-flex-container>
@endsection