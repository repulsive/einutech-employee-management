@extends('layouts.print')
@section('pageTitle', __('candidates.title.main').': '.$candidate->first_name.' '.$candidate->last_name)
@section('screen', 'print')
@section('sidebar_category', 'candidates')

@section('content')
    <script>(function(){window.print();})();</script>
    <einu-heading size="1">
        {{ __('candidates.title.personal') }}
    </einu-heading>
    <einu-control-group>
        <einu-col size="M6 T6 D6">
            <label for="first_name">{{ __('candidates.field.first_name') }}</label>
            <input disabled type="text" id="first_name" value="{{ $candidate->first_name }}" />
        </einu-col>
        <einu-col size="M6 T6 D6">
            <label for="last_name">{{ __('candidates.field.last_name') }}</label>
            <input disabled type="text" id="last_name" value="{{ $candidate->last_name }}" />
        </einu-col>
    </einu-control-group>

    <einu-control-group>
        <einu-col size="M6 T6 D6">
            <label for="email">{{ __('candidates.field.email') }}</label>
            <input disabled type="email" id="email" value="{{ $candidate->email }}" />
        </einu-col>
        <einu-col size="M6 T6 D6">
            <label for="phone">{{ __('candidates.field.phone') }}</label>
            <input disabled type="text" id="phone" value="{{ $candidate->phone }}" />
        </einu-col>
    </einu-control-group>

    <einu-control-group>
        <einu-col size="M4 T4 D4">
            <label for="birth_date">{{ __('candidates.field.birth_date') }}</label>
            <input disabled type="date" id="birth_date" value="{{ $candidate->birth_date }}" />
        </einu-col>
        <einu-col size="M4 T4 D4">
            <label for="umcn">{{ __('candidates.field.umcn') }}</label>
            <input disabled type="text" id="umcn" value="{{ $candidate->umcn }}" />
        </einu-col>
        <einu-col size="M4 T4 D4">
            <label for="sex">{{ __('candidates.field.sex') }}</label>
            <select disabled id="sex">
                <option value="1" {{ $candidate->sex == 1 ? 'selected' : '' }}>{{ __('candidates.field.sex.male') }}</option>
                <option value="2" {{ $candidate->sex == 2 ? 'selected' : '' }}>{{ __('candidates.field.sex.female') }}</option>
            </select>
        </einu-col>
    </einu-control-group>

    <einu-heading size="1">{{ __('candidates.title.location') }}</einu-heading>
    <einu-control-group>
        <einu-col size="M4 T4 D4">
            <label for="address">{{ __('candidates.field.address') }}</label>
            <input disabled type="text" id="address" value="{{ $candidate->address }}" />
        </einu-col>
        <einu-col size="M4 T4 D4">
            <label for="city">{{ __('candidates.field.city') }}</label>
            <input disabled type="text" id="city" value="{{ $candidate->city }}" />
        </einu-col>
        <einu-col size="M4 T4 D4">
            <label for="postal">{{ __('candidates.field.postal') }}</label>
            <input disabled type="text" id="postal" value="{{ $candidate->postal }}" />
        </einu-col>
    </einu-control-group>


    <einu-control-group>
        <einu-col size="M6 T6 D6">
            <label for="state">{{ __('candidates.field.state') }}</label>
            <input disabled type="text" id="state" value="{{ $candidate->state }}" />
        </einu-col>
        <einu-col size="M6 T6 D6">
            <label for="country">{{ __('candidates.field.country') }}</label>
            <select disabled id="country">
                @foreach ($countries as $country)
                    @if ($candidate->country_id == $country->id)
                        <option selected value="{{ $country->id }}">{{ $country->name }}</option>
                    @else
                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                    @endif
                @endforeach
            </select>
        </einu-col>
    </einu-control-group>

    <einu-heading size="1">{{ __('candidates.title.job') }}</einu-heading>
    <einu-control-group>
        <einu-col size="M6 T6 D6">
            <label for="start_date">{{ __('candidates.field.start_date') }}</label>
            <input disabled type="date" id="start_date" value="{{ $candidate->start_date }}" />
        </einu-col>
        <einu-col size="M6 T6 D6">
            <label for="position">{{ __('candidates.field.position') }}</label>
            <select disabled name="position" id="position">
                @foreach ($positions as $position)
                    @if ($candidate->position_id == $position->id)
                        <option selected value="{{ $position->id }}">{{ $position->name }}</option>
                    @else
                        <option value="{{ $position->id }}">{{ $position->name }}</option>
                    @endif
                @endforeach
            </select>
        </einu-col>
    </einu-control-group>

    @if (!empty($custom_fields))
        <einu-heading size="1">{{ __('candidates.title.additional') }}</einu-heading>
        <einu-block style="padding: 0;">
            <?php $count = 0; ?>

            @foreach ($custom_fields as $key => $value)
                <?php $count++; ?>

                @if ($count % 2 != 0)
                    <einu-control-group class="field">
                        @endif
                        <einu-col size="M6 T6 D6">
                            <label for="{{ $key }}">{{ $key }}</label>
                            <input disabled type="text" id="{{ $key }}" value="{{ $value }}" />
                        </einu-col>
                        @if ($count % 2 == 0)
                    </einu-control-group>
                @endif
            @endforeach
        </einu-block>
    @endif
@endsection