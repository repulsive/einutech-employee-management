@extends('layouts.app')
@section('pageTitle', $position->name.': '.__('applications.title.apply'))
@section('screen', 'app')

@section('content')
    <form class="einu-form" action="{{ route('applications.show', $position_id) }}" method="POST">
        {{ csrf_field() }}

        <einu-heading size="1">
            {{ $position->name }}

            <a href="{{ $position->company->website }}" class="alternative left">
                <einu-icon code="briefcase"></einu-icon> <span>{{ $position->company->name }}</span>
            </a>
            <?php $department = $position->department;
            $company = $position->company;
            $country = "";
            $city = "";

            if ($department != null) {
                if (($department->country != null) && (!empty($department->city))) {
                    $country = $department->country->name;
                    $city = $department->city;
                } else {
                    $country = $company->country->name;
                    $city = $company->city;
                }
            } else {
                $country = $company->country->name;
                $city = $company->city;
            } ?>
            <a href="{{ 'https://www.google.com/maps/search/?api=1&query='.urlencode($city.', '.$country) }}" class="alternative right green" target="_blank">
                <einu-icon code="globe"></einu-icon> <span>{{ $city.', '.$country }}</span>
            </a>
        </einu-heading>

        <einu-control-group>
            <einu-col size="M12 T6 D6">
                <label for="first_name">{{ __('candidates.field.first_name') }}<span class="required"> •</span></label>
                <input type="text" name="first_name" id="first_name" />
            </einu-col>
            <einu-col size="M12 T6 D6">
                <label for="last_name">{{ __('candidates.field.last_name') }}<span class="required"> •</span></label>
                <input type="text" name="last_name" id="last_name" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M12 T6 D6">
                <label for="email">{{ __('candidates.field.email') }}<span class="required"> •</span></label>
                <input type="email" name="email" id="email" />
            </einu-col>
            <einu-col size="M12 T6 D6">
                <label for="phone">{{ __('candidates.field.phone') }}<span class="required"> •</span></label>
                <input type="text" name="phone" id="phone" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M12 T6 D6">
                <label for="birth_date">{{ __('candidates.field.birth_date') }}<span class="required"> •</span></label>
                <input type="date" name="birth_date" id="birth_date" />
            </einu-col>
            <einu-col size="M12 T6 D6">
                <label for="sex">{{ __('candidates.field.sex') }}<span class="required"> •</span></label>
                <select name="sex" id="sex">
                    <option value="1">{{ __('candidates.field.sex.male') }}</option>
                    <option value="2">{{ __('candidates.field.sex.female') }}</option>
                </select>
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <label for="cv">{{ __('candidates.field.cv') }} (PDF, DOC, DOCX)<span class="required"> •</span></label>
            <einu-input id="cv">
                <input type="file" name="cv" id="cv" accept=".pdf,.doc,.docx" />
            </einu-input>
        </einu-control-group>


        <einu-heading size="1">
            {{ __('employees.title.skills') }}
        </einu-heading>

        <?php $count = 0; ?>
        @foreach($position->skills as $skill)
            <?php $count++; ?>

            @if ($count % 2 != 0)
                <einu-control-group type="radio">
            @endif
                <einu-col size="M12 T12 D6">
                    <einu-control-group type="radio">
                        <label>{{ $skill }}</label>
                        <einu-input>
                            <input type="radio" name="skills['{{ $skill }}']" value="0" id="skills-{{ $skill }}-0" />
                            <label for="skills-{{ $skill }}-0">1</label>
                        </einu-input>
                        <einu-input>
                            <input type="radio" name="skills['{{ $skill }}']" value="25" id="skills-{{ $skill }}-25" />
                            <label for="skills-{{ $skill }}-25">2</label>
                        </einu-input>
                        <einu-input>
                            <input type="radio" name="skills['{{ $skill }}']" value="50" id="skills-{{ $skill }}-50" checked />
                            <label for="skills-{{ $skill }}-50">3</label>
                        </einu-input>
                        <einu-input>
                            <input type="radio" name="skills['{{ $skill }}']" value="75" id="skills-{{ $skill }}-75" />
                            <label for="skills-{{ $skill }}-75">4</label>
                        </einu-input>
                        <einu-input>
                            <input type="radio" name="skills['{{ $skill }}']" value="100" id="skills-{{ $skill }}-100" />
                            <label for="skills-{{ $skill }}-100">5</label>
                        </einu-input>
                    </einu-control-group>
                </einu-col>
            @if ($count % 2 == 0)
                </einu-control-group>
            @endif
        @endforeach
        @if ($count % 2 != 0)
            </einu-control-group>
        @endif

        <einu-control-group style="margin-top: 20px;">
            <button type="submit" class="blue">{{ __('positions.action.apply') }}</button>
        </einu-control-group>
    </form>
@endsection