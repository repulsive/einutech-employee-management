@extends('layouts.app')
@section('pageTitle', __('managers.title.main'))
@section('screen', 'app')
@section('sidebar_category', 'administration.managers')

@section('content')
    {{ $managers->links() }}
    <form action="{{ route('managers.bulkdelete') }}" method="POST">
        {{ csrf_field() }}

        <einu-block class="section">
            <einu-heading size="1">
                {{ __('managers.title.main') }}

                <a href="{{ route('managers.download.spreadsheet') }}" class="alternative left">
                    <einu-icon code="download"></einu-icon> {{ __('managers.action.download') }} (.xlsx)
                </a>
                <a href="{{ route('managers.create') }}" class="alternative right green">
                    <einu-icon code="plus"></einu-icon> {{ __('managers.action.new') }}
                </a>
            </einu-heading>
            <einu-table style="width: 100%;">
                <einu-table-header>
                    <einu-table-cell>
                        <script>function toggleAllCheckboxes(element){$(element).prop('checked')==1?$('.selectcheck').prop('checked',!0):$('.selectcheck').prop('checked',!1);}</script>
                        <input type="checkbox" onchange="toggleAllCheckboxes(this)"/>
                    </einu-table-cell>
                    <einu-table-cell>{{ __('managers.field.id') }}</einu-table-cell>
                    <einu-table-cell>{{ __('managers.field.name') }}</einu-table-cell>
                    <einu-table-cell>{{ __('managers.field.country') }}</einu-table-cell>
                    <einu-table-cell>{{ __('managers.field.manager_number') }}</einu-table-cell>
                    <einu-table-cell>{{ __('managers.field.tax_id_number') }}</einu-table-cell>
                    <einu-table-cell>{{ __('managers.field.options') }}</einu-table-cell>
                </einu-table-header>
                @forelse ($managers as $manager)
                    <einu-table-row>
                        <einu-table-cell>
                            <input type="checkbox" class="selectcheck" name="selected[]" value="{{ $manager->id }}" />
                        </einu-table-cell>
                        <einu-table-cell style="font-family: 'Source Code Pro';">{{ $manager->id }}</einu-table-cell>
                        <einu-table-cell>{{ $manager->name }}</einu-table-cell>
                        <einu-table-cell>{{ $manager->country->name }}</einu-table-cell>
                        <einu-table-cell>{{ $manager->manager_number }}</einu-table-cell>
                        <einu-table-cell>{{ $manager->tax_id_number }}</einu-table-cell>
                        <einu-table-cell style="width: 125px;">
                            <a class="button squared rounded" href="{{ route('managers.show', $manager->id) }}"><einu-icon code="eye"></einu-icon></a>
                            <a class="button squared rounded" href="{{ route('managers.edit', $manager->id) }}"><einu-icon code="pencil"></einu-icon></a>
                            <a class="button squared rounded red" href="{{ route('managers.delete', $manager->id) }}"><einu-icon code="trash-o"></einu-icon></a>
                        </einu-table-cell>
                    </einu-table-row>
                @empty
                    @section('empty.message')
                        <einu-textblock class="message">{{ __('managers.message.empty') }}</einu-textblock>
                    @endsection
                @endforelse
            </einu-table>
            @yield('empty.message')
        </einu-block>
        <einu-block class="action-bar">
            <einu-control-group>
                <button class="red rounded" type="submit">
                    {{ __('managers.action.deletebulk') }}
                </button>
            </einu-control-group>
        </einu-block>
    </form>
    {{ $managers->links() }}
@endsection