@extends('layouts.print')
@section('pageTitle', __('managers.title.main').': '.$manager->first_name.' '.$manager->last_name)
@section('screen', 'print')
@section('sidebar_category', 'managers')

@section('content')
    <script>(function(){window.print();})();</script>
    <einu-heading size="1">
        {{ __('managers.title.personal') }}
    </einu-heading>
    <einu-control-group>
        <einu-col size="M6 T6 D6">
            <label for="first_name">{{ __('managers.field.first_name') }}</label>
            <input disabled type="text" id="first_name" value="{{ $manager->first_name }}" />
        </einu-col>
        <einu-col size="M6 T6 D6">
            <label for="last_name">{{ __('managers.field.last_name') }}</label>
            <input disabled type="text" id="last_name" value="{{ $manager->last_name }}" />
        </einu-col>
    </einu-control-group>

    <einu-control-group>
        <einu-col size="M6 T6 D6">
            <label for="email">{{ __('managers.field.email') }}</label>
            <input disabled type="email" id="email" value="{{ $manager->email }}" />
        </einu-col>
        <einu-col size="M6 T6 D6">
            <label for="phone">{{ __('managers.field.phone') }}</label>
            <input disabled type="text" id="phone" value="{{ $manager->phone }}" />
        </einu-col>
    </einu-control-group>

    <einu-control-group>
        <einu-col size="M4 T4 D4">
            <label for="birth_date">{{ __('managers.field.birth_date') }}</label>
            <input disabled type="date" id="birth_date" value="{{ $manager->birth_date }}" />
        </einu-col>
        <einu-col size="M4 T4 D4">
            <label for="umcn">{{ __('managers.field.umcn') }}</label>
            <input disabled type="text" id="umcn" value="{{ $manager->umcn }}" />
        </einu-col>
        <einu-col size="M4 T4 D4">
            <label for="sex">{{ __('managers.field.sex') }}</label>
            <select disabled id="sex">
                <option value="1" {{ $manager->sex == 1 ? 'selected' : '' }}>{{ __('managers.field.sex.male') }}</option>
                <option value="2" {{ $manager->sex == 2 ? 'selected' : '' }}>{{ __('managers.field.sex.female') }}</option>
            </select>
        </einu-col>
    </einu-control-group>

    <einu-heading size="1">{{ __('managers.title.location') }}</einu-heading>
    <einu-control-group>
        <einu-col size="M4 T4 D4">
            <label for="address">{{ __('managers.field.address') }}</label>
            <input disabled type="text" id="address" value="{{ $manager->address }}" />
        </einu-col>
        <einu-col size="M4 T4 D4">
            <label for="city">{{ __('managers.field.city') }}</label>
            <input disabled type="text" id="city" value="{{ $manager->city }}" />
        </einu-col>
        <einu-col size="M4 T4 D4">
            <label for="postal">{{ __('managers.field.postal') }}</label>
            <input disabled type="text" id="postal" value="{{ $manager->postal }}" />
        </einu-col>
    </einu-control-group>


    <einu-control-group>
        <einu-col size="M6 T6 D6">
            <label for="state">{{ __('managers.field.state') }}</label>
            <input disabled type="text" id="state" value="{{ $manager->state }}" />
        </einu-col>
        <einu-col size="M6 T6 D6">
            <label for="country">{{ __('managers.field.country') }}</label>
            <select disabled id="country">
                @foreach ($countries as $country)
                    @if ($manager->country_id == $country->id)
                        <option selected value="{{ $country->id }}">{{ $country->name }}</option>
                    @else
                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                    @endif
                @endforeach
            </select>
        </einu-col>
    </einu-control-group>

    <einu-heading size="1">{{ __('managers.title.job') }}</einu-heading>
    <einu-control-group>
        <einu-col size="M6 T6 D6">
            <label for="start_date">{{ __('managers.field.start_date') }}</label>
            <input disabled type="date" id="start_date" value="{{ $manager->start_date }}" />
        </einu-col>
        <einu-col size="M6 T6 D6">
            <label for="position">{{ __('managers.field.position') }}</label>
            <select disabled name="position" id="position">
                @foreach ($positions as $position)
                    @if ($manager->position_id == $position->id)
                        <option selected value="{{ $position->id }}">{{ $position->name }}</option>
                    @else
                        <option value="{{ $position->id }}">{{ $position->name }}</option>
                    @endif
                @endforeach
            </select>
        </einu-col>
    </einu-control-group>

    @if (!empty($custom_fields))
        <einu-heading size="1">{{ __('managers.title.additional') }}</einu-heading>
        <einu-block style="padding: 0;">
            <?php $count = 0; ?>

            @foreach ($custom_fields as $key => $value)
                <?php $count++; ?>

                @if ($count % 2 != 0)
                    <einu-control-group class="field">
                        @endif
                        <einu-col size="M6 T6 D6">
                            <label for="{{ $key }}">{{ $key }}</label>
                            <input disabled type="text" id="{{ $key }}" value="{{ $value }}" />
                        </einu-col>
                        @if ($count % 2 == 0)
                    </einu-control-group>
                @endif
            @endforeach
        </einu-block>
    @endif
@endsection