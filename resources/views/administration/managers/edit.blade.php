@extends('layouts.app')
@section('pageTitle', __('managers.title.main').': Edit')
@section('screen', 'app')
@section('sidebar_category', 'managers')

@section('content')
    <form class="einu-form" action="{{ route('managers.show', $manager->id) }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('patch') }}

        <einu-heading size="1">
            {{ __('managers.title.personal') }}

            <a class="alternative left red" href="{{ route('managers.delete', $manager->id) }}">
                <einu-icon code="trash-o"></einu-icon> {{ __('managers.action.delete') }}
            </a>
            <a class="alternative right" href="{{ route('managers.show', $manager->id) }}">
                <einu-icon code="eye"></einu-icon> {{ __('managers.action.show') }}
            </a>
        </einu-heading>
        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="first_name">{{ __('managers.field.first_name') }}</label>
                <input type="text" name="first_name" id="first_name" value="{{ $manager->first_name }}" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="last_name">{{ __('managers.field.last_name') }}</label>
                <input type="text" name="last_name" id="last_name" value="{{ $manager->last_name }}" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="email">{{ __('managers.field.email') }}</label>
                <input type="email" name="email" id="email" value="{{ $manager->email }}" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="phone">{{ __('managers.field.phone') }}</label>
                <input type="text" name="phone" id="phone" value="{{ $manager->phone }}" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M4 T4 D4">
                <label for="birth_date">{{ __('managers.field.birth_date') }}</label>
                <input type="date" name="birth_date" id="birth_date" value="{{ $manager->birth_date }}" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="umcn">{{ __('managers.field.umcn') }}</label>
                <input type="text" name="umcn" id="umcn" value="{{ $manager->umcn }}" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="sex">{{ __('managers.field.sex') }}</label>
                <select name="sex" id="sex">
                    <option value="1" {{ $manager->sex == 1 ? 'selected' : '' }}>{{ __('managers.field.sex.male') }}</option>
                    <option value="2" {{ $manager->sex == 2 ? 'selected' : '' }}>{{ __('managers.field.sex.female') }}</option>
                </select>
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">{{ __('managers.title.location') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M4 T4 D4">
                <label for="address">{{ __('managers.field.address') }}</label>
                <input type="text" name="address" id="address" value="{{ $manager->address }}" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="city">{{ __('managers.field.city') }}</label>
                <input type="text" name="city" id="city" value="{{ $manager->city }}" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="postal">{{ __('managers.field.postal') }}</label>
                <input type="text" name="postal" id="postal" value="{{ $manager->postal }}" />
            </einu-col>
        </einu-control-group>


        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="state">{{ __('managers.field.state') }}</label>
                <input type="text" name="state" id="state" value="{{ $manager->state }}" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="country">{{ __('managers.field.country') }}</label>
                <select name="country_id" id="country">
                    @foreach ($countries as $country)
                        @if ($manager->country_id == $country->id)
                            <option selected value="{{ $country->id }}">{{ $country->name }}</option>
                        @else
                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                        @endif
                    @endforeach
                </select>
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">{{ __('managers.title.job') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="start_date">{{ __('managers.field.start_date') }}</label>
                <input type="date" name="start_date" id="start_date" value="{{ $manager->start_date }}" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="position">{{ __('managers.field.position') }}</label>
                <select name="position_id" id="position">
                    @foreach ($positions as $position)
                        @if ($manager->position_id == $position->id)
                            <option selected value="{{ $position->id }}">{{ $position->name }}</option>
                        @else
                            <option value="{{ $position->id }}">{{ $position->name }}</option>
                        @endif
                    @endforeach
                </select>
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">
            {{ __('managers.title.additional') }}

            <a class="alternative right" href="javascript:void(0)" onclick="add_field('custom_fields')">
                <einu-icon code="plus"></einu-icon> {{ __('managers.action.field') }}
            </a>
        </einu-heading>
        <einu-block style="padding: 0;" id="custom_fields">
            <script>
                function add_field(id){var custom_fields=$('#'+id);var fields_count=custom_fields.find('.field').length;var field='<einu-control-group class="field" einu-data-id="'+(fields_count+1)+'"> <einu-col size="M6 T6 D6"> <label for="custom_data_key">{{ __('managers.field.custom.key') }}</label> <input type="text" name="custom_data_key[]" id="custom_data_key"/> </einu-col> <einu-col size="M6 T6 D6"> <label for="custom_data_value">{{ __('managers.field.custom.value') }}</label> <input type="text" name="custom_data_value[]" id="custom_data_value"/> </einu-col> </einu-control-group>';custom_fields.append(field);}function remove_field(id,field){var field_to_remove=$('#'+id).find('.field[einu-data-id="'+field+'"]');field_to_remove.remove();}
            </script>
            @foreach ($custom_fields as $key => $value)
                <einu-control-group class="field">
                    <einu-col size="M6 T6 D6">
                        <label for="custom_data_key">{{ __('managers.field.custom.key') }}</label>
                        <input type="text" name="custom_data_key[]" id="custom_data_key" value="{{ $key }}" />
                    </einu-col>
                    <einu-col size="M6 T6 D6">
                        <label for="custom_data_value">{{ __('managers.field.custom.value') }}</label>
                        <input type="text" name="custom_data_value[]" id="custom_data_value" value="{{ $value }}" />
                    </einu-col>
                </einu-control-group>
            @endforeach
        </einu-block>
        <einu-control-group style="margin-top: 20px;">
            <button type="submit" class="blue">{{ __('managers.action.submit') }}</button>
        </einu-control-group>
    </form>
@endsection