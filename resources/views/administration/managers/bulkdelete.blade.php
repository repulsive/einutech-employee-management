@extends('layouts.app')
@section('pageTitle', __('managers.title.main').': Delete')
@section('screen', 'dialog')
@section('sidebar_category', 'managers')

@section('content')
    <einu-flex-container>
        <einu-block class="dialog">
            <img class="logo" src="{{ asset('img/eem-logo.png') }}" />
            <einu-heading size="1">{{ __('managers.title.delete') }}</einu-heading>
            <einu-block class="form-container">
                <form action="{{ route('managers.bulkdelete') }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('delete') }}

                    <input type="hidden" name="selected" value="{{ $selected }}" />

                    <einu-control-group>
                        <einu-textblock>{{ __('managers.message.deletebulk') }}</einu-textblock>
                    </einu-control-group>
                    <einu-control-group>
                        <einu-list>
                            @foreach ($managers as $employee)
                                <einu-list-item>{{ $employee->first_name.' '.$employee->last_name }}</einu-list-item>
                            @endforeach
                        </einu-list>
                    </einu-control-group>

                    <einu-control-group>
                        <einu-col size="M6 T6 D6">
                            <button name="submit" value="yes" class="green" type="submit">{{ __('managers.action.yes') }}</button>
                        </einu-col>
                        <einu-col size="M6 T6 D6">
                            <button name="submit" value="no" class="red" type="submit">{{ __('managers.action.no') }}</button>
                        </einu-col>
                    </einu-control-group>
                </form>
            </einu-block>
        </einu-block>
    </einu-flex-container>
@endsection