@extends('layouts.app')
@section('pageTitle', __('administrators.title.main'))
@section('screen', 'app')
@section('sidebar_category', 'administration.administrators')

@section('content')
    {!! $dataGrid->data->appends(Request::except('page'))->links('_partials.pagination') !!}

    <form action="{{ route('administrators.bulkdelete') }}" method="POST">
        {{ csrf_field() }}

        <einu-block class="section">
            <einu-heading size="1">
                {{ __('administrators.title.main') }}

                <a href="{{ route('administrators.download.spreadsheet') }}" class="alternative left">
                    <einu-icon code="download"></einu-icon> {{ __('administrators.action.download') }} (.xlsx)
                </a>
                <a href="{{ route('administrators.create') }}" class="alternative right green">
                    <einu-icon code="plus"></einu-icon> {{ __('administrators.action.new') }}
                </a>
            </einu-heading>
            {!! $dataGrid->show() !!}

            {{--<einu-table style="width: 100%;">--}}
                {{--<einu-table-header>--}}
                    {{--<einu-table-cell>--}}
                        {{--<script>function toggleAllCheckboxes(element){$(element).prop('checked')==1?$('.selectcheck').prop('checked',!0):$('.selectcheck').prop('checked',!1);}</script>--}}
                        {{--<input type="checkbox" onchange="toggleAllCheckboxes(this)"/>--}}
                    {{--</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('administrators.field.id') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('administrators.field.name') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('administrators.field.email') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('administrators.field.options') }}</einu-table-cell>--}}
                {{--</einu-table-header>--}}
                {{--@forelse ($administrators as $administrator)--}}
                    {{--<einu-table-row>--}}
                        {{--<einu-table-cell>--}}
                            {{--<input type="checkbox" class="selectcheck" name="selected[]" value="{{ $administrator->id }}" />--}}
                        {{--</einu-table-cell>--}}
                        {{--<einu-table-cell style="font-family: 'Source Code Pro';">{{ $administrator->id }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{{ $administrator->name }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{{ $administrator->email }}</einu-table-cell>--}}
                        {{--<einu-table-cell style="width: 125px;">--}}
                            {{--<a class="button squared rounded" href="{{ route('administrators.show', $administrator->id) }}"><einu-icon code="eye"></einu-icon></a>--}}
                            {{--<a class="button squared rounded" href="{{ route('administrators.edit', $administrator->id) }}"><einu-icon code="pencil"></einu-icon></a>--}}
                            {{--<a class="button squared rounded red" href="{{ route('administrators.delete', $administrator->id) }}"><einu-icon code="trash-o"></einu-icon></a>--}}
                        {{--</einu-table-cell>--}}
                    {{--</einu-table-row>--}}
                {{--@empty--}}
                {{--@section('empty.message')--}}
                    {{--<einu-textblock class="message">{{ __('administrators.message.empty') }}</einu-textblock>--}}
                {{--@endsection--}}
                {{--@endforelse--}}
            {{--</einu-table>--}}
            {{--@yield('empty.message')--}}
        </einu-block>
        <einu-block class="action-bar">
            <einu-control-group>
                <button class="red rounded" type="submit">
                    {{ __('administrators.action.deletebulk') }}
                </button>
            </einu-control-group>
        </einu-block>
    </form>

    {!! $dataGrid->data->appends(Request::except('page'))->links('_partials.pagination') !!}
@endsection