@extends('layouts.print')
@section('pageTitle', __('companies.title.main').': '.$company->first_name.' '.$company->last_name)
@section('screen', 'print')
@section('sidebar_category', 'companies')

@section('content')
    <script>(function(){window.print();})();</script>
    <einu-heading size="1">
        {{ __('companies.title.personal') }}
    </einu-heading>
    <einu-control-group>
        <einu-col size="M6 T6 D6">
            <label for="first_name">{{ __('companies.field.first_name') }}</label>
            <input disabled type="text" id="first_name" value="{{ $company->first_name }}" />
        </einu-col>
        <einu-col size="M6 T6 D6">
            <label for="last_name">{{ __('companies.field.last_name') }}</label>
            <input disabled type="text" id="last_name" value="{{ $company->last_name }}" />
        </einu-col>
    </einu-control-group>

    <einu-control-group>
        <einu-col size="M6 T6 D6">
            <label for="email">{{ __('companies.field.email') }}</label>
            <input disabled type="email" id="email" value="{{ $company->email }}" />
        </einu-col>
        <einu-col size="M6 T6 D6">
            <label for="phone">{{ __('companies.field.phone') }}</label>
            <input disabled type="text" id="phone" value="{{ $company->phone }}" />
        </einu-col>
    </einu-control-group>

    <einu-control-group>
        <einu-col size="M4 T4 D4">
            <label for="birth_date">{{ __('companies.field.birth_date') }}</label>
            <input disabled type="date" id="birth_date" value="{{ $company->birth_date }}" />
        </einu-col>
        <einu-col size="M4 T4 D4">
            <label for="umcn">{{ __('companies.field.umcn') }}</label>
            <input disabled type="text" id="umcn" value="{{ $company->umcn }}" />
        </einu-col>
        <einu-col size="M4 T4 D4">
            <label for="sex">{{ __('companies.field.sex') }}</label>
            <select disabled id="sex">
                <option value="1" {{ $company->sex == 1 ? 'selected' : '' }}>{{ __('companies.field.sex.male') }}</option>
                <option value="2" {{ $company->sex == 2 ? 'selected' : '' }}>{{ __('companies.field.sex.female') }}</option>
            </select>
        </einu-col>
    </einu-control-group>

    <einu-heading size="1">{{ __('companies.title.location') }}</einu-heading>
    <einu-control-group>
        <einu-col size="M4 T4 D4">
            <label for="address">{{ __('companies.field.address') }}</label>
            <input disabled type="text" id="address" value="{{ $company->address }}" />
        </einu-col>
        <einu-col size="M4 T4 D4">
            <label for="city">{{ __('companies.field.city') }}</label>
            <input disabled type="text" id="city" value="{{ $company->city }}" />
        </einu-col>
        <einu-col size="M4 T4 D4">
            <label for="postal">{{ __('companies.field.postal') }}</label>
            <input disabled type="text" id="postal" value="{{ $company->postal }}" />
        </einu-col>
    </einu-control-group>


    <einu-control-group>
        <einu-col size="M6 T6 D6">
            <label for="state">{{ __('companies.field.state') }}</label>
            <input disabled type="text" id="state" value="{{ $company->state }}" />
        </einu-col>
        <einu-col size="M6 T6 D6">
            <label for="country">{{ __('companies.field.country') }}</label>
            <select disabled id="country">
                @foreach ($countries as $country)
                    @if ($company->country_id == $country->id)
                        <option selected value="{{ $country->id }}">{{ $country->name }}</option>
                    @else
                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                    @endif
                @endforeach
            </select>
        </einu-col>
    </einu-control-group>

    <einu-heading size="1">{{ __('companies.title.job') }}</einu-heading>
    <einu-control-group>
        <einu-col size="M6 T6 D6">
            <label for="start_date">{{ __('companies.field.start_date') }}</label>
            <input disabled type="date" id="start_date" value="{{ $company->start_date }}" />
        </einu-col>
        <einu-col size="M6 T6 D6">
            <label for="position">{{ __('companies.field.position') }}</label>
            <select disabled name="position" id="position">
                @foreach ($positions as $position)
                    @if ($company->position_id == $position->id)
                        <option selected value="{{ $position->id }}">{{ $position->name }}</option>
                    @else
                        <option value="{{ $position->id }}">{{ $position->name }}</option>
                    @endif
                @endforeach
            </select>
        </einu-col>
    </einu-control-group>

    @if (!empty($custom_fields))
        <einu-heading size="1">{{ __('companies.title.additional') }}</einu-heading>
        <einu-block style="padding: 0;">
            <?php $count = 0; ?>

            @foreach ($custom_fields as $key => $value)
                <?php $count++; ?>

                @if ($count % 2 != 0)
                    <einu-control-group class="field">
                        @endif
                        <einu-col size="M6 T6 D6">
                            <label for="{{ $key }}">{{ $key }}</label>
                            <input disabled type="text" id="{{ $key }}" value="{{ $value }}" />
                        </einu-col>
                        @if ($count % 2 == 0)
                    </einu-control-group>
                @endif
            @endforeach
        </einu-block>
    @endif
@endsection