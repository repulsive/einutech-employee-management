@extends('layouts.app')
@section('pageTitle', __('companies.title.main').': '.$company->name)
@section('screen', 'app')
    @section('sidebar_category', 'company')
    @section('sidebar_company', $company)

@section('content')
    <form class="einu-form">
        {{ csrf_field() }}

        <einu-heading size="1">
            {{ __('company.title.personal') }}

            <a class="alternative left" href="{{ route('companies.edit', $company->id) }}">
                <einu-icon code="pencil"></einu-icon> {{ __('company.action.edit') }}
            </a>
        </einu-heading>
        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="name">{{ __('company.field.name') }}</label>
                <input disabled type="text" id="name" value="{{ $company->name }}" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="slug">{{ __('company.field.slug') }}</label>
                <input disabled type="text" id="slug" value="{{ $company->slug }}" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="email">{{ __('company.field.email') }}</label>
                <input disabled type="email" id="email" value="{{ $company->email }}" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="phone">{{ __('company.field.phone') }}</label>
                <input disabled type="text" id="phone" value="{{ $company->phone }}" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M4 T4 D4">
                <label for="company_number">{{ __('company.field.company_number') }}</label>
                <input disabled type="text" id="company_number" value="{{ $company->company_number }}" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="tax_id_number">{{ __('company.field.tax_id_number') }}</label>
                <input disabled type="text" id="tax_id_number" value="{{ $company->tax_id_number }}" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="activity_code">{{ __('company.field.activity_code') }}</label>
                <input disabled type="text" id="activity_code" value="{{ $company->activity_code }}" />
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">{{ __('company.title.location') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M4 T4 D4">
                <label for="address">{{ __('company.field.address') }}</label>
                <input disabled type="text" id="address" value="{{ $company->address }}" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="city">{{ __('company.field.city') }}</label>
                <input disabled type="text" id="city" value="{{ $company->city }}" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="postal">{{ __('company.field.postal') }}</label>
                <input disabled type="text" id="postal" value="{{ $company->postal }}" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="state">{{ __('company.field.state') }}</label>
                <input disabled type="text" id="state" value="{{ $company->state }}" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="country">{{ __('company.field.country') }}</label>
                <input disabled type="text" id="country" value="{{ $country->name }}" />
            </einu-col>
        </einu-control-group>
    </form>
@endsection