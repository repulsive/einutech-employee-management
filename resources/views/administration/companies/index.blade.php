@extends('layouts.app')
@section('pageTitle', __('companies.title.main'))
@section('screen', 'app')
@section('sidebar_category', 'administration.companies')

@section('content')
    {!! $dataGrid->data->appends(Request::except('page'))->links('_partials.pagination') !!}

    <form action="{{ route('companies.bulkdelete') }}" method="POST">
        {{ csrf_field() }}

        <einu-block class="section">
            <einu-heading size="1">
                {{ __('companies.title.main') }}

                <a href="{{ route('companies.download.spreadsheet') }}" class="alternative left">
                    <einu-icon code="download"></einu-icon> {{ __('companies.action.download') }} (.xlsx)
                </a>
                <a href="{{ route('companies.create') }}" class="alternative right green">
                    <einu-icon code="plus"></einu-icon> {{ __('companies.action.new') }}
                </a>
            </einu-heading>
            {!! $dataGrid->show() !!}

            {{--<einu-table style="width: 100%;">--}}
                {{--<einu-table-header>--}}
                    {{--<einu-table-cell>--}}
                        {{--<script>function toggleAllCheckboxes(element){$(element).prop('checked')==1?$('.selectcheck').prop('checked',!0):$('.selectcheck').prop('checked',!1);}</script>--}}
                        {{--<input type="checkbox" onchange="toggleAllCheckboxes(this)"/>--}}
                    {{--</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('companies.field.id') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('companies.field.name') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('companies.field.country') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('companies.field.company_number') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('companies.field.tax_id_number') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('companies.field.options') }}</einu-table-cell>--}}
                {{--</einu-table-header>--}}
                {{--@forelse ($companies as $company)--}}
                    {{--<einu-table-row>--}}
                        {{--<einu-table-cell>--}}
                            {{--<input type="checkbox" class="selectcheck" name="selected[]" value="{{ $company->id }}" />--}}
                        {{--</einu-table-cell>--}}
                        {{--<einu-table-cell style="font-family: 'Source Code Pro';">{{ $company->id }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{{ $company->name }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{{ $company->country->name }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{{ $company->company_number }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{{ $company->tax_id_number }}</einu-table-cell>--}}
                        {{--<einu-table-cell style="width: 125px;">--}}
                            {{--<a class="button squared rounded" href="{{ route('companies.show', $company->id) }}"><einu-icon code="eye"></einu-icon></a>--}}
                            {{--<a class="button squared rounded" href="{{ route('companies.edit', $company->id) }}"><einu-icon code="pencil"></einu-icon></a>--}}
                            {{--<a class="button squared rounded red" href="{{ route('companies.delete', $company->id) }}"><einu-icon code="trash-o"></einu-icon></a>--}}
                        {{--</einu-table-cell>--}}
                    {{--</einu-table-row>--}}
                {{--@empty--}}
                    {{--@section('empty.message')--}}
                        {{--<einu-textblock class="message">{{ __('companies.message.empty') }}</einu-textblock>--}}
                    {{--@endsection--}}
                {{--@endforelse--}}
            {{--</einu-table>--}}
            {{--@yield('empty.message')--}}
        </einu-block>
        <einu-block class="action-bar">
            <einu-control-group>
                <button class="red rounded" type="submit">
                    {{ __('companies.action.deletebulk') }}
                </button>
            </einu-control-group>
        </einu-block>
    </form>

    {!! $dataGrid->data->appends(Request::except('page'))->links('_partials.pagination') !!}
@endsection