@extends('layouts.app')
@section('pageTitle', __('companies.title.main').': Create')
@section('screen', 'app')
@section('sidebar_category', 'administration')

@section('content')
    <form class="einu-form" action="{{ route('company.index') }}" method="POST">
        {{ csrf_field() }}

        <einu-heading size="1">
            {{ __('company.title.personal') }}
        </einu-heading>
        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="name">{{ __('company.field.name') }}</label>
                <input type="text" name="name" id="name" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="slug">{{ __('company.field.slug') }}</label>
                <input type="text" id="slug" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="email">{{ __('company.field.email') }}</label>
                <input type="email" name="email" id="email" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="phone">{{ __('company.field.phone') }}</label>
                <input type="text" name="phone" id="phone" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M4 T4 D4">
                <label for="company_number">{{ __('company.field.company_number') }}</label>
                <input type="text" name="company_number" id="company_number" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="tax_id_number">{{ __('company.field.tax_id_number') }}</label>
                <input type="text" name="tax_id_number" id="tax_id_number" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="activity_code">{{ __('company.field.activity_code') }}</label>
                <input type="text" name="activity_code" id="activity_code" />
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">{{ __('company.title.location') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M4 T4 D4">
                <label for="address">{{ __('company.field.address') }}</label>
                <input type="text" name="address" id="address" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="city">{{ __('company.field.city') }}</label>
                <input type="text" name="city" id="city" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="postal">{{ __('company.field.postal') }}</label>
                <input type="text" name="postal" id="postal" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="state">{{ __('company.field.state') }}</label>
                <input type="text" name="state" id="state" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="country">{{ __('company.field.country') }}</label>
                <select name="country_id" id="country">
                    @foreach ($countries as $country)
                        @if (auth()->user()->country_id == $country->id)
                            <option selected value="{{ $country->id }}">{{ $country->name }}</option>
                        @else
                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                        @endif
                    @endforeach
                </select>
            </einu-col>
        </einu-control-group>

        <einu-control-group style="margin-top: 20px;">
            <button type="submit" class="blue">{{ __('employees.action.submit') }}</button>
        </einu-control-group>
    </form>
@endsection