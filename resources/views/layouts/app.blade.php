@extends('layouts.components.head')
@section('body')
<body einu-flow="com.einutech.eem" einu-theme="{{ \App\Models\Setting::get('theme') != null ? \App\Models\Setting::get('theme') : 'light' }}" screen="@yield('screen')">
    <einu-app class="{{ Auth::check() ? 'workspace' : '' }}">
        @if (Auth::check())
            @include('layouts.components.topbar')
            @include('layouts.components.sidebar')
        @endif
        <einu-content>
            @yield('content')
        </einu-content>
    </einu-app>

    <einu-footer class="copyright">Developed and powered with <span>&#128150;</span> by <a target="_blank" class="no-pad" href="http://www.einutech.com/">Einutech</a></einu-footer>

    <script src="{{ asset('js/app.js') }}"></script>
    @stack('scripts')
</body>
</html>
@endsection