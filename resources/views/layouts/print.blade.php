@extends('layouts.components.head')
@section('body')
    <body onload="window.print()" einu-flow="com.einutech.eem" screen="print">
    <einu-app>
        <einu-content>
            <einu-grid class="header">
                <einu-col size="M3 T3 D3">
                    <img class="logo" src="{{ asset($company->logo) }}" />
                </einu-col>
                <einu-col size="M3 T3 D3">
                    <einu-textblock><strong>{{ $company->name }}</strong></einu-textblock>
                    <einu-textblock><strong>Phone: </strong>{{ $company->phone }}</einu-textblock>
                    <einu-textblock><strong>Email: </strong>{{ $company->email }}</einu-textblock>
                </einu-col>
                <einu-col size="M3 T3 D3">
                    <einu-textblock><strong>Company Number: </strong>{{ $company->company_number }}</einu-textblock>
                    <einu-textblock><strong>Tax Identification Number: </strong>{{ $company->tax_id_number }}</einu-textblock>
                </einu-col>
                <einu-col size="M3 T3 D3">
                    <einu-textblock>{{ $company->address }}</einu-textblock>
                    <einu-textblock>{{ $company->postal.' '.$company->city }}</einu-textblock>
                    <einu-textblock>{{ $company->state.', '.$company->country->name }}</einu-textblock>
                </einu-col>
            </einu-grid>
            @yield('content')
        </einu-content>
    </einu-app>

    <script src="{{ asset('js/app.js') }}"></script>
    </body>
    </html>
@endsection