<einu-block class="sidebar">
    <einu-control-group class="user">
        <einu-col size="M3">
            <img src="{{ Auth::user()->avatar }}">
        </einu-col>
        <einu-col size="M9">
            <einu-textblock>
                <a class="no-pad" href="#">
                    {{ Auth::user()->name }}
                </a>
            </einu-textblock>
            <einu-textblock>
                {{ Auth::user()->highestRole()->name }}
            </einu-textblock>
        </einu-col>
    </einu-control-group>

    @if (Auth::user()->company !== null)
        @if (Auth::user()->hasRole('agent') || Auth::user()->hasRole('manager'))
            <einu-heading size="2">Management</einu-heading>
        @endif
        @if (Auth::user()->hasRole('manager'))
            <einu-control-group>
                <a href="{{ route('company.index') }}" class="button {{ $__env->yieldContent('sidebar_category') == 'company' ? 'blue' : '' }}">
                    <einu-icon code="briefcase" fixed-width></einu-icon>

                    <span>{{ __('company.title.main') }}</span>
                </a>
            </einu-control-group>
            <einu-control-group>
                <a href="{{ route('agents.index') }}" class="button {{ $__env->yieldContent('sidebar_category') == 'agents' ? 'blue' : '' }}">
                    <einu-icon code="user-secret" fixed-width></einu-icon>

                    <span>{{ __('agents.title.main') }}</span>
                </a>
            </einu-control-group>
        @endif
        @if (Auth::user()->hasRole('agent'))
            <einu-control-group>
                <a href="{{ route('employees.index') }}" class="button {{ $__env->yieldContent('sidebar_category') == 'employees' ? 'blue' : '' }}">
                    <einu-icon code="users" fixed-width></einu-icon>

                    <span>{{ __('employees.title.main') }}</span>
                </a>
            </einu-control-group>
            <einu-control-group>
                <a href="{{ route('candidates.index') }}" class="button {{ $__env->yieldContent('sidebar_category') == 'candidates' ? 'blue' : '' }}">
                    <einu-icon code="users" fixed-width></einu-icon>

                    <span>{{ __('candidates.title.main') }}</span>
                </a>
            </einu-control-group>
            <einu-control-group>
                <a href="{{ route('departments.index') }}" class="button {{ $__env->yieldContent('sidebar_category') == 'departments' ? 'blue' : '' }}">
                    <einu-icon code="tasks" fixed-width></einu-icon>

                    <span>{{ __('departments.title.main') }}</span>
                </a>
            </einu-control-group>
            <einu-control-group>
                <a href="{{ route('positions.index') }}" class="button {{ $__env->yieldContent('sidebar_category') == 'positions' ? 'blue' : '' }}">
                    <einu-icon code="tasks" fixed-width></einu-icon>

                    <span>{{ __('positions.title.main') }}</span>
                </a>
            </einu-control-group>
        @endif
    @endif
    @if (Auth::user()->hasRole('administrator') || Auth::user()->hasRole('owner'))
        <einu-heading size="2">Administration</einu-heading>
    @endif
    @if (Auth::user()->hasRole('administrator'))
        <einu-control-group>
            <a href="{{ route('administrators.index') }}" class="button {{ $__env->yieldContent('sidebar_category') == 'administration.administrators' ? 'blue' : '' }}">
                {{ __('administrators.title.main') }}
            </a>
        </einu-control-group>
        <einu-control-group>
            <a href="{{ route('companies.index') }}" class="button {{ $__env->yieldContent('sidebar_category') == 'administration.companies' ? 'blue' : '' }}">
                {{ __('companies.title.main') }}
            </a>
        </einu-control-group>

        <einu-control-group>
            <a href="{{ route('administrators.index') }}" class="button {{ $__env->yieldContent('sidebar_category') == 'admin.companies' ? 'blue' : '' }}">
                Administrators
            </a>
        </einu-control-group>
    @endif
</einu-block>