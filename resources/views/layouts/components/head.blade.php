<!DOCTYPE html>
<html lang="en" einu-theme="{{ \App\Models\Setting::get('theme') != null ? \App\Models\Setting::get('theme') : 'light' }}">
    <head>
        <meta charset="UTF-8">

        <meta http-equiv="X-UA-Compatible"  content="IE=edge">

        <meta name="viewport"               content="width=device-width, initial-scale=1">
        <meta name="description"            content="">
        <meta name="keywords"               content="einutech,web,dizajn,design,izrada,sajtovi,jovan,ivanovic">
        <meta name="author"                 content="Einutech">
        <meta name="language"               content="en">
        <meta name="csrf-token"             content="{{ csrf_token() }}" />
        <meta name="theme-color"            content="#1D2429" />

        <meta property="og:title"           content="Einutech">
        <meta property="og:site_name"       content="Einutech">
        <meta property="og:image"           content="http://www.einutech.com/public/img/facebook-image.png">
        <meta property="og:url"             content="http://www.einutech.com/">
        <meta property="og:description"     content="">

        <title>{{ config('app.name', 'Einutech') }} | @yield('pageTitle')</title>

        {{--<link type="image/png"  rel="icon"          href="http://www.einu.io/public/img/favicon.ico">--}}
        <link type="text/css"   rel="stylesheet"    href="{{ asset('css/app.css') }}">
        @stack('styles')

        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.2/moment.min.js"></script>
        @stack('scripts-head')
    </head>
    @yield('body')