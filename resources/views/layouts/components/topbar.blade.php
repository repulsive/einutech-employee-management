<einu-block class="topbar">
    <einu-control-group class="align-left">
        <script>
            function toggleSidebar() {
                $('.sidebar').toggleClass('toggled');
                $('einu-content').toggleClass('sidebar-toggled');
            }
        </script>
        <a class="button squared blue sidebar-toggle" href="javascript:void(0)" onclick="toggleSidebar()">
            <einu-icon code="bars"></einu-icon>
        </a>
    </einu-control-group>

    <einu-control-group class="logos">
        <a class="button logo-min">
            <img src="https://www.einutech.com/img/logo-min.png" />
        </a>
        <a class="button logo-min">
            <img src="{{ asset('img/eem-logo-light.png') }}" />
        </a>
    </einu-control-group>

    <einu-control-group class="align-right">
        <a class="button blue squared" href="{{ route('settings.index') }}"><einu-icon code="cog"></einu-icon></a>
        <form method="POST" action="{{ route('logout') }}">
            {{ csrf_field() }}

            <button class="red squared" type="submit"><einu-icon code="sign-out"></einu-icon></button>
        </form>
    </einu-control-group>
</einu-block>