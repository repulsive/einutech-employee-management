@extends('layouts.app')
@section('pageTitle', __('departments.title.main').': Create')
@section('screen', 'app')
@section('sidebar_category', 'departments')

@section('content')
    <form class="einu-form" action="{{ route('departments.index') }}" method="POST">
        {{ csrf_field() }}

        <einu-heading size="1">{{ __('departments.title.basic') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M12 T12 D12">
                <label for="name">{{ __('departments.field.name') }}<span class="required"> •</span></label>
                <input type="text" name="name" id="name" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="email">{{ __('departments.field.email') }}</label>
                <input type="email" name="email" id="email" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="phone">{{ __('departments.field.phone') }}</label>
                <input type="text" name="phone" id="phone" />
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">{{ __('departments.title.location') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M4 T4 D4">
                <label for="address">{{ __('departments.field.address') }}</label>
                <input type="text" name="address" id="address" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="city">{{ __('departments.field.city') }}</label>
                <input type="text" name="city" id="city" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="postal">{{ __('departments.field.postal') }}</label>
                <input type="text" name="postal" id="postal" />
            </einu-col>
        </einu-control-group>


        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="state">{{ __('departments.field.state') }}</label>
                <input type="text" name="state" id="state" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="country">{{ __('departments.field.country') }}</label>
                <select name="country_id" id="country">
                    @foreach ($countries as $country)
                        @if (auth()->user()->company->country_id == $country->id)
                            <option selected value="{{ $country->id }}">{{ $country->name }}</option>
                        @else
                            <option value="{{ $country->id }}">{{ $country->name }}</option>
                        @endif
                    @endforeach
                </select>
            </einu-col>
        </einu-control-group>

        <einu-control-group style="margin-top: 20px;">
            <button type="submit" class="blue">{{ __('departments.action.submit') }}</button>
        </einu-control-group>
        <einu-control-group>
            <input type="checkbox" name="continue" id="continue" {{ isset($continue) ? 'checked' : '' }} />
            <label for="continue">{{ __('departments.field.continue.label') }}</label>
        </einu-control-group>

        <einu-control-group class="footer">
            <einu-textblock class="comment"><span style="color: #C02C44;">•</span> {{ __('departments.message.required') }}</einu-textblock>
        </einu-control-group>
    </form>
@endsection