@extends('layouts.app')
@section('pageTitle', __('departments.title.main'))
@section('screen', 'app')
@section('sidebar_category', 'departments')

@section('content')
    <form action="{{ route('departments.bulkdelete') }}" method="POST">
        {{ csrf_field() }}

        <einu-block class="section">
            <einu-heading size="1">
                {{ __('departments.title.main') }}

                <a href="{{ route('departments.download.spreadsheet') }}" class="alternative left">
                    <einu-icon code="download"></einu-icon> <span>{{ __('departments.action.download') }} (.xlsx)</span>
                </a>
                <a href="{{ route('departments.create') }}" class="alternative right green">
                    <einu-icon code="plus"></einu-icon> <span>{{ __('departments.action.new') }}</span>
                </a>
            </einu-heading>
            {!! $dataGrid->show() !!}
        </einu-block>
        <einu-block class="action-bar">
            <einu-control-group>
                <button class="red rounded" type="submit">
                    {{ __('departments.action.deletebulk') }}
                </button>
            </einu-control-group>
        </einu-block>
    </form>
@endsection