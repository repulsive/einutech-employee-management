<einu-block class="side-menu">
    <a href="{{ route('settings.page', 'account') }}" class="button {{ $__env->yieldContent('settings_page') == 'account' ? 'blue' : '' }}"><einu-icon code="user"></einu-icon> {{ __('settings.page.account') }}</a>
    <a href="{{ route('settings.page', 'appearance') }}" class="button {{ $__env->yieldContent('settings_page') == 'appearance' ? 'blue' : '' }}"><einu-icon code="eye"></einu-icon> {{ __('settings.page.appearance') }}</a>
    <a href="{{ route('settings.page', 'security') }}" class="button {{ $__env->yieldContent('settings_page') == 'security' ? 'blue' : '' }}"><einu-icon code="lock"></einu-icon> {{ __('settings.page.security') }}</a>
</einu-block>