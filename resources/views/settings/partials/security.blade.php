<einu-section class="open">
    <einu-heading size="2" class="accordion-head">
        {{ __('settings.title.change_password') }}

        <einu-icon class="far-right chevron" code="chevron-down"></einu-icon>
    </einu-heading>
    <form class="einu-form">
        {{ csrf_field() }}

        <einu-control-group>
            <label for="current_password">{{ __('settings.field.current_password') }}<span class="required"> •</span></label>
            <input type="text" name="current_password" id="current_password" />
        </einu-control-group>
        <einu-control-group>
            <label for="new_password">{{ __('settings.field.new_password') }}<span class="required"> •</span></label>
            <input type="text" name="new_password" id="new_password" />
        </einu-control-group>
        <einu-control-group>
            <label for="repeat_password">{{ __('settings.field.repeat_password') }}<span class="required"> •</span></label>
            <input type="text" name="repeat_password" id="repeat_password" />
        </einu-control-group>
        <einu-control-group>
            <einu-col size="M12 T4 D3" offset="M0 T8 D9">
                <button type="submit" class="small blue">{{ __('settings.action.update') }}</button>
            </einu-col>
        </einu-control-group>
    </form>
</einu-section>
<einu-section>
    <einu-heading size="2" class="accordion-head">
        {{ __('settings.title.tfa') }}

        <einu-icon class="far-right down" code="chevron-down"></einu-icon>
    </einu-heading>
    <form class="einu-form">
        {{ csrf_field() }}

        <einu-control-group>
            <label for="current_password">{{ __('settings.field.current_password') }}<span class="required"> •</span></label>
            <input type="text" name="current_password" id="current_password" />
        </einu-control-group>
        <einu-control-group>
            <label for="new_password">{{ __('settings.field.new_password') }}<span class="required"> •</span></label>
            <input type="text" name="new_password" id="new_password" />
        </einu-control-group>
        <einu-control-group>
            <label for="repeat_password">{{ __('settings.field.repeat_password') }}<span class="required"> •</span></label>
            <input type="text" name="repeat_password" id="repeat_password" />
        </einu-control-group>
        <einu-control-group>
            <einu-col size="M12 T4 D3" offset="M0 T8 D9">
                <button type="submit" class="small blue">{{ __('settings.action.update') }}</button>
            </einu-col>
        </einu-control-group>
    </form>
</einu-section>