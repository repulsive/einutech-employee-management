@extends('layouts.app')
@section('pageTitle', __('settings.title.main'))
@section('screen', 'app')
@section('settings_page', $page)

@section('content')
    <einu-block class="section settings">
        <einu-heading size="1">
            {{ __('settings.title.main') }} - {{ __('settings.page.'.$page) }}

            {{--<a href="{{ route('positions.download.spreadsheet') }}" class="alternative left">--}}
                {{--<einu-icon code="download"></einu-icon> <span>{{ __('positions.action.download') }} (.xlsx)</span>--}}
            {{--</a>--}}
            {{--<a href="{{ route('positions.create') }}" class="alternative right green">--}}
                {{--<einu-icon code="plus"></einu-icon> <span>{{ __('positions.action.new') }}</span>--}}
            {{--</a>--}}
        </einu-heading>

        @include('settings.layout.menu')

        <einu-block class="content accordion">
            @include('settings.partials.'.$page)
        </einu-block>
    </einu-block>
@endsection

@push('scripts')
<script>
    $('.accordion > einu-section > einu-heading[size="2"]').click(function (e) {
        var $self = $(this);

        $self.parent().toggleClass('open');
    });
</script>
@endpush