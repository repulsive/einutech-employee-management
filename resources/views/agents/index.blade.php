@extends('layouts.app')
@section('pageTitle', __('agents.title.main'))
@section('screen', 'app')
@section('sidebar_category', 'agents')

@section('content')
    {{ $agents->links() }}
    <form action="{{ route('agents.bulkdelete') }}" method="POST">
        {{ csrf_field() }}

        <einu-block class="section">
            <einu-heading size="1">
                {{ __('agents.title.main') }}

                <a href="{{ route('agents.download.spreadsheet') }}" class="alternative left">
                    <einu-icon code="download"></einu-icon> {{ __('agents.action.download') }} (.xlsx)
                </a>
                <a href="{{ route('agents.create') }}" class="alternative right green">
                    <einu-icon code="plus"></einu-icon> {{ __('agents.action.new') }}
                </a>
            </einu-heading>
            <einu-table style="width: 100%;">
                <einu-table-header>
                    <einu-table-cell>
                        <script>function toggleAllCheckboxes(element){$(element).prop('checked')==1?$('.selectcheck').prop('checked',!0):$('.selectcheck').prop('checked',!1);}</script>
                        <input type="checkbox" onchange="toggleAllCheckboxes(this)"/>
                    </einu-table-cell>
                    <einu-table-cell>{{ __('agents.field.id') }}</einu-table-cell>
                    <einu-table-cell>{{ __('agents.field.name') }}</einu-table-cell>
                    <einu-table-cell>{{ __('agents.field.email') }}</einu-table-cell>
                    <einu-table-cell>{{ __('agents.field.options') }}</einu-table-cell>
                </einu-table-header>
                @foreach ($agents as $agent)
                    <einu-table-row>
                        <einu-table-cell>
                            <input type="checkbox" class="selectcheck" name="selected[]" value="{{ $agent->id }}" />
                        </einu-table-cell>
                        <einu-table-cell style="font-family: 'Source Code Pro';">{{ $agent->id }}</einu-table-cell>
                        <einu-table-cell>{{ $agent->name }}</einu-table-cell>
                        <einu-table-cell>{{ $agent->email }}</einu-table-cell>
                        <einu-table-cell style="width: 125px;">
                            <a class="button squared rounded" href="{{ route('agents.show', $agent->id) }}"><einu-icon code="eye"></einu-icon></a>
                            <a class="button squared rounded" href="{{ route('agents.edit', $agent->id) }}"><einu-icon code="pencil"></einu-icon></a>
                            <a class="button squared rounded red" href="{{ route('agents.delete', $agent->id) }}"><einu-icon code="trash-o"></einu-icon></a>
                        </einu-table-cell>
                    </einu-table-row>
                @endforeach
            </einu-table>
        </einu-block>
        <einu-block class="action-bar">
            <einu-control-group>
                <button class="red rounded" type="submit">
                    {{ __('agents.action.deletebulk') }}
                </button>
            </einu-control-group>
        </einu-block>
    </form>
    {{ $agents->links() }}
@endsection