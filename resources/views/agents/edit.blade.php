@extends('layouts.app')
@section('pageTitle', __('agents.title.main').': Edit')
@section('screen', 'app')
@section('sidebar_category', 'positions')

@section('content')
    <form class="einu-form" action="{{ route('positions.show', $position->id) }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('patch') }}

        <einu-heading size="1">
            {{ __('positions.title.info') }}

            <a class="alternative left red" href="{{ route('positions.delete', $position->id) }}">
                <einu-icon code="trash-o"></einu-icon> {{ __('positions.action.delete') }}
            </a>
            <a class="alternative right" href="{{ route('positions.show', $position->id) }}">
                <einu-icon code="eye"></einu-icon> {{ __('positions.action.show') }}
            </a>
        </einu-heading>
        <einu-control-group>
            <label for="name">{{ __('positions.field.name') }}</label>
            <input type="text" name="name" id="name" value="{{ $position->name }}"/>
        </einu-control-group>
        <einu-control-group>
            <label for="description">{{ __('positions.field.description') }}</label>
            <textarea rows="3" name="description" id="description">{{ $position->description }}</textarea>
        </einu-control-group>
        <einu-control-group>
            <input style="float: left;" type="checkbox" name="state" id="state" {{ $position->state == 1 ? 'checked' : '' }}/>
            <label style="float: left; padding: 3px; display: inline-block; width: auto;" for="state">{{ __('positions.field.state.label') }}</label>
        </einu-control-group>
        <einu-control-group style="margin-top: 20px;">
            <button type="submit" class="blue">{{ __('positions.action.submit') }}</button>
        </einu-control-group>
    </form>
@endsection