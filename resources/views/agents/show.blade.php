@extends('layouts.app')
@section('pageTitle', __('agents.title.main').': '.$agent->name)
@section('screen', 'app')
@section('sidebar_category', 'agents')

@section('content')
    <form class="einu-form">
        <einu-heading size="1">
            {{ __('agents.title.info') }}

            <a class="alternative left" href="{{ route('agents.edit', $agent->id) }}">
                <einu-icon code="pencil"></einu-icon> {{ __('agents.action.edit') }}
            </a>
            <a class="alternative right red" href="{{ route('agents.delete', $agent->id) }}">
                <einu-icon code="trash-o"></einu-icon> {{ __('agents.action.delete') }}
            </a>
        </einu-heading>
        <einu-control-group>
            <label for="name">{{ __('agents.field.name') }}</label>
            <input disabled type="text" id="name" value="{{ $agent->name }}"/>
        </einu-control-group>
        <einu-control-group>
            <label for="email">{{ __('agents.field.email') }}</label>
            <input disabled type="text" id="email" value="{{ $agent->email }}"/>
        </einu-control-group>
        <einu-control-group>
            {{--<input disabled style="float: left;" type="checkbox" id="state" {{ $agent->state == 1 ? 'checked' : '' }}/>--}}
            <label style="float: left; padding: 3px; display: inline-block; width: auto;" for="state">{{ __('agents.field.state.label') }}</label>
        </einu-control-group>
    </form>
@endsection