<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>EEM | {{ $company->name }} | Positions</title>

        <link type="text/css"   rel="stylesheet"    href="{{ asset('css/app.css') }}">
    </head>
    <body style="{{ request()->has('background') ? ("background-color:#" . request()->get('background') . ";") : 'background-color:#fff;' }} {{ request()->has('foreground') ? ("color:#" . request()->get('foreground') . ";") : 'color:#87909c;' }}" einu-flow="com.einutech.eem|embed" color="{{ request()->has('accent') ? request()->get('accent') : 'blue' }}">
        <einu-app>
            <einu-content>
                @forelse($positions as $position)
                    <einu-section>
                        <einu-col>
                            <einu-block class="title">
                                <einu-heading size="3">{{ $position->name }}</einu-heading>
                            </einu-block>
                            <einu-block class="meta">
                                <einu-textblock class="company">{{ $position->company->name }}</einu-textblock>
                                <span class="delimiter"> • </span>
                                <?php $department = $position->department;
                                $company = $position->company;
                                $country = "";
                                $city = "";

                                if ($department != null) {
                                    if (($department->country != null) && (!empty($department->city))) {
                                        $country = $department->country->name;
                                        $city = $department->city;
                                    } else {
                                        $country = $company->country->name;
                                        $city = $company->city;
                                    }
                                } else {
                                    $country = $company->country->name;
                                    $city = $company->city;
                                } ?>

                                <einu-textblock class="location">{{ $city.', '.$country }}</einu-textblock>
                                <span class="delimiter"> • </span>
                                <einu-textblock class="deadline">{{ __('positions.field.deadline') }}: {{ $position->deadline }}</einu-textblock>
                            </einu-block>
                            <einu-block class="description">
                                <?php $description = \GrahamCampbell\Markdown\Facades\Markdown::convertToHtml($position->description);
                                $start = strpos($description, '<p>');
                                $end = strpos($description, '</p>', $start);
                                $short_description = substr($description, $start, $end - $start + 4);
                                $short_description = html_entity_decode(strip_tags($short_description)); ?>

                                {!! $short_description !!}
                            </einu-block>
                            <einu-block class="skills">
                                @foreach ($position->skills as $skill)
                                    <span style="{{ request()->has('background') ? ("color:#" . request()->get('background') . ";") : 'color:#fff;' }}">{{ $skill }}</span>
                                @endforeach
                            </einu-block>
                        </einu-col>
                        <einu-col>
                            <a style="{{ request()->has('background') ? ("color:#" . request()->get('background') . ";") : 'color:#fff;' }}" class="button {{ request()->has('accent') ? request()->get('accent') : 'blue' }}" target="_blank" href="{{ route('applications.show', $position->id) }}">{{ __('positions.action.apply') }}</a>
                        </einu-col>
                    </einu-section>
                @empty
                    <einu-textblock>{{ __('positions.message.no_open') }}</einu-textblock>
                @endforelse
            </einu-content>
        </einu-app>
    </body>
</html>