@extends('layouts.app')
@section('pageTitle', 'Company: '.$company->name)
@section('screen', 'app')
@section('sidebar_category', 'company')

@section('content')
    <form class="einu-form" action="{{ route('company.index') }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('patch') }}

        <einu-heading size="1">
            {{ __('company.title.personal') }}

            <a class="alternative right" href="{{ route('company.index') }}">
                <einu-icon code="eye"></einu-icon> {{ __('company.action.show') }}
            </a>
        </einu-heading>
        <einu-control-group>
            <script>
                function uploadImage(token, self) {
                    var input = self;
                    var $image = $(input).parent();
                    var imageUrl = '{{ URL::to('/images') }}/';

                    var formData = new FormData();
                    formData.append('_token', token);
                    formData.append('image', input.files[0]);

                    $.ajax({
                        url: '{{ route('company.upload.image', $company->id) }}',
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            $image.css('background-image', 'url(' + imageUrl + data.id + ')');
                        }
                    });
                }
            </script>
            <einu-col size="M3 T3 D3" class="image-container">
                <einu-block class="image company editable file-input" style="background-image: url('{{ route('images.logo', $image != null ? $image->id : 'company') }}');">
                    <einu-block class="hover">
                        <einu-icon code="photo"></einu-icon>
                    </einu-block>
                    <input type="file" name="image" onchange="uploadImage('{{ csrf_token() }}', this)" />
                </einu-block>
            </einu-col>
            <einu-col size="M8 T8 D8" offset="M1 T1 D1">
                <einu-control-group>
                    <einu-col size="M12 T12 D12">
                        <label for="name">{{ __('company.field.name') }}</label>
                        <input type="text" name="name" id="name" value="{{ $company->name }}" />
                    </einu-col>
                </einu-control-group>
                <einu-control-group>
                    <einu-col size="M12 T12 D12">
                        <label for="email">{{ __('company.field.email') }}</label>
                        <input type="email" name="email" id="email" value="{{ $company->email }}" />
                    </einu-col>
                </einu-control-group>
                <einu-control-group>
                    <einu-col size="M12 T12 D12">
                        <label for="phone">{{ __('company.field.phone') }}</label>
                        <input type="text" name="phone" id="phone" value="{{ $company->phone }}" />
                    </einu-col>
                </einu-control-group>
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="slug">{{ __('company.field.slug') }}</label>
                <input disabled type="text" id="slug" value="{{ $company->slug }}" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="activity_code">{{ __('company.field.activity_code') }}</label>
                <input type="text" name="activity_code" id="activity_code" value="{{ $company->activity_code }}" />
            </einu-col>
        </einu-control-group>
        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="company_number">{{ __('company.field.company_number') }}</label>
                <input type="text" name="company_number" id="company_number" value="{{ $company->company_number }}" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="tax_id_number">{{ __('company.field.tax_id_number') }}</label>
                <input type="text" name="tax_id_number" id="tax_id_number" value="{{ $company->tax_id_number }}" />
            </einu-col>
        </einu-control-group>

        <einu-heading size="1">{{ __('company.title.location') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M4 T4 D4">
                <label for="address">{{ __('company.field.address') }}</label>
                <input type="text" name="address" id="address" value="{{ $company->address }}" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="city">{{ __('company.field.city') }}</label>
                <input type="text" name="city" id="city" value="{{ $company->city }}" />
            </einu-col>
            <einu-col size="M4 T4 D4">
                <label for="postal">{{ __('company.field.postal') }}</label>
                <input type="text" name="postal" id="postal" value="{{ $company->postal }}" />
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="state">{{ __('company.field.state') }}</label>
                <input type="text" name="state" id="state" value="{{ $company->state }}" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="country">{{ __('company.field.country') }}</label>
                <input disabled type="text" id="country" value="{{ $country->name }}" />
            </einu-col>
        </einu-control-group>

        <einu-control-group style="margin-top: 20px;">
            <button type="submit" class="blue">{{ __('employees.action.submit') }}</button>
        </einu-control-group>
    </form>
@endsection