@extends('layouts.app')
@section('pageTitle', 'Login')
@section('screen', 'dialog')

@section('content')
    <einu-flex-container>
        <einu-block class="dialog">
            <img class="logo" src="{{ isset($company) ? route('images.logo', $company->logo != null ? $company->logo->id : 'company') : asset('img/eem-logo.png') }}" />
            <einu-heading size="1">Login</einu-heading>
            <einu-block class="form-container">
                <form method="POST" action="{{ isset($company) ? route('login.company', [strtolower($company->country->code), $company->slug]) : route('login') }}">
                    {{ csrf_field() }}
                    <einu-control-group>
                        <label for="email">
                            Email
                            @if ($errors->has('email'))
                                <span class="error"> {{ $errors->first('email') }}</span>
                            @endif
                        </label>
                        <input type="email" id="email" name="email" autocomplete="off" spellcheck="false" value="{{ old('email') }}" />
                    </einu-control-group>

                    <einu-control-group>
                        <label for="password">
                            Password
                            @if ($errors->has('password'))
                                <span class="error"> {{ $errors->first('password') }}</span>
                            @endif
                        </label>
                        <input type="password" id="password" name="password" autocomplete="off" spellcheck="false" value="{{ old('password') }}" />
                        <a href="{{ route('password.request') }}" class="alternative">Forgot your password?</a>
                    </einu-control-group>

                    <einu-control-group>
                        <button class="blue" type="submit">Login</button>
                    </einu-control-group>
                </form>
            </einu-block>
        </einu-block>
    </einu-flex-container>
@endsection