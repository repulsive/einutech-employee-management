<einu-table style="width: 100%;" class="datagrid">
    <einu-table-header>
        <einu-table-cell>
            <script>function toggleAllCheckboxes(element){$(element).prop('checked')==1?$('.selectcheck').prop('checked',!0):$('.selectcheck').prop('checked',!1);}</script>
            <input type="checkbox" onchange="toggleAllCheckboxes(this)"/>
        </einu-table-cell>
        @foreach($dataGrid->columns as $column)
            <einu-table-cell {!! $column->sortable() ? 'class="sortable"' : '' !!}>
                @if($column->sortable() && $dataGrid->desc($column->identifier()))
                    <a href="{{ $column->ascUrl() }}" class="active">
                        {{ $column->label() }}
                        <einu-icon code="sort-down"></einu-icon>
                    </a>
                @elseif($column->sortable() && $dataGrid->asc($column->identifier()))
                    <a href="{{ $column->descUrl() }}" class="active">
                        {{ $column->label() }}
                        <einu-icon code="sort-up"></einu-icon>
                    </a>
                @elseif($column->sortable() )
                    <a href="{{ $column->descUrl() }}">
                        {{ $column->label() }}
                        <einu-icon code="sort"></einu-icon>
                    </a>
                @else
                    {{ $column->label() }}
                @endif
            </einu-table-cell>
        @endforeach
    </einu-table-header>

    @if(!$dataGrid->data->isEmpty())
        @foreach($dataGrid->data as $row)
            <einu-table-row>
                <einu-table-cell>
                    <input type="checkbox" class="selectcheck" name="selected[]" value="{{ $row['id'] }}" />
                </einu-table-cell>
                @foreach($dataGrid->columns as $column)
                    <einu-table-cell style="{!! $column->type() == "options" ? 'width: 125px;' : '' !!} {{ $column->style() }}">
                        @if($column->type() == "options")
                            <a class="button squared rounded" href="{{ route($column->routePrefix.'.show', $row[$column->routeKey]) }}"><einu-icon code="eye"></einu-icon></a>
                            <a class="button squared rounded" href="{{ route($column->routePrefix.'.edit', $row[$column->routeKey]) }}"><einu-icon code="pencil"></einu-icon></a>
                            <a class="button squared rounded red" href="{{ route($column->routePrefix.'.delete', $row[$column->routeKey]) }}"><einu-icon code="trash-o"></einu-icon></a>
                        @elseif($column->identifier() == "created_at")
                            <?php $identifier = $column->identifier() ?>
                            {{ $row[$identifier]->format('j. m. Y. H:i') }}
                        @else
                            <?php $identifier = $column->identifier() ?>
                            @if (!empty($row[$identifier]))
                                {{ $row[$identifier] }}
                            @else
                                <span class="error">
                                    <einu-icon code="exclamation-triangle"></einu-icon>
                                    {{ $column->if_null }}
                                </span>
                            @endif
                        @endif
                    </einu-table-cell>
                @endforeach
            </einu-table-row>
        @endforeach
    @else
        @section('empty.message')
            <einu-textblock class="message">{{ $dataGrid->emptyMessage }}</einu-textblock>
        @endsection
    @endif
</einu-table>
@yield('empty.message')