@extends('layouts.app')
@section('pageTitle', __('positions.title.main').': Edit')
@section('screen', 'app')
@section('sidebar_category', 'positions')

@section('content')
    <form class="einu-form" action="{{ route('positions.show', $position->id) }}" method="POST">
        {{ csrf_field() }}
        {{ method_field('patch') }}

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <einu-heading size="1">
            {{ __('positions.title.info') }}

            <a class="alternative left red" href="{{ route('positions.delete', $position->id) }}">
                <einu-icon code="trash-o"></einu-icon> {{ __('positions.action.delete') }}
            </a>
            <a class="alternative right" href="{{ route('positions.show', $position->id) }}">
                <einu-icon code="eye"></einu-icon> {{ __('positions.action.show') }}
            </a>
        </einu-heading>

        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="name">{{ __('positions.field.name') }}</label>
                <input type="text" name="name" id="name" value="{{ $position->name }}"/>
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="department">{{ __('positions.field.department') }}</label>
                <select name="department_id" id="department">
                    <option></option>
                    @foreach ($departments as $department)
                        @if ($position->department_id == $department->id)
                            <option selected value="{{ $department->id }}">{{ $department->name }}</option>
                        @else
                            <option value="{{ $department->id }}">{{ $department->name }}</option>
                        @endif
                    @endforeach
                </select>
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="skills">{{ __('positions.field.skills') }}</label>
                <input type="tags" name="skills" id="skills" value="{{ $position->skills != null ? implode(',', $position->skills) : "" }}"/>
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="deadline">{{ __('positions.field.deadline') }}</label>
                <input type="date" name="deadline" id="deadline" value="{{ $position->deadline }}"/>
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <script>
                function stateChanged(checkbox) {
                    if (checkbox.checked == true) {
                        $('#deadline').removeAttr('disabled');
                        $('#description').removeAttr('disabled');
                        $('.editor-toolbar').css('display', 'block');
                        if (simplemde.isPreviewActive()) {
                            simplemde.togglePreview();
                        }
                    } else {
                        $('#deadline').attr('disabled', '');
                        $('#description').attr('disabled', '');
                        $('.editor-toolbar').css('display', 'none');
                        if (!simplemde.isPreviewActive()) {
                            simplemde.togglePreview();
                        }
                    }
                }
            </script>
            <input style="float: left;" type="checkbox" name="state" id="state" {{ $position->state == 1 ? 'checked' : '' }} onchange="stateChanged(this)"/>
            <label style="float: left; padding: 3px; display: inline-block; width: auto;" for="state">{{ __('positions.field.state.label') }}</label>
        </einu-control-group>

        <einu-control-group>
            <label for="description">{{ __('positions.field.description') }}</label>
            <textarea rows="3" name="description" id="description" class="mde">{{ $position->description }}</textarea>
        </einu-control-group>
        <einu-control-group style="margin-top: 20px;">
            <button type="submit" class="blue">{{ __('positions.action.submit') }}</button>
        </einu-control-group>
    </form>
@endsection

@push('styles')
<link type="text/css"   rel="stylesheet"    href="{{ asset('css/simplemde.min.css') }}">
@endpush

@push('scripts-head')
<script src="{{ asset('js/input-tags.js') }}"></script>
@endpush

@push('scripts')
<script src="{{ asset('js/simplemde.min.js') }}"></script>
<script>
    var simplemde = new SimpleMDE({
        element: document.getElementsByClassName("mde")[0],
        status: false
    });

    [].forEach.call(document.querySelectorAll('input[type="tags"]'), tagsInput);
</script>
@endpush