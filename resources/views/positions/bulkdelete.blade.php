@extends('layouts.app')
@section('pageTitle', __('positions.title.main').': Delete')
@section('screen', 'dialog')
@section('sidebar_category', 'positions')

@section('content')
    <einu-flex-container>
        <einu-block class="dialog">
            <img class="logo" src="{{ asset('img/eem-logo.png') }}" />
            <einu-heading size="1">{{ __('positions.title.delete') }}</einu-heading>
            <einu-block class="form-container">
                <form action="{{ route('positions.bulkdelete') }}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('delete') }}

                    <input type="hidden" name="selected" value="{{ $selected }}" />

                    <einu-control-group>
                        <einu-textblock>{{ __('positions.message.deletebulk') }}</einu-textblock>
                    </einu-control-group>
                    <einu-control-group>
                        <einu-list>
                            @foreach ($positions as $position)
                                <einu-list-item>{{ $position->name }}</einu-list-item>
                            @endforeach
                        </einu-list>
                    </einu-control-group>

                    <einu-control-group>
                        <einu-col size="M6 T6 D6">
                            <button name="submit" value="yes" class="green" type="submit">{{ __('positions.action.yes') }}</button>
                        </einu-col>
                        <einu-col size="M6 T6 D6">
                            <button name="submit" value="no" class="red" type="submit">{{ __('positions.action.no') }}</button>
                        </einu-col>
                    </einu-control-group>
                </form>
            </einu-block>
        </einu-block>
    </einu-flex-container>
@endsection