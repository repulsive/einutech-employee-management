@extends('layouts.app')
@section('pageTitle', __('positions.title.main').': '.$position->name)
@section('screen', 'app')
@section('sidebar_category', 'positions')

@section('content')
    <form class="einu-form">
        <einu-heading size="1">
            {{ __('positions.title.info') }}

            <a class="alternative left" href="{{ route('positions.edit', $position->id) }}">
                <einu-icon code="pencil"></einu-icon> <span>{{ __('positions.action.edit') }}</span>
            </a>
            <a class="alternative right red" href="{{ route('positions.delete', $position->id) }}">
                <einu-icon code="trash-o"></einu-icon> <span>{{ __('positions.action.delete') }}</span>
            </a>
        </einu-heading>
        <einu-control-group>
            <einu-col size="M12 T6 D6">
                <label for="name">{{ __('positions.field.name') }}</label>
                <input disabled type="text" id="name" value="{{ $position->name }}"/>
            </einu-col>
            <einu-col size="M12 T6 D6">
                <label for="department">{{ __('positions.field.department') }}</label>
                <input disabled type="text" id="department" value="{{ $position->department != null ? $position->department->name : __('positions.error.department.unavailable') }}"/>
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="skills">{{ __('positions.field.skills') }}</label>
                <input disabled type="tags" id="skills" value="{{ $position->skills != null ? implode(',', $position->skills) : "" }}"/>
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="deadline">{{ __('positions.field.deadline') }}</label>
                <input disabled type="date" id="deadline" value="{{ $position->deadline }}"/>
            </einu-col>
        </einu-control-group>

        <einu-control-group>
            <label for="description">{{ __('positions.field.description') }}</label>
            <einu-block class="markdown" id="description">{!! $description !!}</einu-block>
        </einu-control-group>
        <einu-control-group>
            <input disabled style="float: left;" type="checkbox" id="state" {{ $position->state == 1 ? 'checked' : '' }}/>
            <label style="float: left; padding: 3px; display: inline-block; width: auto;" for="state">{{ __('positions.field.state.label') }}</label>
        </einu-control-group>
    </form>
@endsection

@push('scripts-head')
<script src="{{ asset('js/input-tags.js') }}"></script>
@endpush

@push('scripts')
<script src="{{ asset('js/simplemde.min.js') }}"></script>
<script>
    [].forEach.call(document.querySelectorAll('input[type="tags"]'), tagsInput);
</script>
@endpush