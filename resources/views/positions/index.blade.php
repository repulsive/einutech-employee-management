@extends('layouts.app')
@section('pageTitle', __('positions.title.main'))
@section('screen', 'app')
@section('sidebar_category', 'positions')

@section('content')
    {!! $dataGrid->data->appends(Request::except('page'))->links('_partials.pagination') !!}

    <form action="{{ route('positions.bulkdelete') }}" method="POST">
        {{ csrf_field() }}

        <einu-block class="section">
            <einu-control-group>
                <einu-textblock>{{ route('embed.index', auth()->user()->company->id) }}</einu-textblock>
            </einu-control-group>
        </einu-block>
        <einu-block class="section">
            <einu-heading size="1">
                {{ __('positions.title.main') }}

                <a href="{{ route('positions.download.spreadsheet') }}" class="alternative left">
                    <einu-icon code="download"></einu-icon> <span>{{ __('positions.action.download') }} (.xlsx)</span>
                </a>
                <a href="{{ route('positions.create') }}" class="alternative right green">
                    <einu-icon code="plus"></einu-icon> <span>{{ __('positions.action.new') }}</span>
                </a>
            </einu-heading>
            {!! $dataGrid->show() !!}

            {{--<einu-table style="width: 100%;">--}}
                {{--<einu-table-header>--}}
                    {{--<einu-table-cell>--}}
                        {{--<script>function toggleAllCheckboxes(element){$(element).prop('checked')==1?$('.selectcheck').prop('checked',!0):$('.selectcheck').prop('checked',!1);}</script>--}}
                        {{--<input type="checkbox" onchange="toggleAllCheckboxes(this)"/>--}}
                    {{--</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('positions.field.id') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('positions.field.name') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('positions.field.state') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('positions.field.department') }}</einu-table-cell>--}}
                    {{--<einu-table-cell>{{ __('positions.field.options') }}</einu-table-cell>--}}
                {{--</einu-table-header>--}}
                {{--@foreach ($positions as $position)--}}
                    {{--<einu-table-row>--}}
                        {{--<einu-table-cell>--}}
                            {{--<input type="checkbox" class="selectcheck" name="selected[]" value="{{ $position->id }}" />--}}
                        {{--</einu-table-cell>--}}
                        {{--<einu-table-cell style="font-family: 'Source Code Pro';">{{ $position->id }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{{ $position->name }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{{ $position->state == 1 ? __('positions.field.state.open') : __('positions.field.state.closed') }}</einu-table-cell>--}}
                        {{--<einu-table-cell>{!! ($position->department !== null) ? $position->department->name : '<span class="error"><einu-icon code="exclamation-triangle"></einu-icon> '.__('positions.error.department.unavailable').'</span>' !!}</einu-table-cell>--}}
                        {{--<einu-table-cell style="width: 125px;">--}}
                            {{--<a class="button squared rounded" href="{{ route('positions.show', $position->id) }}"><einu-icon code="eye"></einu-icon></a>--}}
                            {{--<a class="button squared rounded" href="{{ route('positions.edit', $position->id) }}"><einu-icon code="pencil"></einu-icon></a>--}}
                            {{--<a class="button squared rounded red" href="{{ route('positions.delete', $position->id) }}"><einu-icon code="trash-o"></einu-icon></a>--}}
                        {{--</einu-table-cell>--}}
                    {{--</einu-table-row>--}}
                {{--@endforeach--}}
            {{--</einu-table>--}}
        </einu-block>
        <einu-block class="action-bar">
            <einu-control-group>
                <button class="red rounded" type="submit">
                    {{ __('positions.action.deletebulk') }}
                </button>
            </einu-control-group>
        </einu-block>
    </form>

    {!! $dataGrid->data->appends(Request::except('page'))->links('_partials.pagination') !!}
@endsection