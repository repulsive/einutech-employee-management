@extends('layouts.app')
@section('pageTitle', __('positions.title.main').': Create')
@section('screen', 'app')
@section('sidebar_category', 'positions')

@section('content')
    <form class="einu-form" action="{{ route('positions.index') }}" method="POST">
        {{ csrf_field() }}

        <einu-heading size="1">{{ __('positions.title.info') }}</einu-heading>
        <einu-control-group>
            <einu-col size="M6 T6 D6">
                <label for="name">{{ __('positions.field.name') }}</label>
                <input type="text" name="name" id="name" />
            </einu-col>
            <einu-col size="M6 T6 D6">
                <label for="department">{{ __('positions.field.department') }}</label>
                <select name="department_id" id="department">
                    <option selected></option>
                    @foreach ($departments as $department)
                        <option value="{{ $department->id }}">{{ $department->name }}</option>
                    @endforeach
                </select>
            </einu-col>
        </einu-control-group>
        <einu-control-group>
            <label for="description">{{ __('positions.field.description') }}</label>
            <textarea rows="3" name="description" id="description"></textarea>
        </einu-control-group>
        <einu-control-group>
            <input type="checkbox" name="state" id="state" />
            <label for="state">{{ __('positions.field.state.label') }}</label>
        </einu-control-group>

        <einu-control-group style="margin-top: 20px;">
            <button type="submit" class="blue">{{ __('positions.action.submit') }}</button>
        </einu-control-group>
        <einu-control-group>
            <input type="checkbox" name="continue" id="continue" {{ isset($continue) ? 'checked' : '' }} />
            <label for="continue">{{ __('positions.field.continue.label') }}</label>
        </einu-control-group>
    </form>
@endsection