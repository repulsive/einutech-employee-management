(function() {
    function CreateSlider(value, index, ar) {
        var mousePosition;
        var sliderValue;
        var sliderPos = 0;
        var offset = { x: 0, y: 0 };
        var isDown = false;
        var isHovering = false;

        var slider = document.getElementsByTagName("einu-slider")[index];
        var sliderThumb = document.createElement("einu-slider-thumb");
        var sliderRail = document.createElement("einu-slider-rail");
        var sliderRailTrail = document.createElement("einu-slider-rail-trail");

        slider.appendChild(sliderThumb);
        slider.appendChild(sliderRail);
        sliderRail.appendChild(sliderRailTrail);

        var sliderVal = slider.getAttribute("value");
        var rotation = slider.getAttribute("rotation");
        var isDisabled = slider.hasAttribute("disabled");
        var maxValue = slider.hasAttribute("max") ? slider.getAttribute("max") : 100;
        var defaultValue = slider.hasAttribute("default") ? slider.getAttribute("default") : maxValue / 2;

        if (sliderVal == null) {
            sliderThumb.setAttribute("title", "0.00%");
            slider.setAttribute("value", "0.00");

            sliderValue = 0;
        } else {
            sliderThumb.setAttribute("title", sliderVal + "%");

            sliderValue = sliderVal;

            moveThumb(calculateOffset(sliderVal));
        }

        sliderThumb.addEventListener('mousedown', function(event) {
            if(isDisabled == false) {
                isDown = true;
                offset = { x: sliderThumb.offsetLeft - event.clientX, y: sliderThumb.offsetTop - event.clientY };
            }
        }, true);

        sliderThumb.addEventListener('dblclick', function(event) {
            moveThumb(calculateOffset(defaultValue));
        }, true);

        sliderRail.addEventListener('mousedown', function(event) {
            if(isDisabled == false) {
                mousePosition = { x: event.clientX, y: event.clientY };

                var leftPos = (mousePosition.x - sliderRail.getBoundingClientRect().left - (sliderThumb.clientWidth / 2));
                var topPos = (mousePosition.y - sliderRail.getBoundingClientRect().top - (sliderThumb.clientHeight / 2));

                if(rotation == "vertical") {
                    moveThumb(topPos);
                } else {
                    moveThumb(leftPos);
                }
            }
        }, true);

        slider.addEventListener('mouseenter', function() {
            isHovering = true;
        }, true);

        slider.addEventListener('mouseleave', function() {
            isHovering = false;
        }, true);

        window.addEventListener('wheel', function(e){
            if(isDisabled == false) {
                wheelDirection = e.deltaY > 0 ? 'down' : 'up';
                if(isHovering == true) {
                    if(wheelDirection == 'up') {
                        moveThumb(sliderPos + 5);
                    }
                    if(wheelDirection == 'down') {
                        moveThumb(sliderPos - 5);
                    }
                }
            }
        });

        document.addEventListener('mouseup', function() {
            isDown = false;
        }, true);

        document.addEventListener('mousemove', function(event) {
            if(isDisabled == false) {
                if(isDown) {
                    event.preventDefault();
                    mousePosition = { x : event.clientX, y: event.clientY };

                    var leftPos = (mousePosition.x + offset.x);
                    var topPos = (mousePosition.y + offset.y);

                    if(rotation == "vertical") {
                        moveThumb(topPos);
                    } else {
                        moveThumb(leftPos);
                    }
                }
            }
        }, true);

        function moveThumb(position) {
            if(position < 0) {
                position = 0;
            }

            if(rotation == "vertical") {
                if(position > (slider.clientHeight - sliderThumb.clientHeight)) {
                    position = (slider.clientHeight - sliderThumb.clientHeight);
                }

                sliderPos = position;
                sliderThumb.style.top = position + 'px';
            } else {
                if(position > (slider.clientWidth - sliderThumb.clientWidth)) {
                    position = (slider.clientWidth - sliderThumb.clientWidth);
                }

                sliderPos = position;
                sliderThumb.style.left = position + 'px';
                sliderRailTrail.style.width = position + 'px';
            }

            sliderValue = parseFloat(calculatePercentage(position)).toFixed(2);

            sliderThumb.setAttribute("title", sliderValue + "%");
            slider.setAttribute("value", sliderValue);

            if (window.CustomEvent) {
                var event = new CustomEvent("slide", { detail: { value: sliderValue }});

                slider.dispatchEvent(event);
            }
        }

        function calculatePercentage(offset) {
            if(rotation == "vertical") {
                var p = ((slider.clientHeight - sliderThumb.clientHeight) / maxValue);
                return offset / p;
            } else {
                var p = ((slider.clientWidth - sliderThumb.clientWidth) / maxValue);
                return offset / p;
            }
        }

        function calculateOffset(percentage) {
            if(rotation == "vertical") {
                var p = ((slider.clientHeight - sliderThumb.clientHeight) / maxValue);
            } else {
                var p = ((slider.clientWidth - sliderThumb.clientWidth) / maxValue);
            }
            return percentage * p;
        }
    }

    var sliders = Array.prototype.slice.call(document.getElementsByTagName("einu-slider"));
    sliders.forEach(CreateSlider);
})();