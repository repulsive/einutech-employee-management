<?php

namespace Einutech\Support;

class Cart
{
    /**
     * Instance of the cart class
     *
     * @var
     */
    private static $instance;

    /**
     * Cart array
     *
     * @var array
     */
    protected $cart = [];

    /**
     * Cart JSON string
     *
     * @var
     */
    protected $cartString;

    /**
     * Get singleton instance of the cart class
     *
     * @return mixed
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    protected function __construct()
    {
        $this->deserialize();
    }

    private function __clone() {}
    private function __wakeup() {}

    /**
     * Deserialize the cart cookie
     *
     * @return $this
     */
    protected function deserialize()
    {
        $this->cartString = $_COOKIE['cart'];
        $this->cart = json_decode($this->cartString);

        return $this;
    }

    /**
     * Serialize the cart array
     *
     * @return $this
     */
    protected function serialize()
    {
        $this->cartString = json_encode($this->cart);

        setcookie('cart', $this->cartString, time() + (86400 * 30), "/");

        return $this;
    }

    /**
     * Insert an item to the cart
     *
     * @param $id
     * @param $quantity
     * @param $price
     * @param $name
     * @param array $attributes
     * @return bool
     */
    public function insert($id, $quantity, $price, $name, $attributes = [])
    {
        $this->deserialize();

        if (!array_key_exists($id, $this->cart)) {
            $this->cart[$id] = [
                'quantity' => $quantity,
                'price' => $price,
                'name' => $name,
                'attributes' => $attributes
            ];

            $this->serialize();

            return true;
        }

        return false;
    }

    /**
     * Remove an item from the cart
     *
     * @param $id
     * @return bool
     */
    public function remove($id)
    {
        $this->deserialize();

        if (!array_key_exists($id, $this->cart)) {
            unset($id, $this->cart);

            $this->serialize();

            return true;
        }

        return false;
    }

    /**
     * Update an item in the cart
     *
     * @param $id
     * @param $quantity
     * @param $price
     * @param $name
     * @param array $attributes
     * @return bool
     */
    public function update($id, $quantity, $price, $name, $attributes = [])
    {
        $this->deserialize();

        if (array_key_exists($id, $this->cart)) {
            $this->cart[$id] = [
                'quantity' => $quantity,
                'price' => $price,
                'name' => $name,
                'attributes' => $attributes
            ];

            $this->serialize();

            return true;
        }

        return false;
    }

    /**
     * Clear the cart
     *
     * @return $this
     */
    public function clear()
    {
        $this->deserialize();

        unset($this->cart);
        $this->cart = [];

        $this->serialize();

        return $this;
    }

    /**
     * Return number of items currently in the cart
     *
     * @return int
     */
    public function count()
    {
        $this->deserialize();

        return count($this->cart);
    }
}