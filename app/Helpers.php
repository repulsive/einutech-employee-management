<?php

/**
 * Format bytes to KB, MB, GB, TB, PB, EB, ZB, YB
 *
 * @param  integer $size
 * @param  integer $precision
 * @return integer
 */
function formatBytes($size, $precision = 2)
{
    if ($size > 0) {
        $size = (int) $size;
        $base = log($size) / log(1024);
        $suffixes = array(' B', ' KB', ' MB', ' GB', ' TB', ' PB', ' EB', ' ZB', ' YB');

        return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
    } else {
        return $size;
    }
}

/**
 * Generates a HEX color code from a string
 *
 * @param  string $string
 * @return string
 */
function generateHexColorFromString($string) {
    $code = dechex(crc32($string));
    $code = substr($code, 0, 6);
    return "#{$code}";
}