<?php

namespace App\Transformers;

use App\Models\Position;
use League\Fractal\TransformerAbstract;

class PositionTransformer extends TransformerAbstract
{
    public function transform(Position $position)
    {
        $department = $position->department != null ? $position->department : null;
        $company = $position->company;

        return [
            'id' => $position->id,

            'name' => $position->name,
            'state' => $position->state == 1 ? __('positions.field.state.open') : __('positions.field.state.closed'),

            'company_id' => $company->id,
            'company_name' => $company->name,

            'department_id' => $department != null ? $department->id : null,
            'department_name' => $department != null ? $department->name : null,
        ];
    }
}