<?php

namespace App\Transformers;

use App\Models\Department;
use League\Fractal\TransformerAbstract;

class DepartmentTransformer extends TransformerAbstract
{
    public function transform(Department $department)
    {
        $company = $department->company;

        return [
            'id' => $department->id,

            'name' => $department->name,

            'company_id' => $company->id,
            'company_name' => $company->name,
        ];
    }
}