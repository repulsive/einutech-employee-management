<?php

namespace App\Transformers;

use App\Models\Employee;
use League\Fractal\TransformerAbstract;

class EmployeeTransformer extends TransformerAbstract
{
    public function transform(Employee $employee)
    {
        $position = $employee->position != null ? $employee->position : null;
        $department = $position != null ? $position->department != null ? $position->department : null : null;
        $company = $employee->company;

        return [
            'id' => $employee->id,

            'first_name' => $employee->first_name,
            'last_name' => $employee->last_name,

            'email' => $employee->email,

            'company_id' => $company->id,
            'company_name' => $company->name,

            'position_id' => $position != null ? $position->id : null,
            'position_name' => $position != "" ? $position->name : "",

            'department_id' => $department != null ? $department->id : null,
            'department_name' => $department != "" ? $department->name : "",

            'score' => $employee->score,
        ];
    }
}