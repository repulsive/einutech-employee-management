<?php

namespace App\Transformers;

use App\Models\Candidate;
use League\Fractal\TransformerAbstract;

class CandidateTransformer extends TransformerAbstract
{
    public function transform(Candidate $candidate)
    {
        $position = $candidate->position != null ? $candidate->position : null;
        $department = $position != null ? $position->department != null ? $position->department : null : null;
        $company = $candidate->company;

        return [
            'id' => $candidate->id,

            'first_name' => $candidate->first_name,
            'last_name' => $candidate->last_name,

            'email' => $candidate->email,

            'company_id' => $candidate->id,
            'company_name' => $candidate->name,

            'position_id' => $position != null ? $position->id : null,
            'position_name' => $position != "" ? $position->name : "",

            'department_id' => $department != null ? $department->id : null,
            'department_name' => $department != "" ? $department->name : "",

            'score' => $candidate->score,
        ];
    }
}