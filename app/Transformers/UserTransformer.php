<?php

namespace App\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        $name = explode(' ', $user->name);

        return [
            'id' => $user->id,

            'first_name' => $name[0],
            'last_name' => end($name),

            'email' => $user->email,
        ];
    }
}