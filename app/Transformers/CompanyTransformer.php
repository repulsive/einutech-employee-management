<?php

namespace App\Transformers;

use App\Models\Company;
use League\Fractal\TransformerAbstract;

class CompanyTransformer extends TransformerAbstract
{
    public function transform(Company $company)
    {
        return [
            'id' => $company->id,

            'name' => $company->name,

            'country_id' => $company->country->id,
            'country_code' => $company->country->code,
            'country_name' => $company->country->name,

            'company_number' => $company->company_number,
            'tax_id_number' => $company->tax_id_number,
            'activity_code' => $company->activity_code,
        ];
    }
}