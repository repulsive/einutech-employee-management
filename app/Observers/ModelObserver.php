<?php

namespace App\Observers;

use App\Models\BaseModel;
use MongoDB\BSON\UTCDatetime;
use Illuminate\Support\Facades\DB;

class ModelObserver
{
    protected $model;

    /**
     * Listen to the BaseModel created event.
     *
     * @param  \App\Models\BaseModel  $model
     * @return void
     */
    public function created(BaseModel $model)
    {
        $this->model = $model;
        $this->log('created');
    }

    /**
     * Listen to the BaseModel updated event.
     *
     * @param  \App\Models\BaseModel  $model
     * @return void
     */
    public function updated(BaseModel $model)
    {
        $this->model = $model;
        $this->log('updated');
    }

    private function log(string $event)
    {
        $date = new UTCDateTime(time());

        DB::connection('mongodb')->collection('log')->insert([
            'type' => get_class($this->model),
            'id' => $this->model->id,
            'event' => $event,
            'timestamp' => $date
        ]);
    }
}