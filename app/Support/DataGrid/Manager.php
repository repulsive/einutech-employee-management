<?php

namespace App\Support\DataGrid;

use App\Support\DataGrid\Columns\ImageColumn;
use App\Support\DataGrid\Columns\LinkColumn;
use App\Support\DataGrid\Columns\OnlineColumn;
use App\Support\DataGrid\Columns\OptionsColumn;
use App\Support\DataGrid\Columns\TextColumn;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class Manager
{
    public $request;
    public $data;
    public $collection;
    public $columns = NULL;
    public $emptyMessage;

    protected $pageItem = 25;
    protected $defaultOrder = ['field' => 'id', 'keyword' => 'asc'];
    protected $orderByDisabled = false;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->columns = Collection::make([]);
    }

    /**
     * Used to paginate a collection or an array
     *
     * @param array|Collection $items
     * @param int   $perPage
     * @param int  $page
     * @param array $options
     *
     * @return LengthAwarePaginator
     */
    public function paginateCollection($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function collection($collection)
    {
        $this->collection = $collection;
        return $this;
    }

    public function column($identifier, $option = [])
    {
        $column = new TextColumn($identifier, $option);
        $this->columns->put($identifier, $column);

        return $this;
    }

    public function setDefaultOrder($orderBy)
    {
        $this->defaultOrder = $orderBy;
        return $this;
    }

    public function setEmptyMessage($message)
    {
        $this->emptyMessage = $message;
        return $this;
    }

    public function disableDefaultOrderBy()
    {
        $this->orderByDisabled = true;
        return $this;
    }

    public function setPagination($item = 10)
    {
        $this->pageItem = $item;
        return $this;
    }

    public function generate()
    {
        if (null !== $this->request->get('asc')) {
            $this->collection = $this->collection->sortBy($this->request->get('asc'));
        }

        if (null !== $this->request->get('desc')) {
            $this->collection = $this->collection->sortByDesc($this->request->get('desc'));
        }

        if ($this->orderByDisabled == false) {
            if ($this->request->get('asc') == null && $this->request->get('desc') == null) {
                if ($this->defaultOrder['keyword'] == 'asc') {
                    $this->collection = $this->collection->sortBy($this->defaultOrder['field']);
                }

                if ($this->defaultOrder['keyword'] == 'desc') {
                    $this->collection = $this->collection->sortByDesc($this->defaultOrder['field']);
                }
            }
        }

        $this->collection = $this->collection->values()->all();

        $this->data = $this->paginateCollection($this->collection, $this->pageItem, Paginator::resolveCurrentPage(), [
            'path' => Paginator::resolveCurrentPath()
        ]);

        return $this;
    }

    public function show()
    {
        return view('_partials.datagrid')
            ->with('dataGrid', $this);
    }

    public function asc($identifier = "")
    {
        return (NULL !== $this->request->get('asc') && $this->request->get('asc') == $identifier);
    }

    public function desc($identifier = "")
    {
        return (NULL !== $this->request->get('desc') && $this->request->get('desc') == $identifier);
    }
    
    public function optionsColumn($identifier, $routePrefix, $routeKey = 'id', $option = [])
    {
        $column = new OptionsColumn($identifier, $routePrefix, $routeKey, $option);
        $this->columns->put($identifier, $column);

        return $this;
    }

    public function dataTableData($collection)
    {
        $this->collection = $collection;
        return $this;
    }

    public function get()
    {
        $count = $this->collection->count();

        $columns = $this->request->get('columns');

        $orders = $this->request->get('order');

        $order = $orders[0];

        $records = $this->collection->orderBy($columns[$order['column']]['name'], $order['dir']);

        $noOfRecord = $this->request->get('length');
        $noOfSkipRecord = $this->request->get('start');

        $records->skip($noOfSkipRecord)->take($noOfRecord);

        $allRecords = $records->get();

        if (isset($this->columns) && $this->columns->count() > 0) {
            $jsonRecords = Collection::make([]);

            foreach ($allRecords as $i => $singleRecord) {
                foreach ($this->columns as $key => $columnData) {
                    if (is_callable($columnData)) {
                        $columnValue = $columnData($singleRecord);
                    } else {
                        $columnValue = $columnData;
                    }

                    $singleRecord->setAttribute($key, $columnValue);
                }

                $jsonRecords->put($i, $singleRecord);
            }
        }

        $data = [
            "data" => (isset($jsonRecords)) ? $jsonRecords : $allRecords,
            "draw" => $this->request->get('draw'),
            "recordsTotal" => $count,
            "recordsFiltered" => $count
        ];

        return JsonResponse::create($data);
    }

    public function addColumn($columnKey, $data)
    {
        if (NULL === $this->columns) {
            $this->columns = Collection::make([]);
        }
        $this->columns->put($columnKey, $data);
        return $this;
    }
}