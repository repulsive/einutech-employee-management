<?php

namespace App\Support\DataGrid\Columns;

use Illuminate\Support\Facades\Route;

class TextColumn extends AbstractColumn
{
    protected $type = 'text';

    public function __construct($identifier, $options)
    {
        $this->if_null = (isset($options['if_null'])) ? $options['if_null'] : false;
        
        parent::__construct($identifier, $options);
    }

    public function ascUrl()
    {
        $currentRouteName = Route::getCurrentRoute()->getName();
        return route($currentRouteName, ['asc' => $this->identifier()]);
    }

    public function descUrl()
    {
        $currentRouteName = Route::getCurrentRoute()->getName();
        return route($currentRouteName, ['desc' => $this->identifier()]);
    }
}