<?php

namespace App\Support\DataGrid\Columns;

class ImageColumn extends AbstractColumn
{
    protected $type = 'image';
}