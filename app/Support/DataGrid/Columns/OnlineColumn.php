<?php

namespace App\Support\DataGrid\Columns;

use Illuminate\Support\Facades\Route;

class OnlineColumn extends AbstractColumn
{
    protected $type = 'online';
}