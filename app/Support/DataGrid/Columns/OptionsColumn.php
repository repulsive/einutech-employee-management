<?php

namespace App\Support\DataGrid\Columns;

use Illuminate\Support\Facades\Route;

class OptionsColumn extends AbstractColumn
{
    protected $type = 'options';

    public $routePrefix;
    public $routeKey;

    public function __construct($identifier, $routePrefix, $routeKey = 'id', $options)
    {
        parent::__construct($identifier, $options);

        $this->routePrefix = $routePrefix;
        $this->routeKey = $routeKey;
    }
}