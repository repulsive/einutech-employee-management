<?php

namespace App\Models;

class Employee extends BaseModel
{
    protected $fillable = [
        'company_id', 'first_name', 'last_name', 'email', 'phone',
        'address', 'postal', 'city', 'state', 'country_id', 'umcn',
        'photo', 'birth_date', 'start_date', 'sex', 'position_id',
        'skills', 'custom_data'
    ];

    protected $with = [
        'country', 'company', 'position'
    ];

    protected $appends = [
        'score'
    ];

    /**
     * Employee's company
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * Employee's company
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Employee's position
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function position()
    {
        return $this->belongsTo(Position::class);
    }

    /**
     * Image of the employee
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'owner');
    }

    /**
     * All files related to the employee
     *
     * @return \Jenssegers\Mongodb\Relations\EmbedsMany
     */
    public function files()
    {
        return $this->embedsMany(File::class, 'files');
    }

    /**
     * Combined skills of the employee and his position
     *
     * @return array
     */
    public function allSkills() {
        $array = array_values(array_flip(array_map(function ($value) {
            global $i;
            return $i++;
        }, array_unique(array_map(function ($value) {
            global $i;
            return $i++;
        },
            array_merge(
                array_flip($this->position->skills),
                $this->skills
            )), SORT_REGULAR
        ))));

        return $array;
    }

    /**
     * Employee's score based on his skills
     *
     * @return string
     */
    public function getScoreAttribute()
    {
        if ($this->skills != null && $this->position->skills != null) {
            $positionSkills = $this->position->skills;
            $numSkills = count($positionSkills);
            $sum = 0.0; 

            foreach ($positionSkills as $skill) { $sum += array_key_exists($skill, $this->skills) ? $this->skills[$skill] : 0; }

            $score = floatval(($sum / $numSkills) / 10);

            return number_format($score, 1);
        } else {
            return 'n/a';
        }
    }
}
