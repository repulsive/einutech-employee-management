<?php

namespace App\Models;

use Illuminate\Support\Facades\Storage;

class File extends BaseModel
{
    protected $guarded = ['id'];

    public static function upload($fileInstance)
    {
        $path = $fileInstance->store('files' . DIRECTORY_SEPARATOR . auth()->user()->company->slug);

        $svg = Storage::get('assets' . DIRECTORY_SEPARATOR . 'file.svg');
        $svg = str_replace('{{EXT}}', ltrim(strtoupper($fileInstance->getClientOriginalExtension()), '.'), $svg);
        $svg = str_replace('{{COLOR}}', generateHexColorFromString($fileInstance->getClientOriginalExtension()), $svg);

        $icon_base64 = 'data:image/svg+xml;base64,'.base64_encode($svg);

        $file = new self([
            'name' => $fileInstance->getClientOriginalName(),
            'extension' => $fileInstance->getClientOriginalExtension(),
            'mime' => $fileInstance->getClientMimeType(),
            'size' => $fileInstance->getSize(),
            'path' => $path,
            'icon' => $icon_base64
        ]);

        return $file;
    }
}
