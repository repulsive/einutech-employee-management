<?php

namespace App\Models;

class Candidate extends BaseModel
{
    protected $fillable = [
        'company_id', 'first_name', 'last_name', 'email', 'phone',
        'address', 'postal', 'city', 'state', 'country_id', 'umcn',
        'photo', 'birth_date', 'start_date', 'sex', 'position_id',
        'custom_data'
    ];

    protected $with = [
        'country', 'company', 'position'
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function position()
    {
        return $this->belongsTo(Position::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'owner');
    }

    public function files()
    {
        return $this->morphMany(File::class, 'owner');
    }
}
