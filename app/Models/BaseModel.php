<?php

namespace App\Models;

/** Eloquent Model : MySQL */
use App\Observers\ModelObserver;
use Illuminate\Database\Eloquent\Model as EloquentModel;

/** Moloquent Model : MongoDB */
use Jenssegers\Mongodb\Eloquent\Model as MoloquentModel;

use App\Events\ModelCreated;
use App\Events\ModelUpdated;

abstract class BaseModel extends MoloquentModel
{
    protected $dispatchesEvents = [
        'created' => ModelCreated::class,
        'updated' => ModelUpdated::class
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->collection = str_plural(strtolower((new \ReflectionClass($this))->getShortName()));
    }
}