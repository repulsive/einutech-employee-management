<?php

namespace App\Models;

class Country extends BaseModel
{
    protected $fillable = [
        'code', 'name'
    ];
    
    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    public function candidates()
    {
        return $this->hasMany(Candidate::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function companies()
    {
        return $this->hasMany(Company::class);
    }
}
