<?php

namespace App\Models;

class Setting extends BaseModel
{
    protected $guarded = ['id'];

    /**
     * Default settings
     *
     * @var array
     */
    protected static $defaults = [
        'theme' => 'light',
        'language' => 'sr',
        'tfa' => 'false',
    ];

    /**
     * User instance that these settings belong to
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get value of a user's setting
     *
     * @param $key
     * @return mixed
     */
    public static function get($key)
    {
        if (auth()->check()) {
            $setting = self::where('user_id', auth()->user()->id)
                ->where('key', $key)
                ->first();

            return !$setting ? self::$defaults[$key] : $setting->value;
        } else {
            return self::$defaults[$key];
        }
    }

    /**
     * Sets a value to a user's setting /
     * Creates a new user's setting
     *
     * @param $key
     * @param $value
     * @return mixed
     */
    public static function set($key, $value)
    {
        $setting = self::where('user_id', auth()->user()->id)
            ->where('key', $key)
            ->first();

        if (!$setting) {
            $setting = self::create(['key' => $key, 'value' => $value]);
            return $setting->value;
        } else {
            $setting->update(['value' => $value]);
            return $setting->value;
        }
    }
}
