<?php

namespace App\Models;

class Role extends BaseModel
{
    protected $fillable = [
        'name', 'level'
    ];
    
    protected $hidden = [
        'created_at', 'updated_at', 'pivot', 'id'
    ];

    public function is($name)
    {
        return (strtolower($this->name) == strtolower($name)) ? true : false;
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
