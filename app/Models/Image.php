<?php

namespace App\Models;

use Illuminate\Http\Request;

class Image extends BaseModel
{
    protected $guarded = ['id'];

    public function owner()
    {
        return $this->morphTo();
    }

    public static function upload(Request $request)
    {
        $uploaded_file = $request->file('image');
        $path = $uploaded_file->store('images/' . auth()->user()->company->slug);

        $image = self::create([
            'extension' => $uploaded_file->getClientOriginalExtension(),
            'name' => $uploaded_file->getClientOriginalName(),
            'size' => $uploaded_file->getSize(),
            'mime' => $uploaded_file->getClientMimeType(),
            'path' => $path
        ]);

        return $image;
    }
}
