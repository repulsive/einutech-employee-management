<?php

namespace App\Models;

use Hashids\Hashids;

class Position extends BaseModel
{
    protected $fillable = [
        'company_id', 'department_id', 'name', 'description', 'state', 'deadline', 'skills'
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        static::deleted(function () {
            foreach ($this->employees as $employee) {
                $employee->position_id = 0;
                $employee->save();
            }
        });
    }

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }

    public function candidates()
    {
        return $this->hasMany(Candidate::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function hashid()
    {
        $hashids = new Hashids('Positions', 16);
        return $hashids->encode($this->id);
    }
}
