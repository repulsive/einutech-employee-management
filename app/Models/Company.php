<?php

namespace App\Models;

use Hashids\Hashids;

class Company extends BaseModel
{
    protected $fillable = [
        'name', 'code', 'address', 'postal', 'city',
        'state', 'country_id', 'logo', 'phone', 'email',
        'tax_id_number', 'company_number', 'activity_code',
    ];

    protected $with = [
        'country',
    ];
    
    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
    
    public function candidates()
    {
        return $this->hasMany(Candidate::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function positions()
    {
        return $this->hasMany(Position::class);
    }

    public function departments()
    {
        return $this->hasMany(Department::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function image()
    {
        return $this->embedsOne(Image::class, 'image');
    }
}
