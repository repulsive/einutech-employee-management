<?php

namespace App\Models;

class Department extends BaseModel
{
    protected $guarded = ['id'];

    public function positions()
    {
        return $this->hasMany(Position::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
