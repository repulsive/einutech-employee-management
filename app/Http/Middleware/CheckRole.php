<?php

namespace App\Http\Middleware;

use App\Models\Role;
use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (auth()->check()) {
            $neededRole = Role::where('name', ucfirst($role))->firstOrFail();

            if (auth()->user()->highestRole()->level >= $neededRole->level) {
                return $next($request);
            }

            abort(403);
        }
        return $next($request);
    }
}