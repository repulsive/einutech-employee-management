<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApplicationFormRequest;
use Illuminate\Http\Request;
use App\Models\Candidate;
use App\Models\Position;
use Hashids\Hashids;

class ApplicationsController extends Controller
{
    public function show($position_id)
    {
        $position = Position::where('_id', $position_id)->where('state', 1)->firstOrFail();

        return view('applications.show', [
            'position_id' => $position_id,
            'position' => $position,
        ]);
    }

    public function store(ApplicationFormRequest $request, $hashid)
    {
        $hashids = new Hashids('Positions', 16);
        $id = $hashids->decode($hashid);

        $position = Position::where('id', $id)->where('state', 1)->firstOrFail();
        $company = $position->company;

        $candidate = Candidate::create($request->all());
        $company->candidates()->save($candidate);
        $candidate->positions()->save($position);
    }
}
