<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\JsonResponse;

class FilesController extends Controller
{
    /**
     * Render icon based on a file's extension
     *
     * @param $id
     * @return mixed
     */
    public function icon($id)
    {
        $file = File::find($id);

        if ($file->owner != null) {
            if ($file->owner->company->id == auth()->user()->company->id) {
                $svg = Storage::get('assets/file.svg');
                $svg = str_replace('{{EXT}}', ltrim(strtoupper($file->extension), '.'), $svg);
                $svg = str_replace('{{COLOR}}', generateHexColorFromString($file->extension), $svg);

                return response($svg)->header('Content-Type', 'image/svg+xml');
            } else {
                abort(404);
            }
        } else {
            $svg = Storage::get('assets/file.svg');
            $svg = str_replace('{{EXT}}', ltrim(strtoupper($file->extension), '.'), $svg);
            $svg = str_replace('{{COLOR}}', generateHexColorFromString($file->extension), $svg);

            return response($svg)->header('Content-Type', 'image/svg+xml');
        }
    }

    /**
     * Download a particular file
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function download($id)
    {
        $_file = File::find($id);

        if ($_file->owner != null) {
            if ($_file->owner->company->id == auth()->user()->company->id) {
                $file_path = Storage::path($_file->path);
                return response()->download($file_path, $_file->name, ['Content-Type', $_file->mime]);
            } else {
                abort(404);
            }
        } else {
            $file_path = Storage::path($_file->path);
            return response()->download($file_path, $_file->name, ['Content-Type', $_file->mime]);
        }
    }

    /**
     * Delete a particular file from
     * storage and from database
     *
     * @param Request $request
     * @return static
     */
    public function destroy(Request $request)
    {
        $file = File::find($request->id);

        if ($file->owner != null) {
            if ($file->owner->company->id == auth()->user()->company->id) {
                Storage::delete($file->path);
                $file->delete();

                return JsonResponse::create(['status' => 'success', 'id' => $request->id]);
            } else {
                abort(404);
            }
        } else {
            Storage::delete($file->path);
            $file->delete();

            return JsonResponse::create(['status' => 'success', 'id' => $request->id]);
        }
    }
}
