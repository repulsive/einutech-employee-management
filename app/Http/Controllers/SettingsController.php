<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index()
    {
        return redirect()->route('settings.page', 'account');
    }

    public function page($page)
    {
        return view('settings.index', [
            'page' => $page
        ]);
    }
}
