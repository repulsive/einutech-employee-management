<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use League\Fractal\TransformerAbstract;
use League\Fractal\Manager as FractalManager;
use League\Fractal\Resource\Collection as FractalCollection;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function generateIndexCollection($model, TransformerAbstract $transformer)
    {
        $collection = collect($model);
        $collection = new FractalCollection($collection, $transformer);
        $collection = (new FractalManager)->createData($collection)->toArray();
        $collection = collect($collection['data']);
        return $collection;
    }
}
