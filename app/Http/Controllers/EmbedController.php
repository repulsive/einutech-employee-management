<?php

namespace App\Http\Controllers;

use Hashids\Hashids;
use App\Models\Company;
use Illuminate\Http\Request;

class EmbedController extends Controller
{
    public function index($company_id)
    {
        $company = Company::where('_id', $company_id)->firstOrFail();

        $positions = $company->positions()->where('state', 1)->get();

        return view('embed.index', [
            'company' => $company,
            'positions' => $positions
        ]);
    }
}