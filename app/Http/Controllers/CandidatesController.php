<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Candidate;
use App\Models\Position;
use App\Support\DataGrid\Facade as DataGrid;
use App\Transformers\CandidateTransformer;
use Illuminate\Http\Request;
use App\Http\Requests\CandidateFormRequest;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates = Candidate::where('company_id', auth()->user()->company_id)->get();

        $collection = $this->generateIndexCollection($candidates, new CandidateTransformer());

        $dataGrid = DataGrid::collection($collection)
            ->setDefaultOrder(['field' => 'created_at', 'keyword' => 'desc'])
            ->setEmptyMessage(__('candidates.message.empty'))
            ->column('id', ['sortable' => true, 'label' => __('candidates.field.id')])
            ->column('first_name', ['sortable' => true, 'label' => __('candidates.field.first_name')])
            ->column('last_name', ['sortable' => true, 'label' => __('candidates.field.last_name')])
            ->column('email', ['sortable' => true, 'label' => __('candidates.field.email')])
            ->column('department_name', ['sortable' => true, 'label' => __('candidates.field.department')])
            ->column('position_name', ['sortable' => true, 'label' => __('candidates.field.position')])
            ->optionsColumn('options', 'candidates', 'id', ['label' => __('candidates.field.options')])
            ->setPagination(25)
            ->generate();

        return view('candidates.index', [
            'dataGrid' => $dataGrid
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $positions = Position::where('company_id', auth()->user()->company_id)->get();
        $countries = Country::orderBy('name', 'asc')->get();

        if ($request->session()->has('continue')) {
            return view('candidates.create', [
                'positions' => $positions,
                'countries' => $countries,
                'continue' => $request->session()->get('continue')
            ]);
        } else {
            return view('candidates.create', [
                'positions' => $positions,
                'countries' => $countries
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CandidateFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CandidateFormRequest $request)
    {
        $custom_data = array_combine($request->post('custom_data_key'), $request->post('custom_data_value'));
        $custom_data = array_filter($custom_data, function($value) { return $value !== null; });

        $request->merge([
            'company_id' => auth()->user()->company_id,
            'custom_data' => empty($custom_data) ? '' : serialize($custom_data)
        ]);

        Candidate::create($request->all());

        if ($request->has('continue')) {
            return redirect()->route('candidates.create')->with(['continue' => true]);
        } else {
            return redirect()->route('candidates.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $positions = Position::where('company_id', auth()->user()->company_id)->get();
        $countries = Country::orderBy('name', 'asc')->get();
        $candidate = Candidate::where('id', $id)->firstOrFail();

        if ($candidate->company->id == auth()->user()->company->id) {
            $custom_fields = unserialize($candidate->custom_data) ? unserialize($candidate->custom_data) : [];

            return view('candidates.show', [
                'positions' => $positions,
                'countries' => $countries,
                'candidate' => $candidate,
                'custom_fields' => $custom_fields
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Displays delete confirmation dialog
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function bulkDelete(Request $request)
    {
        if (!empty($request->post('selected'))) {
            $selected = $request->post('selected');
            $candidates = Candidate::whereIn('id', $selected)->get();

            return view('candidates.bulkdelete', [
                'candidates' => $candidates,
                'selected' => serialize($selected)
            ]);
        } else {
            return redirect()->route('candidates.index');
        }
    }

    /**
     * Displays delete confirmation dialog
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete($id)
    {
        $candidate = Candidate::where('id', $id)->firstOrFail();

        if ($candidate->company->id == auth()->user()->company->id) {
            return view('candidates.delete', [
                'candidate' => $candidate
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $candidate = Candidate::where('id', $id)->firstOrFail();
        $positions = Position::where('company_id', auth()->user()->company_id)->get();
        $countries = Country::orderBy('name', 'asc')->get();

        if ($candidate->company->id == auth()->user()->company->id) {
            $custom_fields = unserialize($candidate->custom_data) ? unserialize($candidate->custom_data) : [];

            return view('candidates.edit', [
                'candidate' => $candidate,
                'positions' => $positions,
                'countries' => $countries,
                'custom_fields' => $custom_fields
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CandidateFormRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CandidateFormRequest $request, $id)
    {
        $candidate = Candidate::where('id', $id)->firstOrFail();

        if ($candidate->company->id == auth()->user()->company->id) {
            if ($request->has('custom_data_key') && $request->has('custom_data_value')) {
                $custom_data = array_combine($request->post('custom_data_key'), $request->post('custom_data_value'));
                $custom_data = array_filter($custom_data, function ($value) {
                    return $value !== null;
                });
            }

            $request->merge([
                'company_id' => auth()->user()->company_id,
                'custom_data' => empty($custom_data) ? '' : serialize($custom_data)
            ]);

            $candidate->update($request->all());

            return redirect()->route('candidates.show', $candidate->id);
        } else {
            abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $id)
    {
        $candidate = Candidate::where('id', $id)->firstOrFail();

        if ($candidate->company->id == auth()->user()->company->id) {
            if ($request->post('submit') == 'yes') {
                $candidate->delete();
                return redirect()->route('candidates.index');
            } else {
                return redirect()->route('candidates.index');
            }
        } else {
            abort(403);
        }
    }

    /**
     * Remove multiple selected resources from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulkDestroy(Request $request)
    {
        $selected = unserialize($request->post('selected'));
        $candidates = Candidate::whereIn('id', $selected)->get();

        if ($request->post('submit') == 'yes') {
            foreach ($candidates as $candidate) {
                $candidate->delete();
            }
            return redirect()->route('candidates.index');
        } else {
            return redirect()->route('candidates.index');
        }
    }

    /**
     * Shows a page prepared for printing
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function print(Request $request, $id)
    {
        $candidate = Candidate::where('id', $id)->firstOrFail();

        if ($candidate->company->id == auth()->user()->company->id) {
            $custom_fields = unserialize($candidate->custom_data) ? unserialize($candidate->custom_data) : [];
            $company = $candidate->company;
            $countries = Country::all();
            $positions = Position::where('company_id', auth()->user()->company->id)->get();

            return view('candidates.print', [
                'candidate' => $candidate,
                'company' => $company,
                'countries' => $countries,
                'positions' => $positions,
                'custom_fields' => $custom_fields
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Uploads files and links them to
     * a candidate model instance
     *
     * @param Request $request
     * @param $id
     * @return static
     */
    public function uploadFiles(Request $request, $id)
    {
        $candidate = Candidate::find($id);

        if ($candidate->company->id == auth()->user()->company->id) {
            $collection = new Collection();

            foreach ($request->file('files') as $file) {
                $file = File::upload($file);
                $candidate->files()->save($file);
                $collection->add($file);
            }

            return JsonResponse::create(['status' => 'success', 'files' => $collection]);
        } else {
            abort(404);
        }
    }

    /**
     * Uploads an image and links it to
     * a candidate model instance
     *
     * @param Request $request
     * @param $id
     * @return static
     */
    public function uploadImage(Request $request, $id)
    {
        $candidate = Candidate::find($id);

        if ($candidate->company->id == auth()->user()->company->id) {
            foreach ($candidate->images as $_image) { $_image->update(['active' => false]); }

            $image = Image::upload($request);
            $candidate->images()->save($image);

            return JsonResponse::create(['status' => 'success', 'id' => $image->id]);
        } else {
            abort(404);
        }
    }

    /**
     * Creates an Excel (.xlsx) spreadsheet
     * and sends it as a reponse
     *
     * @param Request $request
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function downloadSpreadsheet(Request $request)
    {
        $company = auth()->user()->company;
        $candidates = Candidate::where('company_id', $company->id)->get();
        $spreadsheet = new Spreadsheet();

        $now = new \DateTime();
        $timestamp = $now->getTimestamp();

        $title = 'EEM-'.$company->id.'-CandidateList-'.$timestamp;

        $spreadsheet->getProperties()->setCreator('Einutech EEM')
            ->setLastModifiedBy('Einutech EEM')
            ->setTitle($title)
            ->setSubject($title)
            ->setDescription($title)
            ->setCategory($title);

        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'ID')
            ->setCellValue('B1', 'First Name')
            ->setCellValue('C1', 'Last Name')
            ->setCellValue('D1', 'Email')
            ->setCellValue('E1', 'Phone')
            ->setCellValue('F1', 'Birth Date')
            ->setCellValue('G1', 'UMCN')
            ->setCellValue('H1', 'Sex')
            ->setCellValue('I1', 'Address')
            ->setCellValue('J1', 'City')
            ->setCellValue('K1', 'Postal')
            ->setCellValue('L1', 'State')
            ->setCellValue('M1', 'Country')
            ->setCellValue('N1', 'Start Date')
            ->setCellValue('O1', 'Position');

        $count = 1;
        foreach ($candidates as $candidate) {
            $count++;
            $sheet->setCellValue('A'.$count, $candidate->id)
                ->setCellValue('B'.$count, $candidate->first_name)
                ->setCellValue('C'.$count, $candidate->last_name)
                ->setCellValue('D'.$count, $candidate->email)
                ->setCellValue('E'.$count, $candidate->phone)
                ->setCellValue('F'.$count, $candidate->birth_date)
                ->setCellValue('G'.$count, $candidate->umcn)
                ->setCellValue('H'.$count, $candidate->sex == 1 ? 'Male' : 'Female')
                ->setCellValue('I'.$count, $candidate->address)
                ->setCellValue('J'.$count, $candidate->city)
                ->setCellValue('K'.$count, $candidate->postal)
                ->setCellValue('L'.$count, $candidate->state)
                ->setCellValue('M'.$count, $candidate->country->name)
                ->setCellValue('N'.$count, $candidate->start_date)
                ->setCellValue('O'.$count, $candidate->position->name);
        }

        $sheet->setTitle($title);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$title.'.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }
}
