<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Position;
use App\Support\DataGrid\Facade as DataGrid;
use App\Transformers\PositionTransformer;
use GrahamCampbell\Markdown\Facades\Markdown;
use Illuminate\Http\Request;
use App\Http\Requests\PositionFormRequest;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\JsonResponse;

class PositionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $positions = Position::where('company_id', auth()->user()->company_id)->get();

        $collection = $this->generateIndexCollection($positions, new PositionTransformer());

        $dataGrid = DataGrid::collection($collection)
            ->setDefaultOrder(['field' => 'id', 'keyword' => 'asc'])
            ->column('name', ['sortable' => true, 'label' => __('positions.field.name')])
            ->column('state', ['sortable' => true, 'label' => __('positions.field.state')])
            ->column('department_name', ['sortable' => true, 'label' => __('positions.field.department'), 'if_null' => __('positions.error.department.unavailable')])
            ->optionsColumn('options', 'positions', 'id', ['label' => __('positions.field.options')])
            ->setPagination(25)
            ->generate();

        return view('positions.index', [
            'dataGrid' => $dataGrid
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $departments = Department::where('company_id', auth()->user()->company_id)->get();

        if ($request->session()->has('continue')) {
            return view('positions.create', [
                'departments' => $departments,
                'continue' => $request->session()->get('continue')
            ]);
        } else {
            return view('positions.create', [
                'departments' => $departments
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PositionFormRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PositionFormRequest $request)
    {
        $request->merge([
            'company_id' => auth()->user()->company_id,
            'state' => $request->has('state') ? 1 : 0
        ]);

        $request->merge(['skills' => explode(',', $request->skills)]);
        Position::create($request->all());

        if ($request->has('continue')) {
            return redirect()->route('positions.create')->with(['continue' => true]);
        } else {
            return redirect()->route('positions.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $position = Position::where('_id', $id)->firstOrFail();
        $description = Markdown::convertToHtml($position->description);

        if ($position->company->id == auth()->user()->company->id) {
            return view('positions.show', [
                'position' => $position,
                'description' => $description
            ]);
        } else {
            abort(404);
        }

    }

    /**
     * Displays delete confirmation dialog
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function bulkDelete(Request $request)
    {
        if (!empty($request->post('selected'))) {
            $selected = $request->post('selected');
            $positions = Position::whereIn('id', $selected)->get();

            foreach ($positions as $position) {
                if ($position->company->id != auth()->user()->company->id) {
                    abort(403);
                }
            }

            return view('positions.bulkdelete', [
                'positions' => $positions,
                'selected' => serialize($selected)
            ]);
        } else {
            return redirect()->route('positions.index');
        }
    }

    /**
     * Displays delete confirmation dialog
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete($id)
    {
        $position = Position::where('_id', $id)->firstOrFail();

        if ($position->company->id == auth()->user()->company->id) {
            return view('positions.delete', [
                'position' => $position
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $position = Position::where('_id', $id)->firstOrFail();
        $departments = Department::where('company_id', auth()->user()->company_id)->get();

        if ($position->company->id == auth()->user()->company->id) {
            return view('positions.edit', [
                'departments' => $departments,
                'position' => $position
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PositionFormRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PositionFormRequest $request, $id)
    {
        $position = Position::where('_id', $id)->firstOrFail();

        if ($position->company->id == auth()->user()->company->id) {
            $request->merge([
                'company_id' => auth()->user()->company_id,
                'state' => $request->has('state') ? 1 : 0
            ]);

            $request->merge(['skills' => explode(',', $request->skills)]);
            $position->update($request->all());

            return redirect()->route('positions.show', $position->id);
        } else {
            abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $id)
    {
        $position = Position::where('_id', $id)->firstOrFail();

        if ($position->company->id == auth()->user()->company->id) {
            if ($request->post('submit') == 'yes') {
                $position->delete();
                
                return redirect()->route('positions.index');
            } else {
                return redirect()->route('positions.index');
            }
        } else {
            abort(403);
        }
    }

    /**
     * Remove multiple selected resources from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulkDestroy(Request $request)
    {
        $selected = unserialize($request->post('selected'));
        $positions = Position::whereIn('id', $selected)->get();

        if ($request->post('submit') == 'yes') {
            foreach ($positions as $position) {
                if ($position->company->id != auth()->user()->company->id) {
                    abort(403);
                }

                $position->delete();
            }
            return redirect()->route('positions.index');
        } else {
            return redirect()->route('positions.index');
        }
    }

    public function getSkills($id)
    {
        $position = Position::where('_id', $id)->first();
        
        if ($position->company->id == auth()->user()->company->id) {
            return JsonResponse::create($position->skills());
        } else {
            abort(403);
        }
    }

    /**
     * Creates and Excel (.xlsx) spreadsheet
     * and sends it as a reponse
     *
     * @param Request $request
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function downloadSpreadsheet(Request $request)
    {
        $company = auth()->user()->company;
        $positions = Position::where('company_id', $company->id)->get();
        $spreadsheet = new Spreadsheet();

        $now = new \DateTime();
        $timestamp = $now->getTimestamp();

        $title = 'EEM-'.$company->id.'-PositionList-'.$timestamp;

        $spreadsheet->getProperties()->setCreator('Einutech EEM')
            ->setLastModifiedBy('Einutech EEM')
            ->setTitle($title)
            ->setSubject($title)
            ->setDescription($title)
            ->setCategory($title);

        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'ID')
            ->setCellValue('B1', 'Name')
            ->setCellValue('C1', 'Description')
            ->setCellValue('D1', 'State');

        $count = 1;
        foreach ($positions as $position) {
            $count++;
            $sheet->setCellValue('A'.$count, $position->id)
                ->setCellValue('B'.$count, $position->name)
                ->setCellValue('C'.$count, $position->description)
                ->setCellValue('D'.$count, $position->state == 1 ? 'Open' : 'Closed');
        }

        $sheet->setTitle($title);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$title.'.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }
}
