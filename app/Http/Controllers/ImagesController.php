<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImagesController extends Controller
{
    /**
     * Display a particular image
     * 
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        if ($id == 'avatar') {
            $file_data = Storage::get('/assets/avatar.png');
            return response($file_data)->header('Content-Type', 'image/png');
        } else {
            $image = Image::where('_id', $id)->first();

            if ($image->owner != null) {
                if ($image->owner instanceof Company) {
                    $file_data = Storage::get($image->path);
                    return response($file_data)->header('Content-Type', $image->mime);
                } else {
                    if ($image->owner->company->id == auth()->user()->company->id) {
                        $file_data = Storage::get($image->path);
                        return response($file_data)->header('Content-Type', $image->mime);
                    } else {
                        abort(404);
                    }
                }
            } else {
                $file_data = Storage::get($image->path);
                return response($file_data)->header('Content-Type', $image->mime);
            }
        }
    }
    
    public function companyLogo($id) 
    {
        $image = Image::where('_id', $id)->first();
        
        if ($image->owner instanceof Company) {
            $file_data = Storage::get($image->path);
            return response($file_data)->header('Content-Type', $image->mime);
        } else {
            abort(404);
        }
    }
}
