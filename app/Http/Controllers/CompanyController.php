<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = auth()->user()->company;
        $country = $company->country;
        $image = $company->image;

        return view('company.index', [
            'company' => $company,
            'country' => $country,
            'image' => $image
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $company = auth()->user()->company;
        $country = $company->country;
        $image = $company->image;

        return view('company.edit', [
            'company' => $company,
            'country' => $country,
            'image' => $image
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $company = auth()->user()->company;

        $company->update($request->all());

        return redirect()->route('company.index');
    }

    /**
     * Uploads an image and links it to
     * an employee model instance
     *
     * @param Request $request
     * @param $id
     * @return static
     */
    public function uploadImage(Request $request)
    {
        $company = auth()->user()->company;

        foreach ($company->images as $_image) { $_image->update(['active' => false]); }

        $image = Image::upload($request);
        $company->images()->save($image);

        return JsonResponse::create(['status' => 'success', 'id' => $image->id]);
    }
}
