<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Department;
use App\Transformers\DepartmentTransformer;
use Illuminate\Http\Request;
use App\Http\Requests\DepartmentFormRequest;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Support\DataGrid\Facade as DataGrid;

class DepartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::where('company_id', auth()->user()->company_id)->get();

        $collection = $this->generateIndexCollection($departments, new DepartmentTransformer());

        $dataGrid = DataGrid::collection($collection)
            ->setDefaultOrder(['field' => 'id', 'keyword' => 'asc'])
            ->column('name', ['sortable' => true, 'label' => __('departments.field.name')])
            ->optionsColumn('options', 'positions', 'id', ['label' => __('departments.field.options')])
            ->setPagination(25)
            ->generate();

        return view('departments.index', [
            'dataGrid' => $dataGrid
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $countries = Country::all();

        if ($request->session()->has('continue')) {
            return view('departments.create', [
                'countries' => $countries,
                'continue' => $request->session()->get('continue')
            ]);
        } else {
            return view('departments.create', [
                'countries' => $countries,
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DepartmentFormRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartmentFormRequest $request)
    {
        $request->merge([
            'company_id' => auth()->user()->company_id
        ]);

        Department::create($request->all());

        if ($request->has('continue')) {
            return redirect()->route('departments.create')->with(['continue' => true]);
        } else {
            return redirect()->route('departments.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $department = Department::where('id', $id)->firstOrFail();

        if ($department->company->id == auth()->user()->company->id) {
            return view('departments.show', [
                'department' => $department
            ]);
        } else {
            abort(404);
        }

    }

    /**
     * Displays delete confirmation dialog
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function bulkDelete(Request $request)
    {
        if (!empty($request->post('selected'))) {
            $selected = $request->post('selected');
            $departments = Department::whereIn('id', $selected)->get();

            foreach ($departments as $department) {
                if ($department->company->id != auth()->user()->company->id) {
                    abort(403);
                }
            }

            return view('departments.bulkdelete', [
                'departments' => $departments,
                'selected' => serialize($selected)
            ]);
        } else {
            return redirect()->route('departments.index');
        }
    }

    /**
     * Displays delete confirmation dialog
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete($id)
    {
        $department = Department::where('id', $id)->firstOrFail();

        if ($department->company->id == auth()->user()->company->id) {
            return view('departments.delete', [
                'department' => $department
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::where('id', $id)->firstOrFail();

        if ($department->company->id == auth()->user()->company->id) {
            return view('departments.edit', [
                'department' => $department
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PositionFormRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PositionFormRequest $request, $id)
    {
        $department = Department::where('id', $id)->firstOrFail();

        if ($department->company->id == auth()->user()->company->id) {
            $request->merge([
                'company_id' => auth()->user()->company_id,
            ]);

            $department->update($request->all());

            return redirect()->route('departments.show', $department->id);
        } else {
            abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $id)
    {
        $department = Department::where('id', $id)->firstOrFail();

        if ($department->company->id == auth()->user()->company->id) {
            if ($request->post('submit') == 'yes') {
                $department->delete();

                return redirect()->route('departments.index');
            } else {
                return redirect()->route('departments.index');
            }
        } else {
            abort(403);
        }
    }

    /**
     * Remove multiple selected resources from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulkDestroy(Request $request)
    {
        $selected = unserialize($request->post('selected'));
        $departments = Department::whereIn('id', $selected)->get();

        if ($request->post('submit') == 'yes') {
            foreach ($departments as $department) {
                if ($department->company->id != auth()->user()->company->id) {
                    abort(403);
                }

                $department->delete();
            }
            return redirect()->route('departments.index');
        } else {
            return redirect()->route('departments.index');
        }
    }

    /**
     * Creates and Excel (.xlsx) spreadsheet
     * and sends it as a reponse
     *
     * @param Request $request
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function downloadSpreadsheet(Request $request)
    {
        $company = auth()->user()->company;
        $departments = Department::where('company_id', $company->id)->get();
        $spreadsheet = new Spreadsheet();

        $now = new \DateTime();
        $timestamp = $now->getTimestamp();

        $title = 'EEM-'.$company->id.'-DepartmentList-'.$timestamp;

        $spreadsheet->getProperties()->setCreator('Einutech EEM')
            ->setLastModifiedBy('Einutech EEM')
            ->setTitle($title)
            ->setSubject($title)
            ->setDescription($title)
            ->setCategory($title);

        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'ID')
            ->setCellValue('B1', 'Name');

        $count = 1;
        foreach ($departments as $department) {
            $count++;
            $sheet->setCellValue('A'.$count, $department->id)
                ->setCellValue('B'.$count, $department->name);
        }

        $sheet->setTitle($title);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$title.'.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }
}
