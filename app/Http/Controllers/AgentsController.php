<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AgentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agentRole = Role::where('name', 'agent')->first();

        $agents = $agentRole->users()->paginate(25);

        return view('agents.index', [
            'agents' => $agents
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->session()->has('continue')) {
            return view('agents.create', [
                'continue' => $request->session()->get('continue')
            ]);
        } else {
            return view('agents.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $password = Hash::make($request->post('password'));
        $agent = User::create([
            'name' => $request->post('first_name').' '.$request->post('last_name'),
            'email' => $request->post('email'),
            'password' => $password
        ]);

        $agent->assignRole(1);

        if ($request->has('continue')) {
            return redirect()->route('agents.create')->with(['continue' => true]);
        } else {
            return redirect()->route('agents.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agent = User::where('id', $id)->firstOrFail();

        if ($agent->company->id == auth()->user()->company->id) {
            return view('agents.show', [
                'agent' => $agent
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agent = User::where('id', $id)->firstOrFail();

        if ($agent->company->id == auth()->user()->company->id) {
            return view('agents.edit', [
                'agent' => $agent
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $password = Hash::make($request->post('password'));
        $agent = User::where('id', $id)->firstOrFail();

        if ($agent->company->id == auth()->user()->company->id) {
            $agent->update([
                'name' => $request->post('first_name').' '.$request->post('last_name'),
                'email' => $request->post('email'),
                'password' => $password
            ]);

            return redirect()->route('agents.show', $agent->id);
        } else {
            abort(403);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Uploads an image and links it to
     * an agent (user model instance)
     *
     * @param Request $request
     * @param $id
     * @return static
     */
    public function uploadImage(Request $request, $id)
    {
        $user = User::find($id);

        if ($user->company->id == auth()->user()->company->id) {
            foreach ($user->images as $_image) { $_image->update(['active' => false]); }

            $image = Image::upload($request);
            $user->images()->save($image);

            return JsonResponse::create(['status' => 'success', 'id' => $image->id]);
        } else {
            abort(404);
        }
    }
}
