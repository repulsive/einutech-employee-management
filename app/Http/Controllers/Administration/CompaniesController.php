<?php

namespace App\Http\Controllers\Administration;

use App\Models\Company;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transformers\CompanyTransformer;
use App\Support\DataGrid\Facade as DataGrid;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();

        $collection = $this->generateIndexCollection($companies, new CompanyTransformer());

        $dataGrid = DataGrid::collection($collection)
            ->setDefaultOrder(['field' => 'id', 'keyword' => 'asc'])
            ->setEmptyMessage(__('companies.message.empty'))
            ->column('id', ['sortable' => true, 'label' => __('companies.field.id')])
            ->column('name', ['sortable' => true, 'label' => __('companies.field.name')])
            ->column('country_name', ['sortable' => true, 'label' => __('companies.field.country')])
            ->column('company_number', ['sortable' => true, 'label' => __('companies.field.company_number'), 'if_null' => __('companies.error.company_number.unavailable')])
            ->column('tax_id_number', ['sortable' => true, 'label' => __('companies.field.tax_id_number'), 'if_null' => __('companies.error.tax_id_number.unavailable')])
            ->optionsColumn('options', 'companies', 'id', ['label' => __('companies.field.options')])
            ->setPagination(25)
            ->generate();

        return view('administration.companies.index', [
            'dataGrid' => $dataGrid
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $countries = Country::orderBy('name', 'asc')->get();

        if ($request->session()->has('continue')) {
            return view('administration.companies.create', [
                'countries' => $countries,
                'continue' => $request->session()->get('continue')
            ]);
        } else {
            return view('administration.companies.create', [
                'countries' => $countries
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Company::create([
            'name' => $request
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::where('id', $id)->firstOrFail();
        $country = $company->country;

        return view('administration.companies.show', [
            'company' => $company,
            'country' => $country
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = auth()->user()->company;
        $country = $company->country;

        return view('administration.companies.edit', [
            'company' => $company,
            'country' => $country
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove multiple selected resources from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulkDestroy(Request $request)
    {
        //
    }

    /**
     * Uploads an image and links it to
     * an employee model instance
     *
     * @param Request $request
     * @param $id
     * @return static
     */
    public function uploadImage(Request $request, $id)
    {
        if ($id != 'none') {
            $employee = Employee::find($id);

            if ($employee->company->id == auth()->user()->company->id) {
                foreach ($employee->images as $_image) { $_image->update(['active' => false]); }

                $image = Image::upload($request);
                $employee->images()->save($image);

                return JsonResponse::create(['status' => 'success', 'id' => $image->id]);
            } else {
                abort(404);
            }
        } else {
            $image = Image::upload($request);
            return JsonResponse::create(['status' => 'success', 'id' => $image->id]);
        }
    }
}
