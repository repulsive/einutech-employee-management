<?php

namespace App\Http\Controllers\Administration;

use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Support\DataGrid\Facade as DataGrid;
use App\Transformers\UserTransformer;

class AdministratorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $administratorRole = Role::where('name', 'Administrator')->first();

        $administrators = $administratorRole->users;

        $collection = $this->generateIndexCollection($administrators, new UserTransformer());

        $dataGrid = DataGrid::collection($collection)
            ->setDefaultOrder(['field' => 'id', 'keyword' => 'asc'])
            ->setEmptyMessage(__('administrators.message.empty'))
            ->column('id', ['sortable' => true, 'label' => __('administrators.field.id')])
            ->column('first_name', ['sortable' => true, 'label' => __('administrators.field.first_name')])
            ->column('last_name', ['sortable' => true, 'label' => __('administrators.field.last_name')])
            ->column('email', ['sortable' => true, 'label' => __('administrators.field.email')])
            ->optionsColumn('options', 'administrators', 'id', ['label' => __('administrators.field.options')])
            ->setPagination(25)
            ->generate();

        return view('administration.administrators.index', [
            'dataGrid' => $dataGrid
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->session()->has('continue')) {
            return view('administrators.create', [
                'continue' => $request->session()->get('continue')
            ]);
        } else {
            return view('administrators.create');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $password = Hash::make($request->post('password'));
        $administrator = User::create([
            'name' => $request->post('first_name').' '.$request->post('last_name'),
            'email' => $request->post('email'),
            'password' => $password
        ]);

        $administrator->assignRole(3);

        return redirect()->route('administrators.index');

        if ($request->has('continue')) {
            return redirect()->route('administrators.create')->with(['continue' => true]);
        } else {
            return redirect()->route('administrators.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $administrator = User::where('id', $id)->firstOrFail();

        return view('administrator.show', [
            'administrator' => $administrator
        ]);
    }

    /**
     * Displays delete confirmation dialog
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function bulkDelete(Request $request)
    {
        if (!empty($request->post('selected'))) {
            $selected = $request->post('selected');
            $administrators = User::whereIn('id', $selected)->get();

            return view('administrators.bulkdelete', [
                'administrators' => $administrators,
                'selected' => serialize($selected)
            ]);
        } else {
            return redirect()->route('administrators.index');
        }
    }

    /**
     * Displays delete confirmation dialog
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete($id)
    {
        $administrator = User::where('id', $id)->firstOrFail();

        return view('administrators.delete', [
            'administrator' => $administrator
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $administrator = User::where('id', $id)->firstOrFail();

        return view('administrators.edit', [
            'administrator' => $administrator
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $password = Hash::make($request->post('password'));
        $administrator = User::where('id', $id)->firstOrFail();

        $administrator->update([
            'name' => $request->post('first_name').' '.$request->post('last_name'),
            'email' => $request->post('email'),
            'password' => $password
        ]);

        return redirect()->route('administrators.show', $administrator->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $administrator = User::where('id', $id)->firstOrFail();

        if ($request->post('submit') == 'yes') {
            $administrator->revokeRole('administrator');
            return redirect()->route('administrators.index');
        } else {
            return redirect()->route('administrators.index');
        }
    }

    /**
     * Remove multiple selected resources from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulkDestroy(Request $request)
    {
        //
    }

    /**
     * Uploads an image and links it to
     * an employee model instance
     *
     * @param Request $request
     * @param $id
     * @return static
     */
    public function uploadImage(Request $request, $id)
    {
        if ($id != 'none') {
            $employee = Employee::find($id);

            if ($employee->company->id == auth()->user()->company->id) {
                foreach ($employee->images as $_image) { $_image->update(['active' => false]); }

                $image = Image::upload($request);
                $employee->images()->save($image);

                return JsonResponse::create(['status' => 'success', 'id' => $image->id]);
            } else {
                abort(404);
            }
        } else {
            $image = Image::upload($request);
            return JsonResponse::create(['status' => 'success', 'id' => $image->id]);
        }
    }
}
