<?php

namespace App\Http\Controllers;

use App\Events\ModelUpdated;
use App\Models\File;
use App\Models\Image;
use App\Models\Country;
use App\Models\Employee;
use App\Models\Position;
use App\Models\Department;
use App\Support\DataGrid\Facade as DataGrid;
use App\Transformers\EmployeeTransformer;
use App\Http\Requests\EmployeeFormRequest;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class EmployeesController extends Controller
{
    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::where('company_id', auth()->user()->company_id)->get();

        $collection = $this->generateIndexCollection($employees, new EmployeeTransformer());

        $dataGrid = DataGrid::collection($collection)
            ->setDefaultOrder(['field' => 'id', 'keyword' => 'asc'])
            ->setEmptyMessage(__('employees.message.empty'))
            ->column('first_name', ['sortable' => true, 'label' => __('employees.field.first_name')])
            ->column('last_name', ['sortable' => true, 'label' => __('employees.field.last_name')])
            ->column('email', ['sortable' => true, 'label' => __('employees.field.email')])
            ->column('department_name', ['sortable' => true, 'label' => __('employees.field.department'), 'if_null' => __('employees.error.department.unavailable')])
            ->column('position_name', ['sortable' => true, 'label' => __('employees.field.position'), 'if_null' => __('employees.error.position.unavailable')])
            ->column('score', ['sortable' => true, 'label' => __('employees.field.score'), 'style' => 'font-size: 15px; font-weight: 700; text-transform: uppercase; color: #2BC05D;'])
            ->optionsColumn('options', 'employees', 'id', ['label' => __('employees.field.options')])
            ->setPagination(25)
            ->generate();

        return view('employees.index', [
            'dataGrid' => $dataGrid
        ]);
    }

    /**
     * Show the form for creating a new resource
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $departments = Department::where('company_id', auth()->user()->company_id)->get();
        $countries = Country::orderBy('name', 'asc')->get();
        $positions = Position::where('department_id', null)->get();

        if ($request->session()->has('continue')) {
            return view('employees.create', [
                'positions' => $positions,
                'departments' => $departments,
                'countries' => $countries,
                'continue' => $request->session()->get('continue')
            ]);
        } else {
            return view('employees.create', [
                'positions' => $positions,
                'departments' => $departments,
                'countries' => $countries
            ]);
        }
    }

    /**
     * Store a newly created resource in storage
     *
     * @param EmployeeFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(EmployeeFormRequest $request)
    {
        $custom_data = array_combine($request->post('custom_data_key'), $request->post('custom_data_value'));
        $custom_data = array_filter($custom_data, function($value) { return $value !== null; });

        $request->merge([
            'company_id' => auth()->user()->company_id,
            'custom_data' => empty($custom_data) ? '' : serialize($custom_data)
        ]);

        $employee = Employee::create($request->all());

        $image = Image::findMany($request->input('image_id'));
        $files = File::findMany($request->input('file_ids'));

        $employee->image()->save($image);
        $employee->files()->saveMany($files);

        if ($request->has('continue')) {
            return redirect()->route('employees.create')->with(['continue' => true]);
        } else {
            return redirect()->route('employees.index');
        }
    }

    /**
     * Display the specified resource
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $positions = Position::where('company_id', auth()->user()->company_id)->get();
        $countries = Country::orderBy('name', 'asc')->get();
        $employee = Employee::where('_id', $id)->firstOrFail();

        if ($employee->company->id == auth()->user()->company->id) {
            $custom_fields = unserialize($employee->custom_data) ? unserialize($employee->custom_data) : [];
            $image = $employee->image;

            return view('employees.show', [
                'positions' => $positions,
                'countries' => $countries,
                'employee' => $employee,
                'custom_fields' => $custom_fields,
                'image' => $image
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Displays delete confirmation dialog
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function bulkDelete(Request $request)
    {
        if (!empty($request->post('selected'))) {
            $selected = $request->post('selected');
            $employees = Employee::whereIn('id', $selected)->get();

            return view('employees.bulkdelete', [
                'employees' => $employees,
                'selected' => serialize($selected)
            ]);
        } else {
            return redirect()->route('employees.index');
        }
    }

    /**
     * Displays delete confirmation dialog
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete($id)
    {
        $employee = Employee::where('_id', $id)->firstOrFail();

        if ($employee->company->id == auth()->user()->company->id) {
            return view('employees.delete', [
                'employee' => $employee
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::where('_id', $id)->firstOrFail();
        $departments = Department::where('company_id', auth()->user()->company_id)->get();
        $countries = Country::orderBy('name', 'asc')->get();
        $positions = Position::where('department_id', null)->get();

        if ($employee->company->id == auth()->user()->company->id) {
            $custom_fields = unserialize($employee->custom_data) ? unserialize($employee->custom_data) : [];
            $image = $employee->image;

            return view('employees.edit', [
                'employee' => $employee,
                'positions' => $positions,
                'departments' => $departments,
                'countries' => $countries,
                'custom_fields' => $custom_fields,
                'image' => $image
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage
     *
     * @param  EmployeeFormRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeFormRequest $request, $id)
    {
        $employee = Employee::where('_id', $id)->firstOrFail();

        if ($employee->company->id == auth()->user()->company->id) {
            if ($request->has('custom_data_key') && $request->has('custom_data_value')) {
                $custom_data = array_combine($request->input('custom_data_key'), $request->input('custom_data_value'));
                $custom_data = array_filter($custom_data, function ($value) {
                    return $value !== null;
                });
            }

            $request->merge([
                'company_id' => auth()->user()->company_id,
                'custom_data' => empty($custom_data) ? '' : serialize($custom_data)
            ]);

            $employee->update($request->all());

            return redirect()->route('employees.show', $employee->id);
        } else {
            abort(403);
        }
    }

    /**
     * Remove the specified resource from storage
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $id)
    {
        $employee = Employee::where('_id', $id)->firstOrFail();

        if ($employee->company->id == auth()->user()->company->id) {
            if ($request->post('submit') == 'yes') {
                $employee->delete();
                return redirect()->route('employees.index');
            } else {
                return redirect()->route('employees.index');
            }
        } else {
            abort(403);
        }
    }

    /**
     * Remove multiple selected resources from storage
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function bulkDestroy(Request $request)
    {
        $selected = unserialize($request->post('selected'));
        $employees = Employee::whereIn('id', $selected)->get();

        if ($request->post('submit') == 'yes') {
            foreach ($employees as $employee) {
                $employee->delete();
            }
            return redirect()->route('employees.index');
        } else {
            return redirect()->route('employees.index');
        }
    }

    /**
     * Shows a page prepared for printing
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function print(Request $request, $id)
    {
        $employee = Employee::where('_id', $id)->firstOrFail();

        if ($employee->company->id == auth()->user()->company->id) {
            $custom_fields = unserialize($employee->custom_data) ? unserialize($employee->custom_data) : [];
            $company = $employee->company;
            $countries = Country::all();
            $positions = Position::where('company_id', auth()->user()->company->id)->get();

            return view('employees.print', [
                'employee' => $employee,
                'company' => $company,
                'countries' => $countries,
                'positions' => $positions,
                'custom_fields' => $custom_fields
            ]);
        } else {
            abort(404);
        }
    }

    /**
     * Uploads files and links them to
     * an employee model instance
     *
     * @param Request $request
     * @param $id
     * @return static
     */
    public function uploadFiles(Request $request, $id)
    {
        if ($id != 'none') {
            $employee = Employee::find($id);

            if ($employee->company->id == auth()->user()->company->id) {
                $collection = new Collection();

                foreach ($request->file('files') as $file) {
                    $file = File::upload($file);
                    $employee->files()->save($file);
                    $collection->add($file);
                }

                return JsonResponse::create(['status' => 'success', 'files' => $collection]);
            } else {
                abort(404);
            }
        } else {
            $collection = new Collection();

            foreach ($request->file('files') as $file) {
                $file = File::upload($file);
                $collection->add($file);
            }

            return JsonResponse::create(['status' => 'success', 'files' => $collection]);
        }
    }

    /**
     * Uploads an image and links it to
     * an employee model instance
     *
     * @param Request $request
     * @param $id
     * @return static
     */
    public function uploadImage(Request $request, $id)
    {
        if ($id != 'none') {
            $employee = Employee::where('_id', $id)->first();

            if ($employee->company->id == auth()->user()->company->id) {
                foreach ($employee->images as $_image) { $_image->update(['active' => false]); }

                $image = Image::upload($request);
                $employee->image()->save($image);

                return JsonResponse::create(['status' => 'success', 'id' => $image->id]);
            } else {
                abort(404);
            }
        } else {
            $image = Image::upload($request);

            return JsonResponse::create(['status' => 'success', 'id' => $image->id]);
        }
    }

    /**
     * Creates and Excel (.xlsx) spreadsheet
     * and sends it as a reponse
     *
     * @param Request $request
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function downloadSpreadsheet(Request $request)
    {
        $company = auth()->user()->company;
        $employees = Employee::where('company_id', $company->id)->get();
        $spreadsheet = new Spreadsheet();

        $now = new \DateTime();
        $timestamp = $now->getTimestamp();

        $title = 'EEM-'.'-EmployeeList-'.$timestamp;

        $spreadsheet->getProperties()->setCreator('Einutech EEM')
            ->setLastModifiedBy('Einutech EEM')
            ->setTitle($title)
            ->setSubject($title)
            ->setDescription($title)
            ->setCategory($title);

        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'ID')
            ->setCellValue('B1', 'First Name')
            ->setCellValue('C1', 'Last Name')
            ->setCellValue('D1', 'Email')
            ->setCellValue('E1', 'Phone')
            ->setCellValue('F1', 'Birth Date')
            ->setCellValue('G1', 'UMCN')
            ->setCellValue('H1', 'Sex')
            ->setCellValue('I1', 'Address')
            ->setCellValue('J1', 'City')
            ->setCellValue('K1', 'Postal')
            ->setCellValue('L1', 'State')
            ->setCellValue('M1', 'Country')
            ->setCellValue('N1', 'Start Date')
            ->setCellValue('O1', 'Position');

        $count = 1;
        foreach ($employees as $employee) {
            $count++;
            $sheet->setCellValue('A'.$count, $employee->id)
                ->setCellValue('B'.$count, $employee->first_name)
                ->setCellValue('C'.$count, $employee->last_name)
                ->setCellValue('D'.$count, $employee->email)
                ->setCellValue('E'.$count, $employee->phone)
                ->setCellValue('F'.$count, $employee->birth_date)
                ->setCellValue('G'.$count, $employee->umcn)
                ->setCellValue('H'.$count, $employee->sex == 1 ? 'Male' : 'Female')
                ->setCellValue('I'.$count, $employee->address)
                ->setCellValue('J'.$count, $employee->city)
                ->setCellValue('K'.$count, $employee->postal)
                ->setCellValue('L'.$count, $employee->state)
                ->setCellValue('M'.$count, $employee->country->name)
                ->setCellValue('N'.$count, $employee->start_date)
                ->setCellValue('O'.$count, $employee->position->name);
        }

        $sheet->setTitle($title);

        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$title.'.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }
}
